/** @format */
XMLHttpRequest = GLOBAL.originalXMLHttpRequest ? GLOBAL.originalXMLHttpRequest : GLOBAL.XMLHttpRequest;
  
import { AppRegistry, NativeEventEmitter, NativeModules, AsyncStorage } from 'react-native';
import App from './App';
import { name as appName } from './app.json';
import { bgMessaging, loadSound } from '@helpers';

AppRegistry.registerComponent(appName, () => App);

AppRegistry.registerHeadlessTask('RNFirebaseBackgroundMessage', () => bgMessaging);