import React,{ Component } from 'react';
import Spinner from 'react-native-loading-spinner-overlay';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import { Root } from 'native-base';

import { configureStore, navigationService } from '@helpers';
import { SwitchAuth } from '@navigators';

import { AsyncStorage, Alert, Linking } from "react-native";
import firebase, { Notification, NotificationOpen } from 'react-native-firebase';

const persistantStore = configureStore();
const { store, persistor } = persistantStore;


import { checkPermission, requestPermission, createChannel, getFcmToken } from '@helpers';
import { loadSound } from "@helpers";

export default class App extends Component {
	
	// async checkPermission() {
 //        const enabled = await firebase.messaging().hasPermission();
 //        if (enabled) {
 //            this.getToken();
 //        } else {
 //            this.requestPermission();
 //        }
 //    }

    //3
    async getToken() {
        let fcmToken = await AsyncStorage.getItem('fcmToken', null);
        console.log('fcmToken............', fcmToken)
        if (!fcmToken) {
            fcmToken = await getFcmToken();
            if (fcmToken) {
            // user has a device token
                await AsyncStorage.setItem('fcmToken', fcmToken);
                console.log('fcmToken=============>', fcmToken);
            }
        }
    }


    async componentDidMount() {
    	const enabled = await checkPermission();
    	if(enabled){
    		await this.getToken()
    	}else{
    		await requestPermission();
    		await this.getToken()
    	}
        // this.checkPermission();
        this.createNotificationListeners(); //add this line
    }

    ////////////////////// Add these methods //////////////////////
      
    //Remove listeners allocated in createNotificationListeners()
    componentWillUnmount() {
        this.notificationListener();
        this.notificationOpenedListener();
    }

    async createNotificationListeners() {
        /*
        * Triggered when a particular notification has been received in foreground
        * */
        this.notificationListener = firebase.notifications().onNotification((notification) => {
            const { title, body } = notification;
            this.showAlert(title, body);
        });

        /*
        * If your app is in background, you can listen for when a notification is clicked / tapped / opened as follows:
        * */
        this.notificationOpenedListener = firebase.notifications().onNotificationOpened((notificationOpen) => {
            const { title, body } = notificationOpen.notification;
            this.showAlert(title, body);
        });

        /*
        * If your app is closed, you can check if it was opened by a notification being clicked / tapped / opened as follows:
        * */
        const notificationOpen = await firebase.notifications().getInitialNotification();
        if (notificationOpen) {
            const { title, body } = notificationOpen.notification;
            this.showAlert(title, body);
        }
        /*
        * Triggered for data only payload in foreground
        * */
        this.messageListener = firebase.messaging().onMessage(
            async ({_data}) => {
                const sound = await loadSound();
                navigationService.navigate('Accounting', { sound, message: _data });
        });
    }

    showAlert(title, body) {
        Alert.alert(
            title, body,
            [
                { text: 'OK', onPress: () => console.log('OK Pressed') },
            ],
            { cancelable: false },
        );
    }

	render() {
		return (
		    <Root>
		      <Provider store={store}>
		        <PersistGate loading={null} persistor={persistor}>
		        	<SwitchAuth 
		        		persistenceKey={"NavigationState"} 
		        		renderLoadingExperimental={() => <Spinner visible={true} textContent={'Getting things ready...'} /> }
		        		ref={navigatorRef => navigationService.setTopLevelNavigator(navigatorRef) }
		        	/>
		        </PersistGate>
		      </Provider>
		    </Root>
		);
	}
}
