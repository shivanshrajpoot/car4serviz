import _ from 'lodash';
import {
    CHOOSE_SERVICES,
    SELECT_LOCATION,
    VEHICLE_ADDED,
    SAVE_VEHICLES,
    ADD_IMAGES_TO_JOB_CARD,
    CLEAR_JOB_CARD,
    SET_JOB_CARD_ATTRIBUTES,
    FETCH_QUOTE_REQUESTS,
} from './types';

export const getBrands = () => async (dispatch, getState, api) => {
    try {
        return await api.get('/car-brands')
    } catch (e) {
        throw e;
    }
}

export const getModels = (brandId) => async (dispatch, getState, api) => {
    try {
        return await api.get(`/car-brands/${brandId}/models`);
    } catch (e) {
        throw e;
    }
}

export const getVariants = (brandId, modelId) => async (dispatch, getState, api) => {
    try {
        return await api.get(`/car-brands/${modelId}/models/${brandId}/variants`)
    } catch (e) {
        throw e;
    }
}


export const addVehicle = inputs => async (dispatch, getState, api) => {
    try {
        const response = await api.post('/customers/vehicles', inputs);
        let vehicle = _.get(response, 'data.data', []);
        dispatch({
            type: VEHICLE_ADDED,
            payload: vehicle
        });
        return response;
    } catch (e) {
        throw e;
    }
}

export const chooseServices = (services) => (dispatch, getState, api) => {

    let payload = {
        services: [],
        service_subcategories: []
    };

    payload.services = services.services.map(service => {
        return service.id;
    });

    payload.service_subcategories = services.service_subcategories.map(service_subcategory => {
        return service_subcategory.id;
    });

    dispatch({
        type: CHOOSE_SERVICES,
        payload: payload
    });
    return true;
};

export const selectLocation = (location) => (dispatch, getState, api) => {
    dispatch({
        type: SELECT_LOCATION,
        payload: location
    });
    return true;
};

export const getAllVehicles = inputs => async (dispatch, getState, api) => {
    try {
        const response = await api.get('/customers/vehicles');
        const vehicles = _.get(response, 'data.data', []);
        dispatch({
            type: SAVE_VEHICLES,
            payload: vehicles
        });
        return response;

    } catch (e) {
        throw e;
    }
}

export const setJobCardDetails = inputs => (dispatch, getState, api) => {
    dispatch({
        type: SET_JOB_CARD_ATTRIBUTES,
        payload: inputs
    });
    return true;
}

export const addImagesToJobCard = images => (dispatch, getState, api) => {
    dispatch({
        type: ADD_IMAGES_TO_JOB_CARD,
        payload: images
    });
}

export const getServiceProviders = params => async (dispatch, getState, api) => {
    try {
        const response = await api.get('/service-providers', {
            params
        })
        return _.get(response, 'data.data');
    } catch (e) {
        throw e;
    }

}

export const sendBookingRequest = (jobCard) => async (dispatch, getState, api) => {
    try {
        const response = await api.post(`customers/vehicles/${jobCard.vehicle_id}/job-cards`, jobCard);
        // dispatch({
        //        type: CLEAR_JOB_CARD
        //    });
        return _.get(response, 'data.data');

    } catch (e) {
        throw e;
    }
}

export const getQuoteRequests = () => async (dispatch, getState, api) => {
    try {
        const response = await api.get(`customers/job-cards`)
        dispatch({
            type: FETCH_QUOTE_REQUESTS,
            payload: _.get(response, 'data.data')
        })
    } catch (e) {
        throw e;
        console.log(e);
    }
}