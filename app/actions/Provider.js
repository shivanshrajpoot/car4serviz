import _ from 'lodash';
import { 
    FETCH_QUOTES,
    FETCH_BOOKINGS
} from './types';
import { Toast } from '@components';

export const fetchQuotes = () => async (dispatch, getState, api) => {
	try{
        const response = await api.get(`service-providers/quote-requests`);
        let quotes =  _.get(response, 'data.data', [])
        dispatch({
            type: FETCH_QUOTES,
            payload: quotes
        })
	}catch(e){
		throw e;
	}
}

export const fetchBookingRequests = () => async (dispatch, getState, api) => {
    try {
        const response = await api.get(`service-providers/booking-requests`);
        let bookingRequests =  _.get(response, 'data.data', []);
        dispatch({
            type: FETCH_BOOKINGS,
            payload: bookingRequests
        })
    } catch (e) {
        throw e;
    }
}

export const acceptBooking = (booking_request_id) => async (dispatch, getState, api) => {
    try{
        await api.patch(`/service-providers/booking-requests/${booking_request_id}/accept`);
        return true;
    }catch(e){
        throw e;
    }
}