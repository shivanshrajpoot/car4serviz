import _ from 'lodash';
import { 
    SEND_OTP,
    LOGIN_USER,
    LOGOUT_USER,
    REGISTER_USER,
    UPDATE_USER_TYPE,
    SAVE_PROFILE_DATA_STEP,
    UPDATE_ONBOARDING_STATE,
    //Common
    SAVE_SERVICES
} from './types';

import { AsyncStorage } from 'react-native';

import { navigationService, getFcmToken } from '@helpers';
import { Toast } from '@components';
/**
 * Save Fcm Token To Server
 */
export const saveFcmToken = () => async (dispatch, getState, api) => {
	try{
		const fcm_token = await getFcmToken();
		console.log('fcm_token===============>', fcm_token);
		await api.patch('/fcm-token', { fcm_token });
	}catch(e){
		throw e;
	}
}

/**
 * Login User
 */
export const loginUser = credentials => async (dispatch, getState, api) => {
	try{
        const response = await api.post('/login',  credentials);
        const data = _.get(response, 'data.data', null);
		const token = data.access_token;
        api.defaults.headers.common.Authorization = `Bearer ${token}`;
        if(token){
            dispatch({
                type: LOGIN_USER,
                payload: data,
            });
            return data;
        } else {
            throw {
                message: 'SOMETHING WENT WRONG'
            };
        }

        return response;
	}catch(e){
		throw e;
	}
}

/**
 * Logout User
 */
export const logoutUser = () => async (dispatch, getState, api) => {
	try{
		const fcm_token = await getFcmToken();
		console.log('fcm_token===>Logout', fcm_token);
		await api.post('/logout', { fcm_token });
	    api.defaults.headers.common.Authorization = null;
	    navigationService.navigate('Welcome');
	    dispatch({
	        type: LOGOUT_USER,
	    });
	}catch(e){
		Toast('Something went wrong');
	}
};

export const registerUser = inputs =>  async (dispatch, getState, api) => {
	try{
        const response = await api.post('/register', inputs);
        console.log('response=====>', response);
		const data = _.get(response, 'data.data', null);
        const token = data.access_token;
        if (token){
            api.defaults.headers.common.Authorization = `Bearer ${token}`;
            dispatch({
                type: REGISTER_USER,
                payload: data,
            });
        }else{
            throw 'Token Not Found';
        }
	}catch(e){
		throw e;
	}
}

export const sendOtp = inputs => async (dispatch, getState, api) => {
	try{
		await api.post('/send-otp', inputs);
        dispatch({type: SEND_OTP});
	}catch(e){
		throw e;
	}
}

export const updatePassword = inputs => async (dispatch, getState, api) => {
	try{
		const response = api.post('/update-password', inputs);
		return _.get(response, 'data.data', null);
	}catch(e){
		throw e;
	}
}

export const saveProfileDataSteps = (inputs, step, screenName) => async (dispatch, getState, api) => {
	try{
		const response = await api.post(`/service-providers/step/${step}`, inputs);
		dispatch({
            type: UPDATE_ONBOARDING_STATE,
            payload: screenName
        });
        return response;
	}catch(e){
		throw e;
	}
}

export const forgotPassword = inputs => async (dispatch, getState, api) => {
	try{
	    api.defaults.headers.common.Authorization = null;
	    return await api.post('/send-otp/reset-password', inputs)
	}catch(e){
		throw e;
	}
};

export const resetPassword = inputs => async (dispatch, getState, api) => {
	try{
    	api.defaults.headers.common.Authorization = null;
    	return await api.post('/reset-password', inputs);
	}catch(e){
		throw e;
	}
};

export const updateOnboardingState = inputs => (dispatch, getState, api) => {
    dispatch({
        type: UPDATE_ONBOARDING_STATE,
        payload: inputs.screenName
    });
};

export const checkAppVersion = () => async (dispatch, getState, api) => {
	try{
		return await api.get('/configuration');
	}catch(e){
		throw e;
	}
}

export const getServices = () => async (dispatch, getState, api) => {
	try{
		const response = await api.get('/services');
		let services = _.get(response, 'data.data', null);
		console.log('response=======>'. response);
		dispatch({
            type: SAVE_SERVICES,
            payload: services
        })
        return services;
	}catch{
		throw e;
	}
}

export const getCarTypes = () => async (dispatch, getState, api) => {
	try{
		return await api.get('/car-types');
	}catch(e){
		throw e;
	}
}

export const getBankDetails = (ifsc_code) => async (dispatch, getState, api) => {
	try{
		return await api.get('/bank-details', { params: { ifsc_code } });
	}catch(e){
		throw e;
	}
}
export const updateUserType = userType => async (dispatch, getState) => {
    dispatch({
        type: UPDATE_USER_TYPE,
        payload: userType
    });
    return true;
};

export const saveClientProfile = inputs => async (dispatch, getState, api) => {
    try{
     	return await api.post('/customers', inputs);
    }catch(e){
    	throw e;
    }	
};


