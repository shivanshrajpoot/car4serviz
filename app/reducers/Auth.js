import {
    SEND_OTP,
    LOGIN_USER,
    LOGOUT_USER,
    REGISTER_USER,
    UPDATE_USER_TYPE,
    FETCH_CURRENT_USER,
    UPDATE_ONBOARDING_STATE,
} from '@actions';

export default (state = {
    user: null,
    otpSent: false,
    credentials: {},
    onboardingStep: 'LocationDetails',
    userType: null
}, action) => {
    console.log('Action Type', action.type);
    console.log('In auth', action.payload);
    switch (action.type) {
        case FETCH_CURRENT_USER:
            return { ...state, user: action.payload };

        case LOGIN_USER:
            return { ...state, user: action.payload };

        case LOGOUT_USER:
            return { ...state, user: null, userType: null, credentials: {}, otpSent: false, onboardingStep: 'LocationDetails' };

        case REGISTER_USER:
            return { ...state, user: action.payload };

        case SEND_OTP:
            return { ...state, otpSent: true };

        case UPDATE_ONBOARDING_STATE:
            return { ...state, onboardingStep: action.payload };

        case UPDATE_USER_TYPE:
            return { ...state, userType: action.payload };

        default:
            return state;
    }
};
