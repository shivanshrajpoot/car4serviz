import {
    VEHICLE_ADDED,
    SAVE_VEHICLES
} from '@actions';

export default (state = [], action) => {

    switch (action.type) {
        case VEHICLE_ADDED:
            return [...state, action.payload];

        case SAVE_VEHICLES:
            return action.payload;

        default:
            return state;
    }
}