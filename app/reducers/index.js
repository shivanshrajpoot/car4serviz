import { combineReducers } from 'redux';
import Auth from './Auth';
import Client from './Client';
import Provider from './Provider';
import Common from './Common';

export default combineReducers({
    auth: Auth,
    client: Client,
    provider: Provider,
    common: Common,
});
