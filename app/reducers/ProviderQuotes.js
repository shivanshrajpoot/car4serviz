import {
    FETCH_QUOTES
} from '@actions';

export default (state = {
    requests: [],
}, action) => {
    console.log('Action Type', action.type);
    console.log('In Provider Quotes', action.payload);
    switch (action.type) {
        case FETCH_QUOTES:
            return { ...state, requests: action.payload };

        default:
            return state;
    }
};
