import { combineReducers } from 'redux';
import JobCard from './JobCard';
import Vehicle from './Vehicle';
import ClientQuotes from './ClientQuotes';

export default combineReducers({
    job_card: JobCard,
    vehicles: Vehicle,
    quotes: ClientQuotes,
});
