import {
    CHOOSE_SERVICES,
    SELECT_LOCATION,
    ADD_IMAGES_TO_JOB_CARD,
    CLEAR_JOB_CARD,
    SET_JOB_CARD_ATTRIBUTES,
    LOGOUT_USER
} from '@actions';


export default (state = {
    vehicle: null,
    location_lat: null,
    location_long: null,
    address: null,
    service_datetime: null,
    remark: null,
    services: [],
    service_subcategories: [],
    service_provider_id: null,
    service_location: 1,
    images: [],
    is_urgent: false,
    vehicle_id: null,
}, action) => {
    console.log('Action Type', action.type);
    console.log('In Job Card', action.payload);
    switch (action.type) {
        case CHOOSE_SERVICES:
            return { ...state, services: action.payload.services, service_subcategories: action.payload.service_subcategories };

        case SELECT_LOCATION:
            return { ...state, location_lat: action.payload.lat, location_long: action.payload.long, address: action.payload };

        // case SELECT_VEHICLE_FOR_SERVICE: 
        //     return {...state, vehicle: action.payload};

        // case ADD_REMARK:
        //     return {...state, remark: action.payload};

        // case SET_SERVICE_LOCATION_CODE :
        //     return { ...state, service_location : action.payload  };

        case SET_JOB_CARD_ATTRIBUTES:
            return { ...state, ...action.payload };

        case ADD_IMAGES_TO_JOB_CARD:
            return { ...state, images: action.payload };

        case LOGOUT_USER:
        case CLEAR_JOB_CARD:
            return {
                location_lat: null,
                vehicle: null,
                location_long: null,
                address: null,
                service_datetime: null,
                remark: null,
                services: [],
                service_subcategories: [],
                service_provider_id: null,
                service_location: 1,
                images: [],
                is_urgent: false,
                vehicle_id: null,
            };

        default:
            return state;
    }



}