import { combineReducers } from 'redux';
import ProviderQuotes from './ProviderQuotes';
import ProviderBookings from './ProviderBookings';

export default combineReducers({
    quotes: ProviderQuotes,
    bookings: ProviderBookings,
});
