import {
    BOOKING_REQUESTS
} from '@actions';

export default (state = {
    bookings: [],
}, action) => {
    console.log('Action Type', action.type);
    console.log('In Provider Quotes', action.payload);
    switch (action.type) {
        case BOOKING_REQUESTS:
            return { ...state, bookings: action.payload };

        default:
            return state;
    }
};
