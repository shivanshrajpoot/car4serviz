import {
    SAVE_SERVICES
} from '@actions';

export default (state = {
    services: [],
}, action) => {
    console.log('Action Type', action.type);
    console.log('In Common', action.payload);
    switch (action.type) {
        case SAVE_SERVICES:
            return { ...state, services: action.payload };

        default:
            return state;
    }
};
