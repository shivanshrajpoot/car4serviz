import {
    FETCH_QUOTE_REQUESTS
} from '@actions';

export default (state = {
    requests: [],
}, action) => {
    console.log('Action Type', action.type);
    console.log('In ClientQuotes', action.payload);
    switch (action.type) {
        case FETCH_QUOTE_REQUESTS:
            return { ...state, requests: action.payload };

        default:
            return state;
    }
};
