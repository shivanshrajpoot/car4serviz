import * as yup from "yup";

export const locationValidator = yup.object().shape({
    lat: yup.number().required(),
    long: yup.number().required(),
});
