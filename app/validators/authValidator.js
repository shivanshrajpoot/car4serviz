import * as yup from "yup";

export const userValidator = yup.object().shape({
    phone: yup.string().nullable().required('Phone is required.').matches(/^(?:(?:\+|0{0,2})91(\s*[\-]\s*)?|[0]?)?[6789]\d{9}$/gm, 'Please enter a valid phone'),
    password: yup.string().nullable().required('Password is required.').min(6, 'Password must contain at least 6 characters.').max(15, 'Password can contain upto 15 characters.'),
});

export const clientProfileValidator = yup.object().shape({
    first_name: yup.string().required(),
    last_name: yup.string().required(),
    email: yup.string().email().required()
});

