import React, { Component } from "react";
import {
  Icon,
  View,
  Text,
  Button,
  Content,
  Container,
  StyleProvider,
  Toast
} from "native-base";
import { connect } from 'react-redux';
import { StyleSheet } from 'react-native';
import Spinner from 'react-native-loading-spinner-overlay';

import { colors } from '@constants';
import getTheme from "@base-theme-components";

const styles = StyleSheet.create({
    content:{ flexGrow: 1 },
    spinner: { fontSize: 20 },

});

const mapStateToProps = ({ auth }) => ({
    userType: auth.userType
});

const mapDispatchToProps = { };

class Theme extends Component {

    render() {
        const { children, loading, userType, header, fab } = this.props;
        return (
            <StyleProvider style={getTheme()}>
                <Container>
                    { header }
                    <Content contentContainerStyle={styles.content}>
                        <Spinner
                            visible={!!loading}
                            textContent={'Loading...'}
                            textStyle={StyleSheet.flatten([styles.spinner, {color: userType !== 'client' ? colors.client : colors.provider}])}
                        />
                        { children }
                    </Content>
                    { fab }
                </Container>
            </StyleProvider>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Theme);