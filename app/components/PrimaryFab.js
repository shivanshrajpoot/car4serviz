import React, { Component } from 'react';
import { StyleSheet } from 'react-native';
import { Fab, Icon } from "native-base";

import { colors } from '@constants';

export default class PrimaryFab extends Component {
	render() {
		const { onPress, type } = this.props;
		return (
			<Fab
				style={{ backgroundColor: type === 'client' ? colors.clientColor : colors.providerColor }}
                active={true}
                direction="up"
                containerStyle={{  }}
                position="bottomRight"
                onPress={ onPress() }>
                <Icon name="ios-arrow-forward" />
            </Fab>
		);
	}
}