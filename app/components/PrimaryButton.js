import React, { Component } from 'react';
import { StyleSheet, Keyboard } from 'react-native';
import {
  Text,
  Button,
  Spinner
} from "native-base";

import { colors } from '@constants';

const styles = StyleSheet.create({
	button: {
		margin: 10,
		width: "75%"
	},
	text: {
		textAlign: "center"
	}
});

export default class PrimaryButton extends Component {
	render() {
		const { onPress, type, disabled, buttonText, isLoading } = this.props;
		return (
			<Button rounded block style={StyleSheet.flatten([styles.button, { backgroundColor: type === 'client' ? colors.clientColor : colors.providerColor, opacity: !!disabled ? 0.5 : 1  }]) } disabled={ !!disabled } onPress={ () => {
					Keyboard.dismiss
					onPress()
				}}>
				{!isLoading && <Text style={styles.text}>{ buttonText }</Text>}
				{isLoading && <Spinner color="#fafafa" />}
			</Button>
		);
	}
}