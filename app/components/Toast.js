import { Toast } from "native-base";
import { colors } from '@constants';

export default (error, type='provider') => {
	let textColor = colors.clientColor
	if (type !== 'provider') textColor = colors.providerColor;
	Toast.show({
        text: error,
        duration: 3000,
        textStyle: { color: 'white' },
        buttonText: "Okay",
        buttonTextStyle: { color: 'white' },
        buttonStyle: { backgroundColor: textColor }
    })
}