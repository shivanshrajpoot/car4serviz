import React, { Component } from 'react';
import { StyleSheet, Dimensions } from 'react-native';
import {
	Text,
	Left,
	Icon,
	Body,
	Header,
	Button,
} from "native-base";

import { colors } from '@constants';
import { navigationService } from '@helpers';

const styles = StyleSheet.create({
	header: {
		height:(Dimensions.get('window').height * 25 / 100)
	},
	body: {
		flex:1,
		flexDirection:'column',
		justifyContent: "space-between",
		marginTop:5,
		marginLeft:-(Dimensions.get('window').width * 30 / 100)
	},
	title: {
		fontSize:22,
		fontWeight:'bold',
		color:'white'
	},
	description: {
		color:'white'
	},
	icon: {
		color: "white"
	}

});

export default class PrimaryHeader extends Component {

	render() {
        const { navigation, showDrawerButton, goBackTo, type, title, description } = this.props;
		return (
			<Header style={StyleSheet.flatten([styles.header, {backgroundColor: type === 'client' ? colors.clientColor : colors.providerColor}]) } span>
	            <Left>
	            { goBackTo &&
	            	<Button
		                transparent
		                onPress={() => {goBackTo != undefined ? navigationService.navigate(goBackTo) : navigationService.goBack()}}
		            >
						<Icon
							name="arrow-back"
							style={styles.icon}
		            	/>
	            	</Button>
	            }
	            { showDrawerButton && 
	            	<Button
		                transparent
		                onPress={() => navigationService.openDrawer()}>
		                <Icon name="menu" />
		            </Button>
	            }
	            </Left>
	            <Body style={styles.body}>
					<Text style={styles.title}>{ title }</Text>
					<Text style={styles.description}>{ description }</Text>
	            </Body>
          	</Header>
		);
	}
}