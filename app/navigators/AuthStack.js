import { createStackNavigator } from 'react-navigation';
import {
    Auth,
    SignIn,
    Welcome,
    Register,
    VerifyOtp,
    ForgotPassword,
    ResetPassword
} from '@screens';

export default createStackNavigator(
    {
        Auth: Auth,
        SignIn: SignIn,
        Welcome: Welcome,
        Register: Register,
        VerifyOtp: VerifyOtp,
        ForgotPassword: ForgotPassword,
        ResetPassword: ResetPassword,
    },
    {
        initialRouteName: 'Welcome',
    }

);
