import { createDrawerNavigator, createStackNavigator } from 'react-navigation';
import React, { Component } from 'react';

import {
    SideBar,
    About,
    Dashboard,
    ServiceOrders,
    QuotesProvider,
    GenerateInvoice,
    ManageService,
    QnA,
    Accounting,
    Wallet,
    Refer,
    Order,
    OrderWorkFlow,
    Details,
    Payment,
    Workshop,
    ChooseService,
    CarTypePricing,
    LocationDetails,
    AcceptBooking,
} from '@screens';

import { colors } from '@constants';

const drawerOptions = [
    {
        name: "About",
        route: "About",
        icon: 'user',
        bg: "#C5F442"
    },
    {
        name: "Dashboard",
        route: "Dashboard",
        icon: "dashboard",
        bg: "#C5F442"
    },
    {
        name: "Service Orders",
        route: "ServiceOrders",
        icon: "list-alt",
        bg: "#C5F442"
    },
    {
        name: "Quotes",
        route: "Quotes",
        icon: "quote-right",
        bg: "#C5F442"
    },
    {
        name: "Generate Invoice",
        route: "GenerateInvoice",
        icon: "file-text-o",
        bg: "#C5F442"
    },
    {
        name: "Manage Service/Cost",
        route: "ManageService",
        icon: "stack-exchange",
        bg: "#C5F442"
    },
    {
        name: "Q & A",
        route: "QnA",
        icon: "question-circle",
        bg: "#C5F442"
    },
    {
        name: "Accounting/Payments",
        route: "AcceptBooking",
        icon: "rupee",
        bg: "#C5F442"
    },
    {
        name: "Refer & Earn",
        route: "Refer",
        icon: "share",
        bg: "#C5F442"
    },
    {
        name: "Wallet",
        route: "Wallet",
        icon: "money",
        bg: "#C5F442"
    }
];

const drawerNavigatorConfig = {
    initialRouteName: 'Dashboard',
    drawerPosition: 'left',
    drawerBackgroundColor: '#ffc107',
    contentOptions: {
        activeTintColor: '#f9a825',
        itemsContainerStyle: {
            marginVertical: 0,
            backgroundColor: colors.providerColor,
        },
    },
    contentComponent: props => <SideBar {...props} items={drawerOptions} />
};

const quoteStack = createStackNavigator({
    Quotes: QuotesProvider,
}, {
        initialRouteName: 'Quotes'
    });

const orderStack = createStackNavigator({
    Order: Order,
    ServiceOrders: ServiceOrders,
    OrderWorkFlow: OrderWorkFlow,
}, {
        initialRouteName: 'ServiceOrders'
    });

const profileStack = createStackNavigator({
    Details,
    Payment,
    Workshop,
    ChooseService,
    CarTypePricing,
    LocationDetails,
    About
}, {
    initialRouteName: 'About'
})

const routeConfigsProvider = {
    About: profileStack,
    Dashboard: Dashboard,
    ServiceOrders: orderStack,
    GenerateInvoice: GenerateInvoice,
    ManageService: ManageService,
    QnA: QnA,
    Accounting: AcceptBooking,
    Wallet: Wallet,
    Refer: Refer,
    Quotes: quoteStack
};

export default createDrawerNavigator(routeConfigsProvider, drawerNavigatorConfig);