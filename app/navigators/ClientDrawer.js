import { createDrawerNavigator, createStackNavigator } from 'react-navigation';
import React, { Component } from 'react';

import {
    SideBar,
    ProfileClient,
    Dashboard,
    QuotesClient,
    GenerateInvoice,
    Quotation,
    ManageService,
    QnA,
    Accounting,
    Wallet,
    Refer,
    Order,
    BookService,
    OrderWorkFlow,
    SelectLocation,
    SelectCar,
    ServiceOrdersClient,
    QuoteRequests
} from '@screens';


import { colors } from '@constants';
import SelectServiceProvider from '../screens/client/SelectServiceProvider';

const drawerOptions = [
    {
        name: "Profile",
        route: "Profile",
        icon: 'user',
        bg: "#C5F442"
    },
    {
        name: "Dashboard",
        route: "Dashboard",
        icon: "dashboard",
        bg: "#C5F442"
    },
    {
        name: "Book A Service",
        route: "BookService",
        icon: "check-square-o",
        bg: "#C5F442"
    },
    {
        name: "Quotes Requests",
        route: "QuoteRequests",
        icon: "quote-right",
        bg: "#C5F442"
    },
    {
        name: "Service Orders",
        route: "ServiceOrders",
        icon: "list-alt",
        bg: "#C5F442"
    },
    {
        name: "Q & A",
        route: "QnA",
        icon: "question-circle",
        bg: "#C5F442"
    },
    {
        name: "Refer & Earn",
        route: "Refer",
        icon: "share",
        bg: "#C5F442"
    },
    {
        name: "Wallet",
        route: "Wallet",
        icon: "money",
        bg: "#C5F442"
    }
];

const drawerNavigatorConfig = {
    initialRouteName: 'Profile',
    drawerPosition: 'left',
    drawerBackgroundColor: colors.clientColor,
    contentOptions: {
        activeTintColor: '#f9a825',
        itemsContainerStyle: {
            marginVertical: 0,
            backgroundColor: '#fffff',
        },
    },
    contentComponent: props => <SideBar {...props} items={drawerOptions} />
};

const quoteStack = createStackNavigator({
    Quotes: QuotesClient,
    QuoteRequests: QuoteRequests,
}, {
        initialRouteName: 'QuoteRequests'
    });

const orderStack = createStackNavigator({
    Order: Order,
    ServiceOrders: ServiceOrdersClient,
    OrderWorkFlow: OrderWorkFlow,
}, {
        initialRouteName: 'ServiceOrders'
    });

const bookingStack = createStackNavigator({
    BookService: BookService,
    SelectLocation: SelectLocation,
    SelectCar: SelectCar,
    SelectServiceProvider: SelectServiceProvider,
}, {
        initialRouteName: 'BookService'
    });

const routeConfigsProvider = {
    Profile: ProfileClient,
    Dashboard: Dashboard,
    BookService: bookingStack,
    ServiceOrders: orderStack,
    GenerateInvoice: GenerateInvoice,
    ManageService: ManageService,
    QnA: QnA,
    Accounting: Accounting,
    Wallet: Wallet,
    Refer: Refer,
    Quotes: quoteStack
};

export default createDrawerNavigator(routeConfigsProvider, drawerNavigatorConfig);