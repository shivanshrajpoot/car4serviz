import { createStackNavigator } from 'react-navigation';
import Drawer from './ClientDrawer';

export default createStackNavigator(
    {
        Drawer: Drawer
    },
    {
        headerMode: 'none',
        initialRouteName: 'Drawer'
    }
);
