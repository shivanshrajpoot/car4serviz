import { createSwitchNavigator, createAppContainer } from 'react-navigation';
import AuthStack from './AuthStack';
import ProviderStack from './ProviderStack';
import ClientStack from './ClientStack';
import { AuthLoading } from '@screens';

export default createAppContainer(createSwitchNavigator(
    {
        AuthStack: AuthStack,
        AuthLoading: AuthLoading,
        ProviderStack: ProviderStack,
        ClientStack: ClientStack,
    },
    {
        initialRouteName: 'AuthLoading',
    }
));