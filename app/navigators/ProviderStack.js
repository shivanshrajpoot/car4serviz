import { createStackNavigator } from 'react-navigation';
import Drawer from './ProviderDrawer';

import {
    CarTypePricing,
    ChooseService,
    Details,
    LocationDetails,
    Payment,
    ThankYou,
    Workshop,
    AcceptBooking
} from '@screens';

export default createStackNavigator(
    {
        Drawer: Drawer,
        Details: Details,
        Payment: Payment,
        ThankYou: ThankYou,
        Workshop: Workshop,
        ChooseService: ChooseService,
        CarTypePricing: CarTypePricing,
        LocationDetails: LocationDetails,
        AcceptBooking: AcceptBooking,
    },
    {
        headerMode: 'none',
        initialRouteName: 'LocationDetails'
    }
);
