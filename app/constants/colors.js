const providerColor = '#ffa400';
const clientColor = '#1a0037';

export default {
	providerColor,
	clientColor
}