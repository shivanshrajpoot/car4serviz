import {
    NativeModules,
    NativeEventEmitter
} from 'react-native';
import {
    axiosInstance
} from './configureStore';

import { stopRinging, loadSound } from "./ringPhone";
const activityStarter = NativeModules.ActivityStarter;
const eventEmitter = new NativeEventEmitter(activityStarter);

export default async (message) => {
    console.log('Running HEADLESS..................................')
    let Sound = loadSound();
    eventEmitter.addListener(activityStarter.OnBookingRequest, async (response) => {
        console.log('message======>', message);
        const booking_request_id = message.data.id;
        console.log('booking_request_id======>', booking_request_id);
        console.log('response========>', response)
        try {
            if (response == 'ACCEPTED') {
                console.log('Accepted', response);
                console.log('axiosInstance.defaults.baseURL===>', axiosInstance.defaults.baseURL);
                axiosInstance.defaults.headers.common.Authorization = `Bearer ${token}`;
                const res = await axiosInstance.patch(`/service-providers/booking-requests/${booking_request_id}/accept`);
                console.log('res=======>', res);
            } else {
                console.log('Declined', response);
            }
        } catch (error) {
            console.log(error);
        }finally {
            stopRinging(Sound);
        }
    });

    const stringifyMessage = JSON.stringify(message.data);

    return activityStarter.showBookingActivity(stringifyMessage);
}