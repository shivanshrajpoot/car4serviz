import { NativeEventEmitter, NativeModules, AsyncStorage } from 'react-native';
import { axiosInstance } from './configureStore';

const activityStarter = NativeModules.ActivityStarter;

const eventEmitter = new NativeEventEmitter(activityStarter);

eventEmitter.addListener(activityStarter.OnBookingRequest, async (response) => {
	const token = await AsyncStorage.getItem('bearerToken', null);
	if(response == 'accepted'){
		console.log('Accepted', response);
		const response = await axiosInstance.patch(`service-providers/booking-requests/${booking_request_id}/accept`);
		
	}else{
		console.log('Declined', response);
	}
});