import _ from 'lodash';
import axios from 'axios';
import thunk from 'redux-thunk';
import storage from 'redux-persist/lib/storage';
import {
    createStore,
    applyMiddleware
} from 'redux';
import {
    persistStore,
    persistReducer,
    getStoredState
} from 'redux-persist';
import Config from 'react-native-config';
import logger from 'redux-logger'

import reducers from '@reducers';

const persistConfig = {
    key: 'root',
    storage,
};
const persistedReducer = persistReducer(persistConfig, reducers);
export const axiosInstance = axios.create({
    baseURL: `http://3.16.1.44/api`,
    timeout: 10000,
});
console.log('API_URL', axiosInstance.defaults.baseURL);
console.log('ENV', Config.ENV);
let middeleware = null;

// if (Config.ENV === 'DEBUG') {
//     middeleware = applyMiddleware(thunk.withExtraArgument(axiosInstance), logger)
// } else if (Config.ENV === 'PRODUCTION') {
// }
middeleware = applyMiddleware(thunk.withExtraArgument(axiosInstance))

getStoredState(persistConfig)
    .then((state) => {
        const token = _.get(state, 'auth.user.access_token', null);
        if (token !== null) {
            axiosInstance.defaults.headers.common.Authorization = `Bearer ${token}`;
        }
        console.log('Bearer=====>', axiosInstance.defaults.headers.common.Authorization);
    });

export default () => {

    const store = createStore(
        persistedReducer, {},
        middeleware,
    );

    const persistor = persistStore(store);
    return {
        store,
        persistor
    };
};