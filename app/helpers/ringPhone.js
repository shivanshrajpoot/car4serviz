import Sound from 'react-native-sound';
import { Vibration } from "react-native";
Sound.setCategory('Playback');


export const startRinging = (Ringtone) => {
    //Set Infinite Loop
    Ringtone.setNumberOfLoops(-1).setVolume(1);
    Ringtone.play();
    Vibration.vibrate([400, 200, 400, 200], true);
}

export const stopRinging = (Ringtone) => {
    Ringtone.stop(() => {
        Vibration.cancel();
    });
    Ringtone.release();
}

export const loadSound = () => {
    let Ringtone = new Sound('uniphone.wave', Sound.MAIN_BUNDLE, (error) => {
        if (error) {
            console.log('failed to load the sound', error);
            return;
        }
        // loaded successfully
        console.log('duration in seconds: ' + Ringtone.getDuration() + 'number of channels: ' + Ringtone.getNumberOfChannels());
        startRinging(Ringtone);
    });

    return Ringtone;
}