import firebase from 'react-native-firebase';

// Get FCM Token
export const getFcmToken = async () => {
    return await firebase.messaging().getToken();
}

//Check Permission for recieving notifications
export const checkPermission = async () => {
    const enabled = await firebase.messaging().hasPermission();
    return enabled;
}

//Request permission
export const requestPermission = async () => {
    try {
        await firebase.messaging().requestPermission();
        // User has authorised
    } catch (error) {
        throw e;
    }
}

//Create Notification Channel
export const createChannel = () => {
    // Build a channel
    const channel = new firebase.notifications.Android.Channel('test-channel', 'Test Channel', firebase.notifications.Android.Importance.Max)
        .setDescription('My apps test channel')
        .enableVibration(true)
        .setBypassDnd(true)
        .setLockScreenVisibility(firebase.notifications.Android.Visibility.Public);
    firebase.notifications().android.createChannel(channel);
}

export const createTestNotification = () => {
    const notification = new firebase.notifications.Notification()
        .setNotificationId('notificationId')
        .setTitle('My notification title')
        .setBody('My notification body')
        .setData({
            key1: 'value1',
            key2: 'value2',
        })
    notification.android.setChannelId('test-channel');
    notification.android.setAutoCancel(true);
    return notification;
}

export const displayNotification = (notification) => {
    firebase.notifications().displayNotification(notification);
}

