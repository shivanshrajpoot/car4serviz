import React, { Component } from "react";
import { Icon, View, Text, Spinner } from "native-base";
import { StyleSheet } from 'react-native';
import GenerateForm from "react-native-form-builder";
import { connect } from 'react-redux';
import { forEach, get as getLodash } from 'lodash';

import { Theme, PrimaryButton, PrimaryHeader, Toast } from '@components';
import { resetPassword, updateOnboardingState } from '@actions';
import getTheme from '@base-theme-components';  
import { handleError } from '@helpers';

const styles = StyleSheet.create({
    wrapper: {
        flex: 1
    },
    box: {
        flexDirection: 'column',
        justifyContent: 'center',
        alignSelf: 'center'
    },
    forgotPassword: {
        color:'gray',
        textAlign:'center',
        fontSize:17,
        padding:'10%',
        paddingBottom:'0%'
    }

});

const fields = [
    {
        type: "number",
        name: "otp",
        required: true,
        label: "Enter OTP"
    },
    {
        type: "hidden",
        name: "phone",
        required: true,
    },
    {
        type: "password",
        name: "password",
        required: true,
        icon: "lock",
        label: "Enter New Password"
    },
];


class ResetPassword extends Component {
    static navigationOptions = {
        header: null
    };
    
    constructor(props){
        super(props);
        this.state = {
            disableButton: false,
            loading: false,
            resetPassword: {}
        };
        this.submit = this.submit.bind(this)
    }

    submit(){
        const { resetPassword, updateOnboardingState, navigation, userType } = this.props;

        if (this.resetPassword.state.fields.phone.error && this.resetPassword.state.fields.otp.error) {
            return Toast('Please enter valid input.');
        }

        this.setState({
            disableButton: true,
            loading: true
        })

        const formValues = this.resetPassword.getValues();

        formValues.phone =  navigation.getParam('phone', null);
        resetPassword(formValues)
        .then((response) => {
            let message = getLodash(response, 'data.data.message');
            this.setState({
                disableButton: false,
                loading: false
            }, () => Toast(message, userType) )
            setTimeout(function(){
                navigation.navigate('SignIn');
            }, 1000);
        })
        .catch((errorRes) => {
            const { message, validation_errors } = handleError(errorRes);
            let error = message;
            if (validation_errors) {
                forEach(validation_errors, (err, index) => {
                    error = err[0];
                });
            }
            this.setState({
                disableButton: false,
                loading: false
            }, () => Toast(error, userType) )
        });
    }

    render() {
        const { navigation, userType } = this.props;

        return (
            <Theme 
                {...this.props}
                toastRef={ ref => this.toast = ref }
            >
                <PrimaryHeader
                    {...this.props}
                    title="Enter New Password"
                    description="Enter OTP and New Password"
                    goBackTo="ForgotPassword"
                    type={userType}
                />
                <View style={styles.wrapper}>
                    <View>
                        <GenerateForm ref={c => this.resetPassword = c } fields={fields}/>
                    </View>
                    <View style={styles.box}>
                        <PrimaryButton 
                            isLoading={ this.state.loading } 
                            type={ userType } 
                            onPress={ this.submit } 
                            disabled={ this.state.disableButton } 
                            buttonText="RESET" 
                        />
                    </View>
                </View>
            </Theme>
        );
    }
}

const mapStateToProps = ({ auth }) => ({
    user: auth.user,
    userType: auth.userType
});

const mapDispatchToProps = { resetPassword, updateOnboardingState };

export default connect(mapStateToProps, mapDispatchToProps)(ResetPassword);
