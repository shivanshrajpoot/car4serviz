import React, { Component } from "react";
import {
  Container,
  Text,
  Button,
  Content,
  View,
  StyleProvider,
  Icon
} from "native-base";
import { Image, StyleSheet } from "react-native";
import Carousel from "react-native-snap-carousel";
import { connect } from 'react-redux';
import { debounce } from 'lodash';

import getTheme from "@base-theme-components";
import { Theme, PrimaryButton } from '@components';
import { updateUserType, logoutUser } from '@actions';

const styles = StyleSheet.create({
    backgroundImage: {
        height: '40%',
        marginLeft: 20,
        marginRight: 20,
        width: 400
    },
    headerText: {
        marginBottom: 10,
        width: null,
        textAlign: "center",
        fontSize: 20
    }
});

class AuthScreen extends Component {

    static navigationOptions = {
        header: null
    };
    
    render() {
        const { navigation, userType } = this.props;
        return (
            <Theme
                {...this.props}
                toastRef={ ref => this.toast = ref }
            >
                <View style={{flex:1, alignItems:'center'}}>
                    <Image
                        source={require("../../assets/images/logo.png")}
                        style={styles.backgroundImage}/>
                    <View style={{ alignSelf: "center" }}>
                        <PrimaryButton type={userType} onPress={() => navigation.navigate("Register")} buttonText="Register" />
                        <PrimaryButton type={userType} onPress={() => navigation.navigate("SignIn")} buttonText="Log in" />
                    </View>
                </View>
            </Theme>
        );
    }
}

const mapStateToProps = ({ auth }) => ({
    userType: auth.userType,
});

const mapDispatchToProps = { updateUserType, logoutUser };

export default connect(mapStateToProps, mapDispatchToProps)(AuthScreen);

