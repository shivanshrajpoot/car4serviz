import React, { Component, Fragment } from "react";
import { AppRegistry, Dimensions } from "react-native";
import {
  View,
  Text,
  Icon,
  Card,
  CardItem,
  List,
  ListItem,
  Body,
} from "native-base";
import { connect } from 'react-redux';

import { Theme, PrimaryHeader } from '@components';


class QuotesClient extends Component {

  state = {
    selectedSlice: {
        label: '',
        value: 0
      },
    labelWidth: 0,
    data:[
            {
                key: 1,
                amount: 50,
                svg: { fill: '#81D4FA' },
                text: 'General Service'
            },
            {
                key: 2,
                amount: 50,
                svg: { fill: '#4DB6AC' },
                text: 'Maintenance & Repair'
            },
            {
                key: 3,
                amount: 40,
                svg: { fill: '#EF6C00' },
                text: 'Dent Repairing & Paiting'
            },
            {
                key: 4,
                amount: 95,
                svg: { fill: '#FDD835' },
                text: 'Car Wash & Detailing'
            },
            {
                key: 5,
                amount: 35,
                svg: { fill: '#81C784' },
                text: 'Road Side Assistance'
            }
        ]
  };
  static navigationOptions = {
    header: null
  };

  onValueChange(value: string) {

    this.setState({
      selected: value
    });
  }

  header = () => {
    return(<PrimaryHeader
                {...this.props}
                description="You will find here all questions submitted by Vendor, please remember they are valid for only two days."
                type={'client'}
                showDrawerButton
            />)
  }

  render() {
    const { navigate } = this.props.navigation;
    let data = this.state.data;

    const ListItems = () => {
      return data.map((item, index) => {
        return(
          <ListItem key={index} onPress={() => navigate('Quotation')}>
            <Card style={{ width: (Dimensions.get('window').width * 90 / 100)}} >
              <CardItem header>
                <Text>New Service Request</Text>
              </CardItem>
              <CardItem>
                <Body>
                  <Text>
                    My car has a dent on the right back door and I want to get it fixed.
                  </Text>
                </Body>
              </CardItem>
              <CardItem footer>
                <Text>28-08-2018 Thursday 11:00 AM to 01:00 PM</Text>
              </CardItem>
           </Card>
          </ListItem>);
      });
    }



    const { userType } = this.props;
    return(
        <Theme 
            {...this.props}
            toastRef={ ref => this.toast = ref }
            header={this.header()}
        >
            <View style={{ flexDirection:'column' }}>
                <List>
                    <ListItems/>
                </List>
            </View>
        </Theme>
    );
  }
}

const mapStateToProps = ({ auth }) => ({
    userType: auth.userType,
});

const mapDispatchToProps = {  };

export default connect(mapStateToProps, mapDispatchToProps)(QuotesClient);
