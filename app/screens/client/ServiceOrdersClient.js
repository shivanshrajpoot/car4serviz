import React, { Component, Fragment } from "react";
import { AppRegistry, Dimensions } from "react-native";
import {
  View,
  Text,
  Icon,
  Picker,
  Form,
  Tab,
  Tabs,
  Card,
  CardItem,
  List,
  ListItem,
  Body,
  Header,
  Left
} from "native-base";
import GenerateForm from "react-native-form-builder";
import { StackActions, NavigationActions } from "react-navigation";
import { Theme, PrimaryHeader } from '@components';
import { connect } from 'react-redux';

class ServiceOrdersClient extends React.PureComponent {

  state = {
  	loading: false,
    selectedSlice: {
        label: '',
        value: 0
      },
    labelWidth: 0,
    data:[
            {
                key: 1,
                amount: 50,
                svg: { fill: '#81D4FA' },
                text: 'General Service'
            },
            {
                key: 2,
                amount: 50,
                svg: { fill: '#4DB6AC' },
                text: 'Maintenance & Repair'
            },
            {
                key: 3,
                amount: 40,
                svg: { fill: '#EF6C00' },
                text: 'Dent Repairing & Paiting'
            },
            {
                key: 4,
                amount: 95,
                svg: { fill: '#FDD835' },
                text: 'Car Wash & Detailing'
            },
            {
                key: 5,
                amount: 35,
                svg: { fill: '#81C784' },
                text: 'Road Side Assistance'
            }
        ]
  };
	static navigationOptions = {
		header: null
	};

	onValueChange(selected: string) {
		this.setState({ selected });
	}

	_header = () => (
	    <PrimaryHeader
			{...this.props}
			showDrawerButton
			type="client"
			title="Service Orders"
			description="You will find your orders here. It is to keep you updated of your upcoming orders..."
	    />
	)

	

  	render() {
	    let data = this.state.data;
	    const { navigate } = this.props.navigation;
	    ListItems = () => {
	      return data.map((item, index) => {
	        return(
	          <ListItem key={index} onPress={() => navigate('Order')}>
	            <Card style={{ width: (Dimensions.get('window').width * 90 / 100)}} >
	              <CardItem header>
	                <Text>New Service Request</Text>
	              </CardItem>
	              <CardItem>
	                <Body>
	                  <Text>
	                    My car has a dent on the right back door and I want to get it fixed.
	                  </Text>
	                </Body>
	              </CardItem>
	              <CardItem footer>
	                <Text>28-08-2018 Thursday 11:00 AM to 01:00 PM</Text>
	              </CardItem>
	           </Card>
	          </ListItem>);
	      });
	    }

	    return (
	    	<Theme
	    		loading={this.state.loading}
                header={this._header()}
	    	>
	    		<View style={{ flexDirection:'column' }}>
					<Tabs>
						<Tab heading="Pending">
							<ListItems/>
						</Tab>
						<Tab heading="Upcoming">
							<ListItems/>
						</Tab>
						<Tab heading="Ongoing">
						 	<ListItems/>
						</Tab>
						<Tab heading="Completed">
						  	<ListItems/>
						</Tab>
					</Tabs>
	            </View>
	    	</Theme>
	    );
  	}
}

export default connect(() => ({}), {})(ServiceOrdersClient);