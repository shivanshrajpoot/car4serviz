import React, { Component, Fragment } from "react";
import { 
    ListItem,
    Icon,
    Right,
    Switch,
    Button,
    Label,
    View,
    Form,
    Input,
    Body,
    Item,
    Picker,
    Textarea,
    CheckBox,
    DatePicker
} from "native-base";

import SimpleIcon from 'react-native-vector-icons/FontAwesome';

import DateTimePicker from 'react-native-modal-datetime-picker';

import Spinner from "react-native-loading-spinner-overlay";
import { Modal, StyleSheet, Text, ScrollView, Image, TouchableHighlight, TouchableOpacity } from "react-native";
import { connect } from 'react-redux';

import PhotoUpload from "react-native-photo-upload";

import { Theme, PrimaryHeader, PrimaryFab, Toast } from '@components';

import { getServices, chooseServices, setJobCardDetails, addImagesToJobCard, sendBookingRequest } from '@actions';

import { get as getLodash, forEach, find } from 'lodash';

import { handleError } from '@helpers';

import { colors } from '@constants';
import { FlatList } from "react-native-gesture-handler";

import ImageViewer from 'react-native-image-zoom-viewer';

const mapStateToProps = ({ common, client }) => ({
    services: common.services,
    vehicles : client.vehicles,
    job_card: getLodash(client, 'job_card', {})
});

const mapDispatchToProps = { 
	getServices,
	chooseServices,
	setJobCardDetails,
	addImagesToJobCard,
	sendBookingRequest
};

class SelectCar extends Component {

    static navigationOptions = { header: null };

    state = {
        loading: false,
        is_urgent: false,
        isDateTimePickerVisible: false,
        showImagesModal : false,
        service_datetime: null,
        vehicle : null,
        vehicle_id : null,
        service_location : 1,
        remark : null,
        images : []
    };

    componentDidMount(){
      	const { job_card: {
				vehicle, 
				service_location, 
				remark, 
				images, 
				service_datetime 
			} 
		} = this.props;
		this.setState({
			vehicle,
			vehicle_id: vehicle ? vehicle.id : null,
			service_location,
			remark,
			images,
			service_datetime,
		});
    }

    onValueChangeCar(vehicle_id) {
		const { vehicles, setJobCardDetails } = this.props;
		let vehicle = find(vehicles, { id: vehicle_id });
		this.setState({ vehicle, vehicle_id});
		setJobCardDetails({ vehicle, vehicle_id });
    }

    getFormattedDateForDatabase(date){
      let dateTimeString = date.getFullYear()+ '-';
      dateTimeString = date.getMonth() < 10 ? dateTimeString + '0' : dateTimeString;
      dateTimeString = dateTimeString + date.getMonth() + '-';
      dateTimeString = date.getDay() < 10 ? dateTimeString + '0' : dateTimeString;
      dateTimeString = dateTimeString + date.getDay()+' ';
      dateTimeString = date.getHours() < 10 ? dateTimeString + '0' : dateTimeString;
      dateTimeString = dateTimeString + date.getHours() + ':';
      dateTimeString = date.getMinutes() < 10 ? dateTimeString + '0' : dateTimeString;
      dateTimeString = dateTimeString + date.getMinutes();

      return dateTimeString;
    }

    onValueChangeLocation(service_location) {
		this.setState({ service_location });
		this.props.setJobCardDetails({ service_location });
    }

    toggleUrgent(){
		const { is_urgent, service_datetime } = this.state;
		let toggleUrgent = {
			is_urgent: !is_urgent,
			service_datetime : null
		};

		this.setState({...toggleUrgent});
		this.props.setJobCardDetails({...toggleUrgent});
    };

    _hideDateTimePicker = () => this.setState({ isDateTimePickerVisible: false });
 
    _handleDatePicked = (date) => {
    	let service_datetime = this.getFormattedDateForDatabase(date);
		this.setState({ service_datetime });
		this.props.setJobCardDetails({ service_datetime });
		this._hideDateTimePicker();
    };

    onChangeRemark = (remark) => {
		this.setState({ remark });
		this.props.setJobCardDetails({ remark });
    };

    _selectDate = () => this.setState({ isDateTimePickerVisible: true });

    _getDisplayNameForVehicle = vehicle => vehicle.brand.name + " " + vehicle.model.name + " (" + vehicle.registration_number+")";

    _saveData  = async () => {
		const { navigation, sendBookingRequest } = this.props;
		let { job_card } = this.props;

		if(!this._checkForValidationError()) return false;

		if (job_card.services.length > 1) {
			try{
				let address = job_card.address;
		        let addressString = `${address.address_line_1},${address.address_line_2},${address.city},${address.state},${address.postal}`;
		        job_card.address = addressString;
		        delete job_card.vehicle;
		        delete job_card.service_provider_id;

				await sendBookingRequest(job_card);
				Toast('Your Quote Request Was Created.');
				navigation.popToTop();
			}catch(e){
				console.log(getLodash(e, 'response', null))
				Toast('Something went wrong please try again');
			}
		}else{
			navigation.navigate('SelectServiceProvider');
		}

    }

    _checkForValidationError = () => {
      	const { vehicle, service_location, remark, service_datetime, is_urgent } = this.state;
		if(!vehicle) {
			Toast('Select a Vehicle');
			return false;
		}
		if (!service_location) {
			Toast('Choose a location');
			return false;
		}
		if (!(service_datetime || is_urgent)) {
			Toast('Select a Date for service');
			return false;
		}
		return true;
    }

    addImage = (imageData) => {
      
		if(!imageData) return;

		let { images } = this.state;
		let { addImagesToJobCard } = this.props;

		images.push(`data:image/png;base64,${imageData}`);

		this.setState({images});
		addImagesToJobCard(images);
      
    };

    removeImage(index){
      let { images } = this.state;
      let { addImagesToJobCard } = this.props;

      if (images.length < index+1) return;
      images.splice(index, 1);

      this.setState({ images });

      addImagesToJobCard(images);
    }

    renderImages = ({ item, index }) => (
		<View style={styles.imageContainer}>

			<TouchableHighlight onPress={() => this.setState({ showImagesModal : true })}>
				<Image style={styles.images} source={{ uri: item }} />
			</TouchableHighlight>

			<Button onPress={() => {this.removeImage(index)}} style={styles.imageRemoveButton}>
				<Text style={styles.imageCloseText}>x</Text>
			</Button>

		</View>
    )

    hideImagesModal = () => this.setState({ showImagesModal: false });

    _header = () => (
        <PrimaryHeader
			{...this.props}
			showDrawerButton
			type="client"
			title="Select Your Car"
			description="Upload images/videos of your car describing the problem you are facing."
        />
    )

    render() {
      const { vehicles, job_card } = this.props;
      const { images, showImagesModal} = this.state;
        return (
            <Theme 
                loading={this.state.loading}
                header={this._header()}
                fab={<PrimaryFab type="client" onPress={ () => this._saveData } />}
            >
                <ScrollView style={styles.container}>
                  <Picker
                    renderHeader={backAction =>
                      <Header style={{ backgroundColor: "#f44242" }}>
                        <Left>
                          <Button transparent onPress={backAction}>
                            <Icon name="car" style={{ color: colors.clientColor }} />
                          </Button> 
                        </Left>
                        <Body style={{ flex: 3 }}>
                          <Title style={{ color: colors.clientColor }}>Please Select Your Car</Title>
                        </Body>
                        <Right />
                      </Header>}
                      mode="dropdown"
                      iosIcon={<Icon name="ios-arrow-down-outline" />}
                      selectedValue={this.state.vehicle_id}
                      onValueChange={this.onValueChangeCar.bind(this)}>
                    <Picker.Item label="Select Your Car" value="0" />
                    {vehicles.map((vehicle, index) => {
                      return (<Picker.Item label={this._getDisplayNameForVehicle(vehicle)} value={vehicle.id} key={vehicle.id} />)
                    })}
                  </Picker> 
                  <Picker
                    renderHeader={backAction =>
                      <Header style={{ backgroundColor: "#f44242" }}>
                        <Left>
                          <Button transparent onPress={backAction}>
                            <Icon name="arrow-back" style={{ color: colors.clientColor }} />
                          </Button>
                        </Left>
                        <Body style={{ flex: 3 }}>
                          <Title style={{ color: colors.clientColor }}>Please Select A Place</Title>
                        </Body>
                        <Right />
                      </Header>}
                      mode="dropdown"
                      iosIcon={<Icon name="ios-arrow-down-outline" />}
                      selectedValue={this.state.service_location}
                      onValueChange={this.onValueChangeLocation.bind(this)}>
                    
                    <Picker.Item label="Select A Place" value="0" />
                    <Picker.Item label="Vendor's Location" value="1"/>
                    <Picker.Item label="Your Location" value="2" />

                  </Picker>

                  <Textarea 
                    style={{ marginVertical: 20 }}
                    rowSpan={5}
                    value = {this.state.remark}
                    onChangeText={(remark) => this.onChangeRemark(remark)}                
                    bordered 
                    placeholder="Add your comments and describe your issues with the car" />

                  <PhotoUpload onPhotoSelect={this.addImage} >
                      <View style={styles.photoUploadContainer}>
                        <SimpleIcon name='file-picture-o' style={{fontSize: 20, marginRight: 10}} />
                        <Text>Add Images/Videos</Text>
                      </View>
                  </PhotoUpload>
                  	<FlatList
						data={images}
						keyExtractor={(item) => item}
						renderItem={this.renderImages}
						contentContainerStyle={styles.imagesListContainer}
						style = {{ flex:1, flexDirection: 'row' }}
                   	/>

                  	{ job_card.services.length == 1 &&  
                  		<View style={styles.isUrgentContainer}>
							<TouchableOpacity onPress={this.toggleUrgent.bind(this)} >
								<Text style={{ fontSize:18 }}>Is this Urgent ?</Text>
							</TouchableOpacity>
							<CheckBox checked={this.state.is_urgent} color={ colors.clientColor } style={styles.urgentCheckbox} onPress={this.toggleUrgent.bind(this)}/>
                        </View>}
                    {this.state.is_urgent && <Text style={styles.urgentInfoMessage}>We will notify all the nearby garages and get you a booking on urgent basis  </Text>}
                  <DateTimePicker
                    isVisible={this.state.isDateTimePickerVisible}
                    onConfirm={this._handleDatePicked}
                    onCancel={this._hideDateTimePicker}
                    mode="datetime"
                    is24Hour={false}
                  />
                  { !this.state.is_urgent && 
                      <TouchableOpacity style={styles.datePickerButton} onPress={this._selectDate}>
                    		<View style={styles.dateTimeContainer}>
	                      		<Text style={{ fontSize: 18 }} > {this.state.service_datetime ? this.state.service_datetime : 'Pick a date for Service'}</Text>
	                          	<SimpleIcon name='calendar-plus-o' style={styles.calenderIcon} />
                    		</View>
                      </TouchableOpacity>
                  }
                </ScrollView>
            <Modal 
            	visible={showImagesModal} 
            	transparent={true} 
            	onRequestClose={this.hideImagesModal}>

              	<ImageViewer imageUrls={images.map(image_uri => { return { url: image_uri } })}/>
            </Modal>
            </Theme>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(SelectCar);

const styles = StyleSheet.create({
  container: {
    padding: 30,
    flex: 1,
    marginBottom : 50
  },
  imagesListContainer : {
    flex : 1,
    flexDirection : 'row',
    marginVertical: 10
  },
  images : {
    width: 80,
    height: 80,
  },
  imageRemoveButton : {
    position: 'absolute',
    right: -8,
    top: -8,
    
    padding : 1,
    width: 20,
    height:20,
    borderRadius: 10,
    backgroundColor : "#ffffff",
    borderColor : "#555555",
    alignItems : "center",
  },
  imageContainer : {
    width: 80,
    height: 80,
    resizeMode: "cover",
    borderWidth: 1,
    borderColor: 'gray',
    marginRight: 20,
    marginTop: 20,
    backgroundColor : "#dddddd"
  },
  imageCloseText : {
    marginLeft: 5,
    marginBottom: 4,
  },
  dateTimeContainer : {
    flex: 1,
    flexDirection: "row",
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  datePickerButton : {
    backgroundColor : '#ffffff',
    marginRight: 5,
    marginVertical: 10,
    borderWidth: 2,
    borderColor: colors.clientColor,
    padding: 10
  },
  calenderIcon : {
    color : colors.clientColor,
    fontSize : 30
  },
  isUrgentContainer : {
    flex : 1,
    flexDirection : "row",
    marginVertical: 25,
    alignItems: 'center',
    justifyContent: 'space-between', 
  },
  urgentCheckbox : {
    marginLeft: 0,
    marginRight : 20,
  },
  photoUploadContainer : {
    flex : 1,
    flexDirection : "row"
  },
  urgentInfoMessage : {
    fontSize : 15,
    opacity : 0.6,
    marginTop : 10
  }

});