
import React, { Component, Fragment } from "react";
import { FlatList, Dimensions } from "react-native";
import {
  View,
  Text,
  Icon,
  Card,
  CardItem,
  List,
  ListItem,
  Body,
} from "native-base";
import { connect } from 'react-redux';

import { Theme, PrimaryHeader, Toast } from '@components';

import { getQuoteRequests } from '@actions';

import { get } from 'lodash';

import { colors } from '@constants';

const NoDataFound = () => (
	<Card>
		<CardItem header>
			<Text>No Data Found</Text>
		</CardItem>
	</Card>
);

const QuoteRequest = (props) => (
	<ListItem onPress={() => props.navigation.navigate('Quotation')}>
		<Card style={{ width: (Dimensions.get('window').width * 90 / 100)}} >
			<CardItem header>
				<Text>{ `${props.request.vehicle.brand.name} ${props.request.vehicle.model.name}` }</Text>
				<Text>{ `${props.request.vehicle.variant.name}` }</Text>
			</CardItem>
			<CardItem>
				<Body>
					<Text>
						{ props.request.remark }
					</Text>
				</Body>
			</CardItem>
			<CardItem footer>
				<Text>{ props.request.service_datetime }</Text>
			</CardItem>
		</Card>
	</ListItem>
)

class QuoteRequests extends React.PureComponent {

	static navigationOptions = {
		header: null
	}

	state = {
		refreshing: false,
		requests: []
	}

	async componentDidMount() {
		await this._fetchRequests()
	}

	_fetchRequests = async () => {
		this.setState({ refreshing: true })
		const { getQuoteRequests } = this.props;
		try{
			const requests = await getQuoteRequests();
			this.setState({ requests });
		}catch(e){
			Toast('Something Went Wrong');
			console.log(e.response);
		}
		this.setState({ refreshing: false })
	}

	header = () => {
		return(<PrimaryHeader
		            {...this.props}
		            description="You will find here all Quotations submitted by Vendor, please remember they are valid for only two days."
		            type={'client'}
		            showDrawerButton
		        />)
	}

	_renderQuoteRequest = ({item}) => (<QuoteRequest request={item} {...this.props}/>);

	_keyExtractor = (item, index) => `${item.id}`;

	render() {
		const { requests } = this.props;
		return(
		    <Theme 
		        {...this.props}
		        header={this.header()}
		    >
	            <FlatList
	            	data={ requests }
	            	extraData={ this.state.refreshing }
	            	ListEmptyComponent={ <NoDataFound/> }
	            	renderItem={ this._renderQuoteRequest }
	            	keyExtractor={ this._keyExtractor }
	            	refreshing={this.state.refreshing}
	            	onRefresh={ this._fetchRequests }
	            />
		    </Theme>
		);
	}
}

const mapStateToProps = ({ client: { quotes } }) => ({
	requests: quotes.requests
});

const mapDispatchToProps = { getQuoteRequests };

export default connect(mapStateToProps, mapDispatchToProps)(QuoteRequests);
