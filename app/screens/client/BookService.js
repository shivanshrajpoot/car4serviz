import React, { Component, Fragment } from "react";
import { 
    ListItem,
    Icon,
    Right,
    Switch,
    Button,
    Label,
    View,
    Form,
    Input,
    Body,
    Item
} from "native-base";

import Spinner from "react-native-loading-spinner-overlay";
import { Modal, StyleSheet, Text, ScrollView } from "react-native";
import { connect } from 'react-redux';

import { Theme, PrimaryHeader, PrimaryFab, Toast } from '@components';

import { getServices, chooseServices } from '@actions';

import { get as getLodash, forEach, filter, intersection, find, flatMap } from 'lodash';

import { handleError } from '@helpers';

import { colors } from '@constants';

const resetAction = route =>
  StackActions.reset({
    index: 0,
    actions: [NavigationActions.navigate({ routeName: route })]
  });

var styles = StyleSheet.create({
  container: {
    // paddingTop: 30,
    flex: 1
  }
});

class BookService extends Component {

    static navigationOptions = { header: null };

    state = {
        services: [],
        formValues: { services: [], service_categories: [] },
        user: {},
        selectedService: {
            categories: []
        },
        modalOpened: false,
        loading: false
    };

    saveData = () => {
        if (filter(this.state.formValues.services, ['value', true]).length === 0) {
            return alert('Please select a service');
        }
        const { chooseServices, navigation } = this.props;

        let payload = {
            services: this.state.formValues.services.filter(e => e.value),
            service_subcategories: this.state.formValues.service_categories.map((sc) => { return { id: sc }})
        };

        chooseServices(payload);

        navigation.navigate('SelectLocation');
    }

    mapServices = (formValues, services) => {
        formValues["services"] = services.map(service => ({
                id: service.id,
                value: false,
                categories: service.categories
            })
        );
        this.setState({ services, formValues, loading: false });
    }


    componentDidMount = async () => {
        const { getServices, services } = this.props;
        console.log("Component Mounted=========>BookService", services);
        let { formValues } = this.state;
        // If services are already in store don't fetch
        if (services && services.length !== 0) {
            this.mapServices(formValues, services);
            return;
        }

        this.setState({loading: true});

        const fetchedServices = await getServices();
        this.mapServices(formValues, fetchedServices);
        this.setState({ loading: false });
    }

    cancel = () => {
        let { selectedService } = this.state;
        this._onValueChange(false, selectedService.id-1);
    }

    save() {
        let { formValues, selectedService } = this.state;
    	let selected = intersection(formValues.service_categories, 
    						flatMap(selectedService.categories, (cat) => cat.id));
    	if (selected.length === 0) {
    		return this.cancel();
    	}
        this.setState({ modalOpened: false }, () => {
        	console.log('SAVED',this.state);
        });
    }

    _onValueChange = async (value, i) => {
        let formValuesCpy = this.state.formValues;
        formValuesCpy.services[i].value = value;
        this.setState({ formValues: formValuesCpy }, () => {
	        let { formValues, services } = this.state;
	        console.log('formValues', formValues);

	        if (value && i === 0) {
	            if(!formValues.service_categories.length) {
	            	console.log('adding')
	                formValues.service_categories = formValues.service_categories.concat(
	                    services[i].categories.map(c => c.id)
	                );
	            }

	        } else {
	           	console.log('removing')
	            formValues.service_categories = formValues.service_categories.filter(service_category_id => {
	                return find(services[i].categories, { id: service_category_id  }) != undefined ? false : true;
	            });
	           	console.log('removing', formValues.service_categories)
	        }
	        let selectedService = value ? formValues.services[i] : { categories: [] };
	        this.setState({
	            formValues,
	            selectedService: selectedService,
	            selectedCategoriesCpy: JSON.parse(JSON.stringify(formValues.service_categories)),
	            modalOpened: i === 0 ? false: value
	        }, () => {
	        	console.log('this.state', this.state);
	        });
        });
    }

    render() {
        return (
            <Theme 
                loading={this.state.loading}
                toastRef={ ref => this.toast = ref }
            >
                <PrimaryHeader
                    {...this.props}
                    showDrawerButton
                    type="client"
                    title="Choose your Services"
                    description="Choose which service you want."
                    />
                <ScrollView>
                {this.state.services.map((service, i) => {
                  return (
                    <View key={i} style={{marginBottom: 15}}>
                      <ListItem>
                        <Body>
                          <Label>{service.name}</Label>
                        </Body>
                        <Right>
                            <Switch
                                value={this.state.formValues.services[i].value}
                                disabled={i === 4}
                                onValueChange={value => this._onValueChange(value, i)}
                            />
                        </Right>
                      </ListItem>
                    </View>
                  );
                })}
              </ScrollView>
              <Modal
                animationType="slide"
                transparent={false}
                onRequestClose={this.cancel}
                visible={this.state.modalOpened}
                dimissable={false}
              >
                <View>
                  <View>
                    <View
                      style={{
                        backgroundColor: colors.clientColor,
                        paddingTop: 30,
                        paddingBottom: 10,
                        paddingLeft: 20
                      }}
                    >
                      <Text style={{ color: "white", fontSize: 20 }}>
                        Select what you want in this category
                      </Text>
                    </View>
                        <ScrollView style={{height:'78%'}}>
                          <View>
                            <View>
                              {this.state.selectedService.categories.map((cateogry, i) => {
                                return (
                                  <ListItem key={i}>
                                    <Body>
                                      <Label>{cateogry.name}</Label>
                                    </Body>
                                    <Right>
                                      <Switch
                                        value={this.state.formValues.service_categories.indexOf(cateogry.id) >= 0}
                                        onValueChange={value => {
                                        		console.log('value', value);
                                        		console.log('cateogry', cateogry);
	                                        	let formValues = this.state.formValues;
	                                          	if (value) {
	                                        		formValues.service_categories.push(cateogry.id);
	                                          	} else {
	                                            	formValues.service_categories = formValues.service_categories.filter(e => e != cateogry.id);
	                                          	}
	                                          	this.setState({ formValues });
                                        	}
                                    	}
                                      />
                                    </Right>
                                  </ListItem>
                                );
                              })}
                            </View>
                          </View>
                        </ScrollView>
                        <View
                          style={{
                            flexDirection: "row",
                            justifyContent: "space-around",
                            marginTop: "3%"
                          }}
                        >
                          <Button
                            style={{ width: "45%", backgroundColor: colors.clientColor }}
                            onPress={this.save.bind(this)}
                          >
                            <Text
                              style={{
                                textAlign: "center",
                                width: "100%",
                                color: "white"
                              }}
                            >
                              <Icon name="done-all" />
                            </Text>
                          </Button>

                          <Button
                            style={{ width: "45%", textAlign: "center", backgroundColor: colors.clientColor }}
                            onPress={this.cancel}
                          >
                            <Text
                              style={{
                                textAlign: "center",
                                width: "100%",
                                color: "white"
                              }}
                            >
                              <Icon name="close" />
                            </Text>
                          </Button>
                        </View>
                  </View>
                </View>
              </Modal>
              <PrimaryFab type="client" onPress={ () => this.saveData } />
            </Theme>
        );
    }
}

const mapStateToProps = ({ common }) => ({
    services: common.services
});

const mapDispatchToProps = { getServices, chooseServices };

export default connect(mapStateToProps, mapDispatchToProps)(BookService);