import React, { Component, Fragment } from "react";
import {
  View,
  Text,
  Icon,
  Card,
  CardItem,
  Item,
  Body,
  Form,
  Input,
  Button,
  Fab,
  Picker
} from "native-base";
import { connect } from 'react-redux';
import GenerateForm from "react-native-form-builder";
import PhotoUpload from "react-native-photo-upload";
import { Image, Modal, StyleSheet, ScrollView, List  } from "react-native";
import { forEach, get, filter } from 'lodash';

import { Theme, PrimaryHeader, PrimaryButton, Toast } from '@components';
import { authValidator } from '@validators';
import { saveClientProfile, getBrands, getModels, getVariants, addVehicle, logoutUser, getAllVehicles, getServices } from '@actions';
import { handleError } from '@helpers';
import { colors } from '@constants';
import { FlatList } from "react-native-gesture-handler";

const fields = [
    {
        type: "text",
        name: "first_name",
        required: true,
        icon: "person",
        label: "First Name"
    },
    {
        type: "text",
        name: "last_name",
        required: true,
        icon: "person",
        label: "Last Name"
    },
    {
        type: "email",
        name: "email",
        required: true,
        icon: "mail",
        label: "Email"
    },
];

const carFields = [
    {
        type: "text",
        name: "registration_number",
        required: true,
        label: "Registration Number"
    },
    {
        type: "number",
        name: "purchase_year",
        required: true,
        label: "Purchase Year"
    },
    // {
    //     type: "picker",
    //     name: "fuel_type",
    //     required: true,
    //     label: "Fuel Type",
    //     options: ['Petrol', 'Diesel', 'CNG', "Battery"],
    //     props: {
    //         mode:"dropdown",
    //         style: {marginHorizontal: 20}
    //     }
    // },
];

const fuelTypes = [
    {
        id: 1,
        name: 'Petrol',
    },
    {
        id: 2,
        name: 'Diesel',
    },
    {
        id: 3,
        name: 'CNG',
    },
    {
        id: 4,
        name: 'Battery',
    }
];

const userType = 'client';

class ProfileClient extends Component {
    static navigationOptions = { header: null };

    constructor(props){
        super(props);
        this.state = {
            fabActive:false,
            cars: [],
            loading: false,
            brands: [],
            models: [],
            variants: [],
            disableButton: true,
            formValues: {
              profile_photo: ""
            },
            selectedBrand: 0,
            selectedModel: 0,
            selectedVariant: 0,
            selectedFuelType: 0,
            carType: null,
            modalOpened: false,
            vehicleListModalOpened : false
        }
        this.submit = this.submit.bind(this);
        this.addVehicle = this.addVehicle.bind(this);
    }

    async componentWillMount(){
        const { getBrands, getAllVehicles ,logoutUser, getServices } = this.props;
        getAllVehicles();
        await getBrands()
            .then((response) => {
                let brands = get(response, 'data.data', [])
                console.log('brands', brands);
                this.setState({brands: brands});
            })
            .catch((errorRes) => {

            });
        getServices();
        

    }

    componentDidMount(){
        const { first_name, last_name, email, profile_photo } = this.props;
        let { formValues } = this.state;
        formValues.profile_photo = profile_photo
        this.profile.setValues({ first_name, last_name, email, formValues });

        this.setState({ disableButton: true });
    }

    onPhotoSelect(avatar){
         if (avatar) {
            let formValues = this.state.formValues;
            formValues.profile_photo = avatar;
            this.setState({ formValues });
        }
    }

    header = () => {
        return(<PrimaryHeader
                    {...this.props}
                    description="Let us know your basic information, you can manage your cars by tapping on car button"
                    type={'client'}
                    title="Welcome"
                    showDrawerButton
                />)
    }

    async submit(){
        let formValues = {...this.profile.getValues(), ...this.state.formValues};
        const isValid = await authValidator.clientProfileValidator.isValid(formValues);

        console.log('formValues', formValues)
        if (isValid == false) return Toast('Please enter valid input.', userType);
        
        this.setState({
            disableButton: true,
            loading: true 
        })

        Object.keys(formValues).forEach((key) => (formValues[key] == null) && delete formValues[key]);

        const { saveClientProfile, logoutUser } = this.props;


        saveClientProfile(formValues)
            .then((response) => {
                this.setState({
                    disableButton: false,
                    loading: false 
                })
                Toast('Update Successfuly.');
            })
            .catch((errorRes) => {
                const { message, validation_errors } = handleError(errorRes);
                let error = message;
                if (validation_errors) {
                    forEach(validation_errors, (err, index) => {
                        error = err[0];
                    });
                }
                this.setState({
                    disableButton: false,
                    loading: false
                }, () => Toast(error, userType) )
            })
    }

    onBrandSelect(brandId) {
        if (brandId === 0) {
            this.setState({models: []})
            return;
        }
        const { getModels } = this.props;
        this.setState({selectedBrand: brandId}, () => {
            getModels(brandId)
                .then((response) => {
                    let models = get(response, 'data.data')
                    this.setState({models: models});
                })
                .catch((errorRes) => {
                    
                })
        });

    }

    onModelSelect(modelId) {
        if (modelId === 0) {
            this.setState({variants: []})
            return;
        }
        const { getVariants } = this.props;
        this.setState({selectedModel: modelId}, () => {
            getVariants(this.state.selectedBrand, modelId)
                .then((response) => {
                    let variants = get(response, 'data.data')
                    this.setState({variants: variants});
                })
                .catch((errorRes) => {

                })
        });

    }

    onVariantSelect(variantId){
        if (variantId === 0) return;
        let variant = filter(this.state.variants, { id: variantId })[0];
        this.setState({selectedVariant: variantId, carType: variant.car_type_id})
    }


    addVehicle(){
        this.setState({loading: true})
        const { addVehicle } = this.props;
        let formValues = {
            ...this.addCar.getValues(),
            ...{car_variant_id: this.state.selectedVariant, car_type_id: this.state.carType },
            ...{ fuel_type: this.state.selectedFuelType }
        };

        addVehicle(formValues)
            .then(() => {
                this.setState({modalOpened: false, loading: false})
                Toast('Added Successfuly')
            })
            .catch((errorRes) => {
                const { message, validation_errors } = handleError(errorRes);
                let error = message;
                if (validation_errors) {
                    forEach(validation_errors, (err, index) => {
                        error = err[0];
                    });
                }
                this.setState({
                    loading: false,
                    modalOpened: false
                }, () => Toast(error, userType) )
            })


    }

    fieldValueChanged = () => {
        this.setState({ disableButton: false});
    }

    _renderVehicleListItem = ({item}) => (
        <Card>                    
            <CardItem header>
              <Text>{item.brand.name} {item.model.name}</Text>
            </CardItem>
            
            <CardItem>
                <Body style={{
                    flex: 1,
                    flexDirection: 'column'
                }}>
                    <Text>
                        {item.variant.name}
                    </Text>
                    <Text>
                        Registration Number : {item.registration_number}
                    </Text>
                </Body>
            </CardItem>

            <CardItem footer>
                <Text></Text>
            </CardItem>
        </Card>

    );


    render() {
        const { navigate } = this.props.navigation;
        let models = this.state.models;

        const { userType, vehicles } = this.props;
        return(
            <Theme 
                {...this.props}
                toastRef={ ref => this.toast = ref }
                header={this.header()}
                loading={this.state.loading}
            >
                <View style={{ flex: 1 }}>
                    <View>
                    <PhotoUpload
                        ref="photoUpload"
                        containerStyle={{ flexGrow: 0, marginTop: 10 }}
                        onPhotoSelect={avatar => this.onPhotoSelect(avatar)}>
                        <View style={{ position: "absolute", zIndex: 1, right: "10%" }}>
                            <Icon name="create" />
                        </View>
                        <Icon
                            name="person"
                            style={{
                                fontSize: 100,
                                color: "grey",
                                backgroundColor: "#eff0f1",
                                borderRadius: 200 / 2,
                                width: 200,
                                height: 200,
                                padding: 60,
                                alignItems: "center",
                                justifyContent: "center",
                                display: this.state.formValues.profile_photo
                                ? "none"
                                : "flex"
                            }}/>
                          <Image
                            style={{
                                borderRadius: 200 / 2,
                                width: 200,
                                height: 200,
                                padding: 61,
                                alignItems: "center",
                                justifyContent: "center",
                                display: this.state.formValues.profile_photo
                                ? "flex"
                                : "none"
                            }}
                            resizeMode="cover"
                            source={{
                                uri: this.state.formValues.profile_photo || "https://via.placeholder.com/200x200.png"
                            }}
                          />
                    </PhotoUpload>
                        <GenerateForm ref={c => this.profile = c} fields={fields} onValueChange={this.fieldValueChanged} />
                    </View>
                    <View style={{  flexDirection: 'column',
                                    justifyContent: 'center',
                                    alignSelf: 'center' }}>
                    
                    <PrimaryButton
                        type={ userType } 
                        onPress={ this.submit } 
                        disabled={ this.state.disableButton } 
                        buttonText="Save" />
                    </View>
                </View>
                <Fab
                    style={{ backgroundColor: colors.clientColor }}
                    active={this.state.fabActive}
                    direction="down"
                    containerStyle={{  }}
                    position="topRight"
                    onPress={() => this.setState({fabActive: !this.state.fabActive}) }>
                    <Icon name="car" />
                    <Button 
                        style={{ backgroundColor: colors.providerColor }}
                        onPress={() => this.setState({ modalOpened: true, fabActive: !this.state.fabActive}) }
                    >
                      <Icon name="add" />
                    </Button>
                    <Button style={{ backgroundColor: colors.providerColor }}
                        onPress={() => this.setState({ vehicleListModalOpened: true, fabActive: !this.state.fabActive })}>
                      <Icon name="list" />
                    </Button>
                </Fab>
                <Modal
                    animationType="slide"
                    transparent={false}
                    onRequestClose={() => this.setState({ modalOpened: false })}
                    visible={this.state.modalOpened}
                  >
                    <View style={{ flex: 1 }}>
                        <View >
                            <View
                                style={{
                                    backgroundColor: colors.clientColor,
                                    paddingTop: 30,
                                    paddingBottom: 20,
                                    paddingLeft: 20,
                                }}
                            >
                                <Text style={{ color: "white", fontSize: 20 }}>
                                    Add Car
                                </Text>
                            </View>
                            <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
                                <GenerateForm ref={c => this.addCar = c } fields={carFields}/>

                                <Picker
                                    style={{ margin: 20 }}
                                    note
                                    iosIcon={<Icon name="arrow-down" />}
                                    mode="dropdown"
                                    textStyle={{ color: "#5cb85c" }}
                                    itemStyle={{
                                        backgroundColor: "#d3d3d3",
                                        marginLeft: 0,
                                        paddingLeft: 10
                                    }}
                                    itemTextStyle={{ color: '#788ad2' }}
                                    headerStyle={{ backgroundColor: "#b95dd3" }}
                                    headerBackButtonTextStyle={{ color: "#fff" }}
                                    headerTitleStyle={{ color: "#fff" }}
                                    selectedValue={this.state.selectedBrand}
                                    onValueChange={this.onBrandSelect.bind(this)}
                                >
                                    <Picker.Item label="Select Your Brand" value={0} />
                                    { this.state.brands.map((item, index) => {
                                        return(<Picker.Item key={index} label={item.name} value={item.id} />)
                                        })
                                    }
                                </Picker>
                                <Picker
                                    style={{ margin: 20 }}
                                    note
                                    iosIcon={<Icon name="arrow-down" />}
                                    mode="dropdown"
                                    textStyle={{ color: "#5cb85c" }}
                                    itemStyle={{
                                        backgroundColor: "#d3d3d3",
                                        marginLeft: 0,
                                        paddingLeft: 10
                                    }}
                                    itemTextStyle={{ color: '#788ad2' }}
                                    headerStyle={{ backgroundColor: "#b95dd3" }}
                                    headerBackButtonTextStyle={{ color: "#fff" }}
                                    headerTitleStyle={{ color: "#fff" }}
                                    selectedValue={this.state.selectedModel}
                                    onValueChange={this.onModelSelect.bind(this)}
                                >
                                    <Picker.Item label="Select Your Model" value={0} />
                                    { this.state.models.map((item, index) => {
                                        return(<Picker.Item key={index} label={item.name} value={item.id} />)
                                        })
                                    }
                                </Picker>
                                <Picker
                                    style={{ margin: 20 }}
                                    note
                                    iosIcon={<Icon name="arrow-down" />}
                                    mode="dropdown"
                                    textStyle={{ color: "#5cb85c" }}
                                    itemStyle={{
                                        backgroundColor: "#d3d3d3",
                                        marginLeft: 0,
                                        paddingLeft: 10
                                    }}
                                    itemTextStyle={{ color: '#788ad2' }}
                                    headerStyle={{ backgroundColor: "#b95dd3" }}
                                    headerBackButtonTextStyle={{ color: "#fff" }}
                                    headerTitleStyle={{ color: "#fff" }}
                                    selectedValue={this.state.selectedVariant}
                                    onValueChange={this.onVariantSelect.bind(this)}
                                >
                                    <Picker.Item label="Select Your Variant" value={0} />
                                    { this.state.variants.map((item, index) => {
                                        return(<Picker.Item key={index} label={item.name} value={item.id} />)
                                        })
                                    }
                                </Picker>
                                <Picker
                                    style={{ margin: 20 }}
                                    note
                                    iosIcon={<Icon name="arrow-down" />}
                                    mode="dropdown"
                                    textStyle={{ color: "#5cb85c" }}
                                    itemStyle={{
                                        backgroundColor: "#d3d3d3",
                                        marginLeft: 0,
                                        paddingLeft: 10
                                    }}
                                    itemTextStyle={{ color: '#788ad2' }}
                                    headerStyle={{ backgroundColor: "#b95dd3" }}
                                    headerBackButtonTextStyle={{ color: "#fff" }}
                                    headerTitleStyle={{ color: "#fff" }}
                                    selectedValue={this.state.selectedFuelType}
                                    onValueChange={(fuelTypeId) => this.setState({selectedFuelType: fuelTypeId})}
                                >
                                    <Picker.Item label="Select Fuel Type" value={0} />
                                    { fuelTypes.map((item, index) => {
                                        return(<Picker.Item key={index} label={item.name} value={item.id} />)
                                        })
                                    }
                                </Picker>
                                
                                    <Button iconLeft primary
                                        onPress={() => this.addVehicle()}
                                    style={{ backgroundColor: colors.clientColor, marginLeft: 20 }}
                                    >
                                        <Icon name='add' />
                                        <Text>Add Car</Text>
                                    </Button>
                                    <Button iconLeft primary
                                        onPress={() => this.setState({ modalOpened: false })}
                                    style={{ backgroundColor: colors.clientColor, marginLeft: 20, marginTop: 20, marginBottom: 80 }}
                                    >
                                        <Icon name='close' />
                                        <Text>Cancel</Text>
                                    </Button>
                                
                            </ScrollView>
                           
                      </View>
                    </View>
              </Modal>
                <Modal
                    animationType="slide"
                    transparent={false}
                    onRequestClose={() => this.setState({ vehicleListModalOpened: false })}
                    visible={this.state.vehicleListModalOpened}
                    >

                    <View
                        style={{
                            backgroundColor: colors.clientColor,
                            paddingTop: 30,
                            paddingBottom: 20,
                            paddingLeft: 20,
                        }}
                    >
                        <Text style={{ color: "white", fontSize: 20 }}>
                            Your Vehicles
                        </Text>
                    </View>
                    
                    <ScrollView>
                            <FlatList data={vehicles} renderItem={this._renderVehicleListItem}/>
                    </ScrollView>

                </Modal>
            </Theme>
        );
    }
}

const mapStateToProps = ({ auth, client }) => ({
    userType: auth.userType,
    vehicles : client.vehicles,
    first_name: get(auth, 'user.customer.first_name'),
    last_name: get(auth, 'user.customer.last_name'),
    profile_photo: get(auth, 'user.customer.profile_photo_url'),
    email: get(auth, 'user.user.email'),

});

const mapDispatchToProps = { saveClientProfile, getBrands, getModels, getVariants, addVehicle, logoutUser, getAllVehicles, getServices };

export default connect(mapStateToProps, mapDispatchToProps)(ProfileClient);

