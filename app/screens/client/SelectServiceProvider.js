import React, { Component, Fragment } from 'react'
import { 
    View, 
    Card, 
    CardItem, 
    Text, 
    Body, 
    Header, 
    Item, 
    Icon, 
    Input, 
    Button, 
    Thumbnail, 
    Left,
    Right,
    List,
    ListItem
} from 'native-base';
import { connect } from 'react-redux';
import TimerCountdown from 'react-native-timer-countdown';
import { get as getLodash, forEach, find, filter, startsWith } from 'lodash';
import { ScrollView, FlatList, Modal, Image, Slider } from 'react-native';

import { getServiceProviders, sendBookingRequest } from '@actions';
import { Theme, PrimaryHeader, PrimaryFab, Toast, PrimaryButton } from '@components';
import { colors } from '@constants';

const NoDataFound = () => (
	<Card style={{ marginHorizontal:10 }}>
            <CardItem header bordered>
                <Left>
                    <Body>
                        <Text>No Data Found...</Text>
                    </Body>
                </Left>
            </CardItem>
    </Card>
);

class SelectServiceProvider extends React.PureComponent {

    static navigationOptions = { header: null };

    state = {
        service_providers: [],
        search_results: [],
        loading : true,
        searchKeyword: '',
        modalOpened: false,
        selectedProvider: null,
        timerModalOpened: false,
        range: 4,
        page: 1
    };

    _getServiceProviders = () => {
        this.setState({ loading : true, searchKeyword:'' });

    	let { range, page } = this.state;

        const { 
        	getServiceProviders, 
        	job_card: { address: { lat, long} } 
        } = this.props;

    	getServiceProviders({ latitude: lat, longitude: long, range, page  })
    		.then(({ data }) => {
	            this.setState({ 
	            	service_providers: data, 
	            	search_results: data, 
	            	loading : false
	            });
        })
        .catch(error => {
            this.setState({ loading : false});
        });
    }

    async componentDidMount(){
        await this._getServiceProviders();
    }

    _getTotalAmount = service_provider => {
        const {vehicle} = this.props;
        let car_type_with_price = find(service_provider.car_types, { id: vehicle.car_type_id}, null);
        if (!car_type_with_price) return "NA";
        return getLodash(car_type_with_price, 'pivot.price', "NA");
    }

    _onClickRequestBooking = async () => {
        this.setState({ modalOpened: false });
    	const { navigation, sendBookingRequest } = this.props;
    	let { job_card } = this.props;
        let { selectedProvider } = this.state;

        let address = job_card.address;
        let addressString = `${address.address_line_1},${address.address_line_2},${address.city},${address.state},${address.postal}`;
        job_card.address = addressString;
        delete job_card.vehicle;
        try{
        	Toast("Sending Booking Request... Please wait...");
        	await sendBookingRequest({ ...job_card, service_provider_id: selectedProvider.id })
        	this.setState({timerModalOpened: true});
        	// navigation.popToTop();
        }catch(e){
        	console.log(getLodash(e, 'response'));
        	Toast("Something went wrong");
        }
    }

    _showProviderDetails = (item) => {
        this.setState({
            selectedProvider: item,
            modalOpened: true
        }, () => {
        	console.log(this.state.selectedProvider)
        })
    }

    _onTimeElapsed = () => {
    	this.setState({ timerModalOpened: false, modalOpened: false });
        setTimeout(() => Toast('Provider rejected your request'), 700)
       }
       
    Timer = () => (
        <TimerCountdown
            initialSecondsRemaining={2000 * 60}
            onTick={secondsRemaining => console.log("tick", secondsRemaining)}
            onTimeElapsed={this._onTimeElapsed}
            allowFontScaling={true}
            style={{ fontSize: 50, color: colors.clientColor }}
        />
    )


    renderServiceProviderCard = ({item}) =>
    (
        <Card style={{ marginHorizontal:10 }}>
            <CardItem header bordered>
                <Left>
                    <Thumbnail source={{uri: item.workshop_photo_url}} />
                    <Body>
                        <Text>{item.garage_name} </Text>
                    </Body>
                </Left>
            </CardItem>

            <CardItem bordered>
                <Body style={{
                    flex: 1,
                    flexDirection: 'column'
                }}>
                    <Text style={{
                        fontSize : 15
                    }}>
                        Address : {item.address_line_1 + ", " + item.address_line_2 + " " + item.city +" "+item.state}
                    </Text>
                    <Text>
                        Pickup Available : {item.pickup_facility_available ? 'Yes' : 'No'}
                    </Text>
                    
                    { item['24_facility_available'] ? <Text>24*7 Service</Text> : null }
                    <Text style={{
                        marginTop: 10
                    }}>
                            Service Cost : {this._getTotalAmount(item)} 
                    </Text>
                        
                </Body>
            </CardItem>

            <CardItem footer bordered>
                <Left>
                    <Button transparent>
                      <Icon active name="star" />
                      <Text>3.4</Text>
                    </Button>
                </Left>
                <Right>
                    <Button
                        rounded
                        type='client'
                        onPress={()=> {this._showProviderDetails(item)}}
                        style={{ backgroundColor: colors.clientColor }}
                    >
                        <Text>View Details</Text>
                    </Button>
                </Right>
            </CardItem>
        </Card>
    );

    _searchProvider = keyword => {
        this.setState({ searchKeyword: keyword });
        if (keyword.length === 0) {
            return this.setState({
                search_results: this.state.service_providers
            });
        }
        console.log('keyword======>', keyword)
        this.setState((prevState) => {
            let result = filter(prevState.search_results, 
                            (provider) => startsWith(provider.garage_name, keyword));
            return { search_results: result };
        })
    }

    _changeDistance = (value) => {
    	this.setState({ range: value })
    	this._getServiceProviders();
    }


    header = () => {
        return(
        	<Fragment>
            <Header androidStatusBarColor={colors.clientColor} searchBar style={{ backgroundColor: colors.clientColor }} rounded>
                <Item>
                    <Icon name="ios-search" />
                    <Input 
                        onChangeText={ this._searchProvider } 
                        value={this.state.searchKeyword} 
                        placeholder="Search" 
                    />
                </Item>
                <Button transparent>
                    <Text>Search</Text>
                </Button>
            </Header>
            <View style={{ borderColor:colors.clientColor, borderWidth:3, opacity:0.5, flexDirection:'row', height:50, justifyContent:'flex-start', alignItems:'center' }} >
        		<Text style={{  width:'30%', marginLeft:5 }} >Distance: { `${this.state.range} KM` }</Text>
        		<View style={{ width:'75%' }} >
        			<Slider onSlidingComplete={this._getServiceProviders} step={1} minimumValue={4} onValueChange={this._changeDistance} style={{ marginRight: 20 }} maximumValue={50} />
        		</View>
        	</View>
        	</Fragment>
        )
    }

    render() {
        const { search_results, selectedProvider } = this.state;
        return (
            <Theme
                loading={this.state.loading}
                header={this.header()}
            >
            <Modal
                animationType="slide"
                transparent={true}
                onRequestClose={() => alert('You can not go back at this stage.')}
                visible={this.state.timerModalOpened}
            >
                    <Card transparent>
                        <CardItem style={{ backgroundColor: colors.providerColor, alignItems:'center', flexDirection:'column', height: '80%', justifyContent: 'center'}}>
                        	<Text>
                                { this.Timer() }
                            </Text>
                        </CardItem>
                        <CardItem style={{ alignItems: 'center', flexDirection: 'column', height: '20%' }} >
                            <Body>
                                <Text style={{ color: colors.providerColor }}>
                                    Please wait while the provider accepts your request.
                                    You can not go back at this stage.
                                </Text>
                            </Body>
                        </CardItem>
                    </Card>
            </Modal>
            { selectedProvider &&
                <Modal
                    animationType="slide"
                    transparent={false}
                    onRequestClose={() => this.setState({ modalOpened: false })}
                    visible={this.state.modalOpened}
                  >
                    <View>
                        <Card style={{ marginHorizontal:10 }}>
                            <CardItem header bordered>
                                <Left>
                                    <Thumbnail source={{uri: selectedProvider.workshop_photo_url}} />
                                    <Body>
                                        <Text>{selectedProvider.garage_name} </Text>
                                    </Body>
                                </Left>
                            </CardItem>
                            <CardItem cardBody>
                                <Image 
                                    source={{uri: selectedProvider.workshop_photo_url}} 
                                    style={{height: 150, width: null, flex: 1}}
                                />
                            </CardItem>

                            <CardItem bordered>
                                <Body style={{
                                    flex: 1,
                                    flexDirection: 'column'
                                }}>
                                    <Text style={{
                                        fontSize : 15
                                    }}>
                                        Address : {selectedProvider.address_line_1 + ", " + selectedProvider.address_line_2 + " " + selectedProvider.city +" "+selectedProvider.state}
                                    </Text>
                                    <Text>
                                        Pickup Available : {selectedProvider.pickup_facility_available ? 'Yes' : 'No'}
                                    </Text>
                                    
                                    { selectedProvider['24_facility_available'] ? <Text>24*7 Service</Text> : null }
                                    <Text style={{
                                        marginTop: 10
                                    }}>
                                            Service Cost : {this._getTotalAmount(selectedProvider)} 
                                    </Text>
                                        
                                </Body>
                            </CardItem>
                            <CardItem>
                                <ScrollView style={{ height: 150}}>
                                    <List>
                                    {
                                       filter(selectedProvider.service_subcategories, { service_id: 1}).map((ser, i) => {
                                            return(
                                                <ListItem key={i+ser.id}>
                                                    <Text>{ ser.name }</Text>
                                                </ListItem>
                                            );
                                       })
                                    }
                                    </List>
                                </ScrollView>
                            </CardItem>

                            <CardItem footer bordered>
                                <Left>
                                    <Button transparent>
                                      <Icon active name="star" />
                                      <Text>3.4</Text>
                                    </Button>
                                </Left>
                                <Right>
                                    <Button
                                        rounded
                                        type='client'
                                        onPress={()=> {this._onClickRequestBooking()}}
                                        style={{ backgroundColor: colors.clientColor }}
                                    >
                                        <Text>Book Now</Text>
                                    </Button>
                                </Right>
                            </CardItem>
                        </Card>
                    </View>
                </Modal>
            }
            <View style={{flex: 1}}>
                <ScrollView>
                    <FlatList
                        keyExtractor={ (item, index) => index+item.garage_name }
                        data={search_results}
                        renderItem={this.renderServiceProviderCard}
                        style={{ marginHorizontal:10 }}
                        ListEmptyComponent={<NoDataFound/>}
                    >
                    </FlatList>
                </ScrollView>
            </View>
            </Theme>
        );
    }
}


const mapStateToProps = ({ client }) => ({
    vehicle : client.job_card.vehicle,
    job_card : client.job_card
});

const mapDispatchToProps = { getServiceProviders, sendBookingRequest };

export default connect(mapStateToProps, mapDispatchToProps)(SelectServiceProvider);