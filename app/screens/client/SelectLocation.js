import React, { Component, Fragment } from "react";
import getTheme from "@base-theme-components";
import {
  Text,
  Fab,
  Icon,
  Input,
  Item,
  View,
  Picker,
  Button
} from "native-base";

import RNGooglePlaces from "react-native-google-places";
import { connect } from 'react-redux';
import { StyleSheet } from 'react-native';
import GenerateForm from "react-native-form-builder";

import { Theme, PrimaryHeader, PrimaryFab, Toast } from '@components';
import { selectLocation } from '@actions';
import { handleError } from '@helpers';
import { forEach } from 'lodash';
import { locationValidator } from '@validators';

const styles = StyleSheet.create({
    mainContent: {
        height: 400,
        padding: 15,
        paddingTop: 20,
        flexDirection: "column",
        flex: 1,
        alignItems: "center",
        justifyContent: "space-evenly"
    },
    mapButton: {
        alignItems: 'center'
    }
});

const fields = [
    {
        type: "text",
        name: "address_line_1",
        required: true,
        icon: "home",
        label: "Address Line 1",
    },
    {
        type: "text",
        name: "address_line_2",
        required: true,
        icon: "home",
        label: "Address Line 2",
    },
    {
        type: "text",
        name: "city",
        required: true,
        label: "City",
        editable: false
    },
    {
        type: "text",
        name: "state",
        required: true,
        label: "State",
        editable: false
    },
    {
        type: "text",
        name: "postal_code",
        required: true,
        label: "Postal Code",
        editable: false
    },
    {
        type: "hidden",
        name: "lat",
        label: "Latitude",
        editable: false
    },
    {
        type: "hidden",
        name: "long",
        label: "Longitude",
        editable: false
    }
];

const Header = () => (
    <PrimaryHeader
        type="client"
        title="Fill in your location details"
        description="Your location is important for us to react you." />
);

class SelectLocation extends Component {
    static navigationOptions = { header: null };
    
    state = {
        formValues: {
            address_line_1: "",
            address_line_2: "",
            city: "",
            state: "",
            postal_code: "",
            lat: "",
            long: ""
        },
        loading: false,
        errors: {}
    };

    componentWillMount() {
        console.log('SelectLocation==========>componentDidMount')
        // this.openSearchModal();
    }

    openSearchModal = () => {
        RNGooglePlaces.openPlacePickerModal()
        .then(place => {
            let address = place.address;
            let postal_code = (address.match(/([0-9]{6}|[0-9]{3}\s[0-9]{3})/g) || [])[0] || "";
            let state = ( (address.match(/,[^,]*([0-9]{6}|[0-9]{3}\s[0-9]{3})/g) || [])[0] || "" ).split(" ")[1] || "";
            let city = ( (address.match(/,[^,]*,[^,]*([0-9]{6}|[0-9]{3}\s[0-9]{3})/g) || [])[0] || "" ).split(" ")[1] || "";
            let temp = city ? address.substr(0, address.match(city).index).split(",") : address.split(",");
            let address_line_1 = temp.slice(0, Math.floor(temp.length / 2)).join(", ");
            let address_line_2 = temp.slice(Math.floor(temp.length / 2)).join(", ");
            let lat = place.latitude;
            let long = place.longitude;

            this.setState({
                formValues:{
                    address_line_1,
                    address_line_2,
                    city,
                    state,
                    postal_code,
                    lat,
                    long,
                }
            })
            this.locationDetails.setValues(this.state.formValues);
            console.log('formValues', this.locationDetails.getValues());
        })
        .catch(error => console.log(error.message)); // error is a Javascript Error object
    }

    submit = async () => {
    	const { formValues } = this.state;
    	const isValid = await locationValidator.isValid(formValues);
    	if(!isValid) return Toast('Please select a location');

        const { selectLocation, navigation } = this.props;
        this.locationDetails.setValues();
        selectLocation(this.locationDetails.getValues())
        navigation.navigate('SelectCar');
    }

    render() {
        return (
            <Theme loading={this.state.loading}
                fab={<PrimaryFab type="client" onPress={() => this.submit.bind(this)} />}
                header={<Header/>}
            >
                <View>
                    <Button style={styles.mapButton} iconLeft full onPress={this.openSearchModal}>
                        <Icon name='pin'/>
                        <Text>Open Map</Text>
                    </Button>
                    <GenerateForm ref={c => this.locationDetails = c } fields={fields}/>
                </View>
            </Theme>
        );
    }
}

const mapStateToProps = ({ auth }) => ({
    navigateToScreen: auth.onboardingStep
});

const mapDispatchToProps = { selectLocation };

export default connect(mapStateToProps, mapDispatchToProps)(SelectLocation);