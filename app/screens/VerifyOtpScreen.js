import React, { Component } from "react";
import { PermissionsAndroid } from "react-native";
import { View } from "native-base";
// import SmsAndroid  from 'react-native-get-sms-android';
import { connect } from 'react-redux';
import { registerUser, sendOtp, saveFcmToken } from '@actions';
import _ from 'lodash';
import SmsListener from 'react-native-android-sms-listener';
import { Theme, PrimaryHeader, PrimaryFab, PrimaryButton, Toast } from '@components';
import { handleError } from '@helpers';
import { forEach } from 'lodash';
import TimerCountdown from 'react-native-timer-countdown';
import CodeInput from 'react-native-confirmation-code-input';

const styles = {
	wrapper: {
		flex: 1
	},
	submitButton: {
		paddingHorizontal: 10,
		paddingTop: 20
	}
};

const Header = (props) => (
    <PrimaryHeader
        {...props}
        title="OTP Verification"
        description="Enter OTP sent to your phone number"
        goBackTo="Register"
    />
);

const Timer = (props) => (
    <TimerCountdown
        initialSecondsRemaining={1000 * 20}
        onTimeElapsed={props.onTimeElapsed}
        allowFontScaling={true}
        style={{ fontSize: 30 }}
    />
);

class VerifyOtp extends Component {
	static navigationOptions = { header: null };

	constructor(props){
		super(props);
		this.state = { 
			loading: false,
            isBtnVisible: false,
            otp: null
        };
        this.smsListener = null;
	}

    async componentDidMount() {
        try{
            await this.requestReadSmsPermission();
            this.smsListener = SmsListener.addListener(message => this.readOtp(message))
        }catch(e){
            console.log('ERROR IN READING MESSAGE', e);
        }
    }

    componentWillUnmount(){
        this.smsListener.remove();
    }

	readOtp(message){
        let verificationCodeRegex = /Your One Time Verification code for Car4Serviz is ([\d]{4}). Please do not share it with anyone\./g
        if (verificationCodeRegex.test(message.body)) {
            let verificationCode = message.body.match(/(\d{4})+/g)[0]	 	
            console.info(
                'OTP=====>',
                message.address,
                verificationCode
            );
            this.submit(verificationCode);
        }
	}
    
    async requestReadSmsPermission() {
        try {
            var granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.READ_SMS,
                {
                    title: "Auto Verification OTP",
                    message: "need access to read sms, to verify OTP"
                }
            );
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                console.log("sms read permissions granted", granted);
                granted = await PermissionsAndroid.request(
                    PermissionsAndroid.PERMISSIONS.RECEIVE_SMS, {
                        title: "Receive SMS",
                        message: "Need access to receive sms, to verify OTP"
                    }
                );
                if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                    console.log("RECEIVE_SMS permissions granted", granted);
                } else {
                    console.log("RECEIVE_SMS permissions denied");
                }
            } else {
                console.log("sms read permissions denied");
                throw 'Permission Denied';
            }
        } catch (err) {
            console.log(err);
        }
    }
    

	submit = async (otp = null) => {
        const { registerUser, saveFcmToken, navigation, userType } = this.props;

		this.setState({ loading: true, otp });

		let formValues = this.props.navigation.getParam("formValues"); 
		formValues = {otp, ...formValues};

		console.log('formValues', formValues);

		let navigateTo = userType === 'provider' ? 'ProviderStack' : 'ClientStack';
        
        try {
            await registerUser(formValues);
            await saveFcmToken();
            this.setState({ loading: false }, () => {
                navigation.navigate(navigateTo);
            });
        } catch (errorRes) {
            console.log('errorRes', errorRes);
            const { message, validation_errors } = handleError(errorRes);
            let error = message;
            if (validation_errors) {
                forEach(validation_errors, (err, index) => {
                    error = err[0];
                });
            }
            this.setState({
                disableButton: false,
                loading: false
            }, () => Toast(error, userType))
        }
    }
    
    _onTimeElapsed = () => {
        this.setState({ isBtnVisible: true })
    }

    _resendOtp = async () => {
        const { sendOtp } = this.props;
        this.setState({ loading: true });
        let formValues = this.props.navigation.getParam("formValues");
        try {
            await sendOtp(formValues);
            Toast('OTP sent');
        } catch (e) {
            Toast('Something went wrong');
        }finally{
            this.setState({ loading: false, isBtnVisible: false });
        }
    }

	render(){
      	const { userType } = this.props;

		return(
			<Theme
                {...this.props}
                loading={ this.state.loading }
                header={<Header type={userType} />}
			>
				<View style={styles.wrapper}>
					<View  style={{ alignItems:'center' }}>
                        <CodeInput
                            ref="otpInput"
                            secureTextEntry
                            codeLength={4}
                            activeColor='rgba(49, 180, 4, 1)'
                            inactiveColor='rgba(49, 180, 4, 1.3)'
                            autoFocus={false}
                            ignoreCase={true}
                            inputPosition='center'
                            size={50}
                            keyboardType="numeric"
                            onFulfill={(code) => this.submit(code)}
                            containerStyle={{ margin: 30 }}
                            codeInputStyle={{ borderWidth: 1.5 }}
                        />
                    {!this.state.isBtnVisible && <Timer onTimeElapsed={() => this._onTimeElapsed() } />}
					</View>
					{this.state.isBtnVisible && <View style={{ justifyContent:'center', flexDirection:'row' }} >
                        <PrimaryButton buttonText={'Resend OTP'} onPress={ this._resendOtp } />                        
					</View>}
				</View>
				<PrimaryFab type={userType} onPress={ () => this.submit } />
			</Theme>
		);
	}
}

const mapStateToProps = ({ auth }) => ({
	userType: auth.userType
});

const mapDispatchToProps = { registerUser, sendOtp, saveFcmToken };

export default connect(mapStateToProps, mapDispatchToProps)(VerifyOtp);