import React, { Component } from "react";
import { View, Text, Item, Input, Icon } from "native-base";
import { StyleSheet } from 'react-native';
import { connect } from 'react-redux';

import { Theme, PrimaryHeader, PrimaryFab, Toast } from "@components";
import { authValidator } from '@validators';
import { handleError } from '@helpers';
import { sendOtp } from '@actions';
import { forEach } from 'lodash';

const styles = StyleSheet.create({
    wrapper: {
        flex: 1
    },
    submitButton: {
        paddingHorizontal: 10,
        paddingTop: 20
    },
    textLine1:{
        color:'gray',
        textAlign:'center',
        fontSize:13,
        padding:'10%',
        paddingBottom:'0%'
    },
    textLine2:{
        fontSize:13,
        textDecorationLine:'underline'
    }

});

const Header = (props) => (
    <PrimaryHeader
        {...props}
        title="Register"
        description="Enter Phone Number and Password"
        goBackTo="Auth"
    />
);

class Register extends Component {
    static navigationOptions = {
        header: null
    };

    state = {
        loading: false,
        isPasswordVisible: false,
        password: null,
        phone: null,
        passwordErr: false,
        phoneErr: false,
    }

    async submit() {
        const { navigation, sendOtp, userType } = this.props;
        let { phone, password } = this.state;
        let formValues = { phone, password };
        console.log('formValues', formValues);
        try {
            await authValidator.userValidator.validate(formValues);
        } catch ({path, message}) {
            if(path === 'password'){
                this.setState({ passwordErr: message })
            }else{
                this.setState({ passwordErr: false })
            } 
            if(path === 'phone'){
                this.setState({ phoneErr: message })
            }else{
                this.setState({ phoneErr: false })
            }
            return;
        }
        this.setState({ passwordErr: false, phoneErr: false })

        formValues.user_type = userType === 'provider' ? 1 : 2;
        this.setState({ loading: true });

        try {
            await sendOtp(formValues);
            this.setState({ loading: false }, () => {
                navigation.navigate("VerifyOtp", {
                    formValues: formValues
                });
            });
        } catch (errorRep) {
            const { message, validation_errors } = handleError(errorRep);
            let error = message;
            if (validation_errors) {
                forEach(validation_errors, err => {
                    error = err[0];
                });
            }
            this.setState({ loading: false }, () => Toast(error, userType));
        }finally{
            this.setState({ loading: false });
        }
    }

    render() {
        const { userType } = this.props;

        return (
            <Theme 
                {...this.props}
                loading={ this.state.loading }
                header={<Header type={userType} />}
            >
                <View style={styles.wrapper}>
                    <View style={{ margin: 10, padding: 10 }}>
                        <Item error={!(!this.state.phoneErr)} style={{ marginVertical: 10 }} >
                            <Icon active name='call' />
                            <Input keyboardType={'phone-pad'} dataDetectorTypes={'phoneNumber'} autoComplete={'tel'} placeholder='Phone' onChangeText={phone => this.setState({phone})} value={this.state.phone}  />
                            {this.state.phoneErr && <Icon name='close-circle' />}
                        </Item> 
                        {this.state.phoneErr && <Text style={{ color: 'red' }}>{ this.state.phoneErr }</Text>}
                        <Item error={!(!this.state.passwordErr)} style={{ marginVertical: 10 }}>
                            <Icon active name='lock' />
                            <Input contextMenuHidden={true} autoComplete={'password'} secureTextEntry={!this.state.isPasswordVisible} placeholder='Password' onChangeText={password => this.setState({password})} value={this.state.password} />
                            {this.state.passwordErr && <Icon name='close-circle' />}
                            {!this.state.passwordErr && <Icon active name={this.state.isPasswordVisible ? 'eye-off': 'eye'  } onPress={() => this.setState((prevState) => ({isPasswordVisible: !(prevState.isPasswordVisible)}))} />}
                        </Item>
                        {this.state.passwordErr && <Text style={{ color: 'red' }}>{ this.state.passwordErr }</Text>}
                    </View>
                    <View>
                        <Text style={styles.textLine1}>{`By accepting to create an account, You accept `} 
                            <Text style={styles.textLine2}>{`Terms and Conditions`}</Text>{` and `}  
                            <Text style={styles.textLine2}>{`Privacy Policy`}</Text>{` of Car4Serviz.`}
                        </Text>
                    </View>
                </View>
                <PrimaryFab type={ userType } onPress={ () => this.submit.bind(this) } />
            </Theme>
        );
    }
}

const mapStateToProps = ({ auth }) => ({
    user: auth.user,
    userType: auth.userType
});

const mapDispatchToProps = { sendOtp };

export default connect(mapStateToProps, mapDispatchToProps)(Register);

