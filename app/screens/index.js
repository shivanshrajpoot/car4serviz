import Welcome from './WelcomeScreen';
import SignIn from './SignInScreen';
import Auth from './AuthScreen';
import Register from './RegisterScreen';
import AuthLoading from './AuthLoadingScreen';
import CarTypePricing from './service-provider-onboarding/CarTypePricingScreen';
import ChooseService from './service-provider-onboarding/ChooseServiceScreen';
import Details from './service-provider-onboarding/DetailsScreen';
import LocationDetails from './service-provider-onboarding/LocationDetailsScreen';
import Payment from './service-provider-onboarding/PaymentScreen';
import ThankYou from './service-provider-onboarding/ThankYouScreen';
import Workshop from './service-provider-onboarding/WorkshopScreen';
import VerifyOtp from './VerifyOtpScreen';
import ForgotPassword from './ForgotPasswordScreen';
import ResetPassword from './ResetPasswordScreen';
import SideBar from './Sidebar';

//Service Provider Screens
import About from './service-provider/About';
import Dashboard from './service-provider/Dashboard';
import ServiceOrders from './service-provider/ServiceOrders';
import QuotesProvider from './service-provider/Quotes';
import GenerateInvoice from './service-provider/GenerateInvoice';
import Quotation from './service-provider/Quotation';
import ManageService from './service-provider/ManageService';
import QnA from './service-provider/QnA';
import Accounting from './service-provider/Accounting';
import Wallet from './service-provider/Wallet';
import Refer from './service-provider/Refer';
import Order from './service-provider/Order';
import OrderWorkFlow from './service-provider/OrderWorkFlow';
import AcceptBooking from './service-provider/AcceptBooking';

//Client Screens
import QuotesClient from './client/Quotes';
import ProfileClient from './client/Profile';
import BookService from './client/BookService';
import SelectLocation from './client/SelectLocation';
import SelectCar from './client/SelectCar';
import ServiceOrdersClient from './client/ServiceOrdersClient';
import QuoteRequests from './client/QuoteRequests';


export {
	Auth,
	Welcome,
	ForgotPassword,
	ResetPassword,
	SignIn,
	Register,
	AuthLoading,
//Service Provider
	CarTypePricing,
	ChooseService,
	Details,
	LocationDetails,
	Payment,
	ThankYou,
	Workshop,
	VerifyOtp,
	SideBar,
	About,
	Dashboard,
	ServiceOrders,
	QuotesProvider,
	GenerateInvoice,
	Quotation,
	ManageService,
	QnA,
	Accounting,
	Wallet,
	Refer,
	Order,
	OrderWorkFlow,
	AcceptBooking,
//Client
	QuotesClient,
	ProfileClient,
	BookService,
	SelectLocation,
	SelectCar,
	ServiceOrdersClient,
	QuoteRequests
};