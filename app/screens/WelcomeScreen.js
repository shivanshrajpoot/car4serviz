import React, { Component } from "react";
import {
    Container,
    Text,
    Button,
    Content,
    View,
    StyleProvider,
    Icon
} from "native-base";
import { Image, StyleSheet, AsyncStorage, Alert } from "react-native";
import Carousel from "react-native-snap-carousel";
import { connect } from 'react-redux';
import getTheme from "@base-theme-components";
import { Theme, PrimaryButton } from '@components';

import { updateUserType, logoutUser } from '@actions';
import firebase, { Notification, NotificationOpen } from 'react-native-firebase';

const styles = StyleSheet.create({
    backgroundImage: {
        height: '40%',
        marginLeft: 20,
        marginRight: 20,
        width: 400
    },
    headerText: {
        marginBottom: 10,
        width: null,
        textAlign: "center",
        fontSize: 20
    },
});

import { createTestNotification, displayNotification, createChannel } from '@helpers';

class Welcome extends Component {

    static navigationOptions = {
        header: null
    };

    // async requestForPermission(){
    //     try {
    //         await firebase.messaging().requestPermission();
    //         await this.listenNotifications();
    //     } catch (error) {
    //         // User has rejected permissions
    //     }
    // }

    // async listenNotifications(){
    //     const notificationOpen: NotificationOpen = await firebase.notifications().getInitialNotification();
    //     if (notificationOpen) {
    //         const action = notificationOpen.action;
    //         const notification: Notification = notificationOpen.notification;
    //         var seen = [];
    //         alert('fdsfads'+JSON.stringify(notification.data, function(key, val) {
    //             if (val != null && typeof val == "object") {
    //                 if (seen.indexOf(val) >= 0) {
    //                     return;
    //                 }
    //                 seen.push(val);
    //             }
    //             return val;
    //         }));
    //     } 
    //     const channel = new firebase.notifications.Android.Channel('test-channel', 'Test Channel', firebase.notifications.Android.Importance.Max)
    //             .setDescription('My apps test channel');
    //     // Create the channel
    //     firebase.notifications().android.createChannel(channel);
        
    //     this.notificationDisplayedListener = firebase.notifications().onNotificationDisplayed(
    //         (notification: Notification) => {
    //         // Process your notification as required
    //         // ANDROID: Remote notifications do not contain the channel ID. You will have to specify this manually if you'd like to re-display the notification.
    //     });

    //     this.notificationListener = firebase.notifications().onNotification(
    //         (notification: Notification) => {
    //         // Process your notification as required
    //         notification
    //             .android.setChannelId('test-channel')
    //             .android.setSmallIcon('ic_launcher');
    //         firebase.notifications()
    //             .displayNotification(notification);
            
    //     });
    //     this.notificationOpenedListener = firebase.notifications().onNotificationOpened(
    //         (notificationOpen: NotificationOpen) => {
    //         // Get the action triggered by the notification being opened
    //         const action = notificationOpen.action;
    //         // Get information about the notification that was opened
    //         const notification: Notification = notificationOpen.notification;
    //         var seen = [];
    //         alert(JSON.stringify(notification.data, function(key, val) {
    //             if (val != null && typeof val == "object") {
    //                 if (seen.indexOf(val) >= 0) {
    //                     return;
    //                 }
    //                 seen.push(val);
    //             }
    //             return val;
    //         }));
    //         firebase.notifications().removeDeliveredNotification(notification.notificationId);
    //     });
    // }

    // async componentDidMount() {
    //     firebase.messaging().hasPermission()
    //     .then(enabled => {
    //         if (enabled) {
    //             this.listenNotifications();
    //         } else {
    //             this.requestForPermission();
    //         } 
    //     });
    // }

    // componentWillUnmount() {
    //     this.notificationDisplayedListener();
    //     this.notificationListener();
    //     this.notificationOpenedListener();
    // }

    userTypeSelected = (type) => {
        const { updateUserType, navigation, userType } = this.props;
        updateUserType(type)
        .then(() => {
            navigation.navigate('Auth');
        });
    }

    showTestNotification = () => {
    	createChannel();
    	const notf = createTestNotification()
		displayNotification(notf);
	 //    const channel = new firebase.notifications.Android
		//     .Channel('notification-channel', 'Notification Channel', firebase.notifications.Android.Importance.Max)
		//     .setDescription('Notification channell');

		// // Create the channel
		// firebase.notifications().android.createChannel(channel);
		// const notification = new firebase.notifications.Notification()
		// 	.setNotificationId('notificationId')
		// 	.setTitle('Test')
		// 	.setBody('Body');

		// notification.android.setChannelId('notification-channel');
		// notification.android.setAutoCancel(true);

		// firebase.notifications().displayNotification(notification);
    }

    render() {
        return (
           <Theme>
                <View style={{flex:1, alignItems:'center'}}>
                    <Image
                        source={require("../../assets/images/logo.png")}
                        style={styles.backgroundImage}/>
                    <View style={{ alignSelf: "center" }}>
                        <Text style={styles.headerText}>
                            I'm here for 
                        </Text>
                        <PrimaryButton type="client" onPress={ () => this.userTypeSelected("client") } buttonText="Service" />
                        <PrimaryButton type="provider" onPress={ () => this.userTypeSelected("provider") } buttonText="Providing a service" />
                        <PrimaryButton type="provider" onPress={ this.showTestNotification } buttonText="Show Test Notifcation" />
                    </View>
                </View>
           </Theme>
        );
    }
}

const mapStateToProps = ({ auth }) => ({
    userType: auth.userType,
});

const mapDispatchToProps = { updateUserType, logoutUser };

export default connect(mapStateToProps, mapDispatchToProps)(Welcome);

