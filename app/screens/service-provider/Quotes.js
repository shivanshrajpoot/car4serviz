import React, { Component, Fragment } from "react";
import { ScrollView, RefreshControl, FlatList, Dimensions } from "react-native";
import {
  View,
  Text,
  Icon,
  Card,
  CardItem,
  List,
  ListItem,
  Body,
} from "native-base";
import { connect } from 'react-redux';

import { Theme, PrimaryHeader, Toast } from '@components';
import { fetchQuotes } from '@actions';

class QuotesProvider extends React.PureComponent {

	state = {
		refreshing: false
	};

	async componentDidMount(){
		await this._fetchQuoteRequests();
	}

	static navigationOptions = {
		header: null
	};

	header = () => (<PrimaryHeader
		            {...this.props}
		            title={'Quotations'}
		            description={'You will find customers Quotations here. Look at the pictures submitted and the issues customers have in comments.'}
		            type={'provider'}
		            showDrawerButton
		        />
	);

  	_renderQuote = () => (
        <ListItem onPress={() => {}}>
			<Card style={{ width: (Dimensions.get('window').width * 90 / 100)}} >
				<CardItem header>
					<Text>New Service Request</Text>
				</CardItem>
				<CardItem>
					<Body>
						<Text>
							My car has a dent on the right back door and I want to get it fixed.
						</Text>
					</Body>
				</CardItem>
				<CardItem footer>
					<Text>28-08-2018 Thursday 11:00 AM to 01:00 PM</Text>
				</CardItem>
			</Card>
		</ListItem>
    );

  	_fetchQuoteRequests = async () => {
  		const { fetchQuotes } = this.props;
  		this.setState({refreshing: true})
  		try{
  			await fetchQuotes();
  		}catch(e){
  			Toast('Something went wrong.')
  		}
  		this.setState({refreshing: false})
  	}

	render() {
		const { userType } = this.props;
		return(
		    <Theme 
		        {...this.props}
		        header={this.header()}
		    >
	            <FlatList
	            	data={this.props.requests}
	            	keyExtractor={ (item, i) => `${i}` }
	            	renderItem={ this._renderQuote }
                    ListEmptyComponent={<Card style={{ justifyContent: 'center', alignItems: 'center' }}>
											<CardItem>
												<Text>No Data Found</Text>
											</CardItem>
										</Card>}
	            	refreshing={ this.state.refreshing }
	            	onRefresh={ this._fetchQuoteRequests }
	            />
		    </Theme>
		);
	}
}

const mapStateToProps = ({ auth, provider }) => ({
    userType: auth.userType,
    requests: provider.requests
});

const mapDispatchToProps = { fetchQuotes };

export default connect(mapStateToProps, mapDispatchToProps)(QuotesProvider);
