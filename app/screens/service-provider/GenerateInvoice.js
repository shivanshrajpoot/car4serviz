import React, { Component, Fragment } from "react";
import { AppRegistry, Dimensions, Modal, ScrollView } from "react-native";
import {
  Badge,
  View,
  Text,
  Button,
  Container,
  Content,
  StyleProvider,
  Icon,
  Form,
  Item,
  Label,
  Input,
  Picker,
  DatePicker,
  Header,
  Body,
  Left,
  Fab
} from "native-base";
import GenerateForm from "react-native-form-builder";
import getTheme from "@base-theme-components";
import { StackActions, NavigationActions } from "react-navigation";
import Toast, { DURATION } from "react-native-easy-toast";
import axios from "axios";

// const resetAction = StackActions.reset({
//   index: 0,
//   actions: [
//     NavigationActions.navigate({ routeName: 'Decider'})
//   ]
// })
import { PieChart } from 'react-native-svg-charts';

const styles = {
  wrapper: {
    
  },
};

export default class GenerateInvoice extends Component {
  state = {
    clientModal: false,
    selectedSlice: {
        label: '',
        value: 0
      },
    selected2: undefined,
    labelWidth: 0,
    data:[
            {
                key: 1,
                amount: 50,
                svg: { fill: '#81D4FA' },
                text: 'General Service'
            },
            {
                key: 2,
                amount: 50,
                svg: { fill: '#4DB6AC' },
                text: 'Maintenance & Repair'
            },
            {
                key: 3,
                amount: 40,
                svg: { fill: '#EF6C00' },
                text: 'Dent Repairing & Paiting'
            },
            {
                key: 4,
                amount: 95,
                svg: { fill: '#FDD835' },
                text: 'Car Wash & Detailing'
            },
            {
                key: 5,
                amount: 35,
                svg: { fill: '#81C784' },
                text: 'Road Side Assistance'
            }
        ]
  };
  static navigationOptions = {
    header: null
  };

  onValueChange2(value: string) {
    this.setState({
        selected2: value,
        clientModal: true
    });
  }

  setDate(newDate) {
    this.setState({ chosenDate: newDate });
  }

  submit(){
    
  }

  render() {

    return (
      <StyleProvider style={ getTheme() }>
        <Container>
            <Header style={{ backgroundColor: '#FFA400', height:(Dimensions.get('window').height * 30 / 100) }} span>
              <Left>
                <Button
                  transparent
                  onPress={() => this.props.navigation.openDrawer()}>
                  <Icon name="menu" />
                </Button>
              </Left>
              <Body style={{ flex:1, flexDirection:'column', justifyContent: "space-between", marginTop:5 ,marginLeft:-(Dimensions.get('window').width * 30 / 100) }}>
                <Text style={{fontSize:25, fontWeight:'bold', color:'white'}}>Generate Invoice</Text>
                <Text style={{color:'white'}}>One of the great feature of this App is that you can generate as many invoice as you want. This will work for other customers as well, who do come through us.</Text>
              </Body>
            </Header>
          <Content contentContainerStyle={{ flexGrow: 1 }}>
            <View style={{ flexDirection:'column', paddingTop:10, paddingRight:10, paddingLeft:10, marginBottom:20 }}>
              <Form style={{ marginRight:10 }}>
                <View style={{ flexDirection:'row', padding:10 }} >
                  <Item floatingLabel style={{ marginRight:10, width:(Dimensions.get('window').width * 65 / 100)}} >
                    <Label>Client</Label>
                    <Input />
                  </Item>
                  <Item picker style={{ width:(Dimensions.get('window').width * 24 / 100), marginRight:10}}>
                    <Picker
                      mode="dropdown"
                      placeholderStyle={{ color: "#bfc6ea" }}
                      placeholderIconColor="#007aff"
                      selectedValue={this.state.selected2}
                      onValueChange={this.onValueChange2.bind(this)}
                    >
                      <Picker.Item label="Client Name" value="key0" />
                      <Picker.Item label="Phone Number" value="key1" />
                      <Picker.Item label="Email ID" value="key2" />
                      <Picker.Item label="GSTIN" value="key3" />
                      <Picker.Item label="TIN" value="key4" />
                      <Picker.Item label="PAN" value="key5" />
                      <Picker.Item label="Street Address" value="key6" />
                      <Picker.Item label="City" value="key7" />
                      <Picker.Item label="State" value="key7" />
                    </Picker>
                  </Item>
                </View>
                <Item floatingLabel style={{ padding:5 }}>
                  <Label>Transaction #</Label>
                  <Input />
                </Item>
                <Item style={{ marginTop:20 }} >
                  <Label>Date</Label>
                  <DatePicker
                    defaultDate={new Date()}
                    minimumDate={new Date()}
                    locale={"en"}
                    timeZoneOffsetInMinutes={undefined}
                    modalTransparent={false}
                    animationType={"fade"}
                    androidMode={"default"}
                    placeHolderText="Select date"
                    textStyle={{ color: "#62B1F6" }}
                    placeHolderTextStyle={{ color: "#62B1F6" }}
                  onDateChange={this.setDate}
                  />
                </Item>
                <Item style={{ marginTop:20 }} >
                  <Label>Next Date</Label>
                  <DatePicker
                    defaultDate={new Date()}
                    minimumDate={new Date()}
                    locale={"en"}
                    timeZoneOffsetInMinutes={undefined}
                    modalTransparent={false}
                    animationType={"fade"}
                    androidMode={"default"}
                    placeHolderText="Select date"
                    textStyle={{ color: "#62B1F6" }}
                    placeHolderTextStyle={{ color: "#62B1F6" }}
                  onDateChange={this.setDate}
                  />
                </Item>
                <View style={{ flexDirection:'row', padding:10 }} >
                  <Item floatingLabel style={{ marginRight:10, width:(Dimensions.get('window').width * 65 / 100)}} >
                    <Label>Product/Service</Label>
                    <Input />
                  </Item>
                  <Item picker style={{ width:(Dimensions.get('window').width * 24 / 100), marginRight:10}}>
                    <Picker
                      mode="dropdown"
                      iosIcon={<Icon name="ios-arrow-down-outline" />}
                      placeholder="Select your SIM"
                      placeholderStyle={{ color: "#bfc6ea" }}
                      placeholderIconColor="#007aff"
                      selectedValue={this.state.selected2}
                      onValueChange={this.onValueChange2.bind(this)}
                    >
                      <Picker.Item label="Product/Item/Service" value="key0" />
                      <Picker.Item label="Description" value="key1" />
                      <Picker.Item label="UOM" value="key2" />
                      <Picker.Item label="Quantity" value="key3" />
                      <Picker.Item label="Unit Price" value="key4" />
                      <Picker.Item label="Discount %" value="key5" />
                      <Picker.Item label="Tax %" value="key6" />
                    </Picker>
                  </Item>
                </View>
                <Item floatingLabel style={{ padding:5 }}>
                  <Label>Shipping/Packaging Charge</Label>
                  <Input />
                </Item>
                <Item floatingLabel style={{ padding:5 }}>
                  <Label>Total</Label>
                  <Input />
                </Item>
                <View style={{ paddingHorizontal: 10, paddingTop: 20 }}>
                  <Button
                    rounded
                    block
                    onPress={() => this.submit()}
                  >
                    <Text>PREVIEW</Text>
                  </Button>
                </View>
              </Form>
            </View>
            {/* </KeyboardAvoidingView> */}
            <Modal
                    animationType="slide"
                    transparent={false}
                    onRequestClose={() => this.setState({ discountModal: false })}
                    visible={this.state.clientModal}
                >
                  <View
                    style={{
                      backgroundColor: getTheme().variables.headerColor,
                      paddingTop: 30,
                      paddingBottom: 10,
                      paddingLeft: 20
                    }}
                  >
                    <Text style={{ color: "white", fontSize: 20 }}>
                      Change Client Details
                    </Text>
                  </View>
                  <ScrollView style={{ height: "90%", padding: 15 }}>
                    <Item floatingLabel style={{ marginTop: 30 }}>
                      <Label>Client Name</Label>
                      <Input value="Kaushlendra" />
                    </Item>
                    <Item floatingLabel style={{ marginTop: 30 }}>
                      <Label>Phone Number</Label>
                      <Input value="2368123182" keyboardType="numeric"
                      />
                    </Item>
                    <Item floatingLabel style={{ marginTop: 30 }}>
                      <Label>Email</Label>
                      <Input value="email@gmail.com" />
                    </Item>
                    <Item floatingLabel style={{ marginTop: 30 }}>
                      <Label>GSTIN</Label>
                      <Input value="GSTIN2731392" />
                    </Item>
                    <Item floatingLabel style={{ marginTop: 30 }}>
                      <Label>TIN</Label>
                      <Input value="TIN163218" />
                    </Item>
                    <Item floatingLabel style={{ marginTop: 30 }}>
                      <Label>PAN</Label>
                      <Input value="WYNCD523TDB532" />
                    </Item>
                    <Item floatingLabel style={{ marginTop: 30 }}>
                      <Label>Street Address</Label>
                      <Input value="Street No. 4 233" />
                    </Item>
                    <Item floatingLabel style={{ marginTop: 30 }}>
                      <Label>City</Label>
                      <Input value="Gurugram" />
                    </Item>
                    <Item floatingLabel style={{ marginTop: 30, marginBottom: 30 }}>
                      <Label>State</Label>
                      <Input value="Haryana" />
                    </Item>
                  </ScrollView>
                  <Fab
                    direction="left"
                    style={{ backgroundColor: getTheme().variables.headerColor }}
                    position="bottomRight"
                    onPress={() => this.setState({ clientModal: false })}
                  >
                    <Icon name="done-all" />
                  </Fab>
                </Modal>
          </Content>
          <Toast
            ref="toast"
            position="bottom"
            positionValue={150}
            fadeInDuration={750}
            fadeOutDuration={1000}
          />
        </Container>
      </StyleProvider>
    );
  }
}

AppRegistry.registerComponent("GenerateInvoice", () => GenerateInvoice);
