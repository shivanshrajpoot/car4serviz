import React, { Component, Fragment } from "react";
import { AppRegistry, Dimensions, Image, ScrollView } from "react-native";
import {
  Badge,
  View,
  Text,
  Button,
  Container,
  Content,
  StyleProvider,
  Icon,
  Picker,
  Form,
  Card,
  CardItem,
  List,
  ListItem,
  Body,
  DeckSwiper,
  Left,
  Right,
  Thumbnail,
  Item,
  Input,
  Label,
  Textarea,
  Header
} from "native-base";
import GenerateForm from "react-native-form-builder";
import getTheme from "@base-theme-components";
import { StackActions, NavigationActions } from "react-navigation";
import Toast, { DURATION } from "react-native-easy-toast";
import axios from "axios";
import Carousel from 'react-native-snap-carousel';

// const resetAction = StackActions.reset({
//   index: 0,
//   actions: [
//     NavigationActions.navigate({ routeName: 'Decider'})
//   ]
// })
import { PieCart } from 'react-native-svg-charts';

const styles = {
  slide: {
    padding:10
  },
  title:{

  }
};

export default class Quotation extends Component {
  constructor(props){
    super(props);
  }

  static navigationOptions = {
    header: null
  };

  onValueChange(value: string) {

    this.setState({
      selected: value
    });
  }

   _renderItem ({item, index}) {
      return (
          <View style={styles.slide}>
              <Image source={item.image} style={{ height:300 }}/>
          </View>
      );
    }

  submit(){

  }

  render() {
    const cards = [
      {
        text: 'Card Two',
        name: 'One',
        image: require('../../../assets/images/drawer-cover-provider.png'),
      },
      {
        text: 'Card One',
        name: 'One',
        image: require('../../../assets/images/logo.png'),
      }
    ];

    return (
      <StyleProvider style={ getTheme() }>
        <Container>
        <Header style={{ backgroundColor: '#FFA400', height:(Dimensions.get('window').height * 25 / 100) }} span>
              <Left>
              <Button
                  transparent
                  onPress={() => this.props.navigation.goBack()}>
                <Icon
                name="arrow-back"
                style={{ color: "white" }}
              />
                </Button>
              </Left>
              <Body style={{ flex:1, flexDirection:'column', justifyContent: "space-between", marginTop:5 ,marginLeft:-(Dimensions.get('window').width * 30 / 100) }}>
                <Text style={{fontSize:25, fontWeight:'bold', color:'white'}}>Quotation</Text>
                <Text style={{color:'white'}}>You will find customers Quotations here. Look at the pictures submitted and the issues customers have in comments.</Text>
              </Body>
            </Header>
          <Content contentContainerStyle={{ flexGrow: 1 }}>
          <ScrollView contentContainerStyle={{ paddingVertical: 20}}>
            <Carousel
                ref={(c) => { this._carousel = c; }}
                data={cards}
                renderItem={this._renderItem}
                sliderWidth={Dimensions.get('window').width * 98/100}
                itemWidth={Dimensions.get('window').width}
              />
            <View style={{ flexDirection: "column", flex: 1, justifyContent:'center',  alignItems:'center', marginBottom:20 }}>
              <Card style={{ width: (Dimensions.get('window').width * 90 / 100)}} >
                <CardItem header>
                  <Text>New Service Request</Text>
                </CardItem>
                <CardItem>
                  <Body>
                    <Text>
                      My car has a dent on the right back door and I want to get it fixed.
                    </Text>
                  </Body>
                </CardItem>
                <CardItem footer>
                  <Text>28-08-2018 Thursday 11:00 AM to 01:00 PM</Text>
                </CardItem>
              </Card>
              <Form style={{ marginRight:10 }}>
                <View style={{ flexDirection:'column', padding:10, justifyContent:'center' }} >
                  <Item floatingLabel style={{ marginRight:10, width:(Dimensions.get('window').width * 65 / 100)}} >
                    <Label>Your Quote</Label>
                    <Input />
                  </Item>
                  <Textarea rowSpan={5} bordered placeholder="Comments" />
                </View>
                <View style={{ paddingHorizontal: 10, paddingTop: 20 }}>
                  <Button
                    rounded
                    block
                    onPress={() => this.submit()}
                  >
                    <Text>SUBMIT</Text>
                  </Button>
                </View>
              </Form>
            </View>          
          </ScrollView>
          </Content>
          <Toast
            ref="toast"
            position="bottom"
            positionValue={150}
            fadeInDuration={750}
            fadeOutDuration={1000}
          />
        </Container>
      </StyleProvider>
    );
  }
}

AppRegistry.registerComponent("Quotation", () => Quotation);
