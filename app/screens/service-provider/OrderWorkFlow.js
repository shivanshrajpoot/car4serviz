import React, { Component, Fragment } from "react";
import { AppRegistry, Dimensions, StyleSheet, Image } from "react-native";
import {
  Badge,
  View,
  Text,
  Button,
  Container,
  Content,
  StyleProvider,
  Icon,
  Picker,
  Form,
  Tab,
  Tabs,
  Card,
  CardItem,
  List,
  ListItem,
  Body,
  Header,
  Left
} from "native-base";
import GenerateForm from "react-native-form-builder";
import getTheme from "@base-theme-components";
import { StackActions, NavigationActions } from "react-navigation";
import Toast, { DURATION } from "react-native-easy-toast";
import axios from "axios";
import Timeline from 'react-native-timeline-listview'

// const resetAction = StackActions.reset({
//   index: 0,
//   actions: [
//     NavigationActions.navigate({ routeName: 'Decider'})
//   ]
// })
import { PieChart } from 'react-native-svg-charts';

const styles = {
  greyText:{
    marginBottom: 10,
    color: 'grey'
  },
  container: {
    flex: 1,
    padding: 20,
    paddingTop:65,
    backgroundColor:'white'
  },
  list: {
    flex: 1,
    marginTop:20,
  },
};

export default class OrderWorkFlow extends Component {

    constructor(){
      super()
      this.onEventPress = this.onEventPress.bind(this)
      this.renderSelected = this.renderSelected.bind(this)
      this.renderDetail = this.renderDetail.bind(this)

      this.data = [
        {
          time: '09:00', 
          title: 'Archery Training', 
          description: 'The Beginner Archery and Beginner Crossbow course does not require you to bring any equipment, since everything you need will be provided for the course. ',
          lineColor:'#009688', 
          icon: require('../../../assets/images/logo.png'),
          imageUrl: 'https://cloud.githubusercontent.com/assets/21040043/24240340/c0f96b3a-0fe3-11e7-8964-fe66e4d9be7a.jpg'
        },
        {
          time: '10:45', 
          title: 'Play Badminton', 
          description: 'Badminton is a racquet sport played using racquets to hit a shuttlecock across a net.', 
          icon: require('../../../assets/images/logo.png'),
          imageUrl: 'https://cloud.githubusercontent.com/assets/21040043/24240405/0ba41234-0fe4-11e7-919b-c3f88ced349c.jpg'
        },
        {
          time: '12:00', 
          title: 'Lunch', 
          icon: require('../../../assets/images/logo.png'),
        },
        {
          time: '14:00', 
          title: 'Watch Soccer', 
          description: 'Team sport played between two teams of eleven players with a spherical ball. ',
          lineColor:'#009688', 
          icon: require('../../../assets/images/logo.png'),
          imageUrl: 'https://cloud.githubusercontent.com/assets/21040043/24240419/1f553dee-0fe4-11e7-8638-6025682232b1.jpg'
        },
        {
          time: '16:30', 
          title: 'Go to Fitness center', 
          description: 'Look out for the Best Gym & Fitness Centers around me :)', 
          icon: require('../../../assets/images/logo.png'),
          imageUrl: 'https://cloud.githubusercontent.com/assets/21040043/24240422/20d84f6c-0fe4-11e7-8f1d-9dbc594d0cfa.jpg'
        }
      ]
    this.state = {selected: null}
  } 

  static navigationOptions = {
    header: null
  };
  
  onEventPress(data){
    this.setState({selected: data})
  }

  renderSelected(){
      if(this.state.selected)
        return <Text style={{marginTop:10}}>Selected event: {this.state.selected.title} at {this.state.selected.time}</Text>
  }

  renderDetail(rowData, sectionID, rowID) {
    let title = <Text style={[styles.title]}>{rowData.title}</Text>
    var desc = null
    if(rowData.description && rowData.imageUrl)
      desc = (
        <View style={styles.descriptionContainer}>   
          <Image source={{uri: rowData.imageUrl}} style={styles.image}/>
          <Text style={[styles.textDescription]}>{rowData.description}</Text>
        </View>
      )
    
    return (
      <View style={{flex:1}}>
        {title}
        {desc}
      </View>
    )
  }

  render() {

    return (
      <StyleProvider style={ getTheme() }>
        <Container>
           <Header style={{ backgroundColor: '#FFA400', height:(Dimensions.get('window').height * 25 / 100) }} span>
              <Left>
                 <Button
                  transparent
                  onPress={() => this.props.navigation.goBack()}>
                    <Icon
                    name="arrow-back"
                    style={{ color: "white" }}
                  />
                </Button>
              </Left>
              <Body style={{ flex:1, flexDirection:'column', justifyContent: "space-between", marginTop:5 ,marginLeft:-(Dimensions.get('window').width * 30 / 100) }}>
                <Text style={{fontSize:25, fontWeight:'bold', color:'white'}}>Service Order Workflow</Text>
                <Text style={{color:'white'}}>Let your customer know the time you can complete the car servicing. Put down additional comments if needed.</Text>
              </Body>
            </Header>
          <Content contentContainerStyle={{ flexGrow: 1, margin:20 }}>
           {this.renderSelected()}
            <Timeline 
              style={styles.list}
              data={this.data}
              circleSize={20}
              circleColor='rgba(0,0,0,0)'
              lineColor='rgb(45,156,219)'
              timeContainerStyle={{minWidth:52, marginTop: -5}}
              timeStyle={{textAlign: 'center', backgroundColor:'#ff9797', color:'white', padding:5, borderRadius:13}}
              descriptionStyle={{color:'gray'}}
              options={{
                style:{paddingTop:5}
              }}
              innerCircle={'icon'}
              onEventPress={this.onEventPress}
              renderDetail={this.renderDetail}
            />
              </Content>
              <Toast
                ref="toast"
                position="bottom"
                positionValue={150}
                fadeInDuration={750}
                fadeOutDuration={1000}
              />
        </Container>
      </StyleProvider>
    );
  }
}

AppRegistry.registerComponent("OrderWorkFlow", () => OrderWorkFlow);
