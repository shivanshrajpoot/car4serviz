import React, { Component, Fragment } from "react";
import { AppRegistry, Dimensions, Linking } from "react-native";
import { Text as ChartLabel } from 'react-native-svg';
import {
  Badge,
  View,
  Text,
  Button,
  Container,
  Content,
  StyleProvider,
  Icon,
  Picker,
  Form,
  Left,
  Header,
  Body
} from "native-base";
import GenerateForm from "react-native-form-builder";
import getTheme from "@base-theme-components";
import { StackActions, NavigationActions } from "react-navigation";
import Toast, { DURATION } from "react-native-easy-toast";
import axios from "axios";
import { platform } from '@base-theme-components';
import { navigationService } from '@helpers';

// const resetAction = StackActions.reset({
//   index: 0,
//   actions: [
//     NavigationActions.navigate({ routeName: 'Decider'})
//   ]
// })
import { PieChart } from 'react-native-svg-charts';

const styles = {
  wrapper: {
    
  },
};

export default class Dashboard extends Component {
  state = {
    selectedSlice: {
        label: '',
        value: 0
      },
    labelWidth: 0,
    data:[
            {
                key: 1,
                amount: 50,
                svg: { fill: '#81D4FA' },
                text: 'General Service'
            },
            {
                key: 2,
                amount: 50,
                svg: { fill: '#4DB6AC' },
                text: 'Maintenance & Repair'
            },
            {
                key: 3,
                amount: 40,
                svg: { fill: '#EF6C00' },
                text: 'Dent Repairing & Paiting'
            },
            {
                key: 4,
                amount: 95,
                svg: { fill: '#FDD835' },
                text: 'Car Wash & Detailing'
            },
            {
                key: 5,
                amount: 35,
                svg: { fill: '#81C784' },
                text: 'Road Side Assistance'
            }
        ]
  };

  static navigationOptions = {
    header: null
  };

  onValueChange(value: string) {

    this.setState({
      selected: value
    });

    let data = this.state.data;

    data = data.map((label, index) => {
      label['amount'] = Math.floor((Math.random() * 100) + 1);
      return label;
    });

  }

  componentDidMount(){
    Linking.getInitialURL().then(url => {
        console.log('Navigating.............')
        if(url) this.navigate(url);
    }).catch(err => {
        console.log('err======>', err)
    });
  }

  navigate = (url) => { // E
      const route = url.replace(/.*?:\/\//g, '');
      const routeName = route.split('/')[0];
      console.log('Navigating');
      if (routeName === 'booking') {
        navigationService.navigate('AcceptBooking');
      }
  }

  render() {
    const { data } = this.state;

    const NewLabels = () =>{
      return data.map((label, index) => {
        return(
          <View key={index} style={{ flexDirection: 'row', justifyContent: 'flex-end', padding:5, marginBottom:2, marginRight:5 }}>
            <Text style={{ fontSize:15 }}>
                { label.text }
            </Text>
            <Badge style={{ backgroundColor: label.svg.fill, marginLeft:10, width:27 }}/>  
          </View>
        );
      });
    };

    const Labels = ({ slices, height, width }) => {
            return slices.map((slice, index) => {
                const { labelCentroid, pieCentroid, data } = slice;
                return (
                    <ChartLabel
                        key={index}
                        x={pieCentroid[ 0 ]}
                        y={pieCentroid[ 1 ]}
                        fill={'white'}
                        textAnchor={'middle'}
                        alignmentBaseline={'middle'}
                        fontSize={18}
                        stroke={'black'}
                        strokeWidth={0.2}
                    >
                        {data.amount + 'K'}
                    </ChartLabel>
                )
            })
          }
    const deviceWidth = Dimensions.get('window').width
    return (
      <StyleProvider style={ getTheme(platform) }>
        <Container>

          <Header style={{ backgroundColor: '#FFA400', height:(Dimensions.get('window').height * 25 / 100) }} span>
            <Left>
              <Button
                transparent
                onPress={() => this.props.navigation.openDrawer()}>
                <Icon name="menu" />
              </Button>
            </Left>
            <Body style={{ flex:1, flexDirection:'column', justifyContent: "space-between", marginTop:5, marginLeft:-(Dimensions.get('window').width * 30 / 100) }}>
              <Text style={{fontSize:25, fontWeight:'bold', color:'white'}}>Dashboard</Text>
              <Text style={{color:'white'}}>Your Dashboard to tell you the story of your Sales Volumne with us!!</Text>
            </Body>
          </Header>
          <Content contentContainerStyle={{ flexGrow: 1 }}>
          {/* <KeyboardAvoidingView behavior="height"> */}
            <View style={{ flexDirection:'column' }}>
              <View style={{ flexDirection:'row', justifyContent:'flex-end' }}>
                <Form>
                  <Picker
                    note
                    mode="dropdown"
                    style={{ width: 120, backgroundColor: '#78909C', color:'#ffffff' }}
                    selectedValue={this.state.selected}
                    onValueChange={this.onValueChange.bind(this)}
                  >
                    <Picker.Item label="Today" value="key0" />
                    <Picker.Item label="Week" value="key1" />
                    <Picker.Item label="Month" value="key2" />
                    <Picker.Item label="Year" value="key3" />
                  </Picker>
                </Form>
              </View>
              <View style={{ flexDirection:'column', justifyContent:'flex-start', padding:10 }} >
                <View style={{
                  justifyContent: 'flex-end',
                }}>
                  <NewLabels/>
                </View>
              <View style={{ justifyContent: 'center', flex: 1 }}>
               <PieChart
                    style={{ height: 400 }}
                    valueAccessor={({ item }) => item.amount}
                    data={data}
                    spacing={0}
                    outerRadius={'95%'}
                  >
                  <Labels/>
                    <Text
                       style={{
                        position: 'absolute',
                        left: deviceWidth/2 - 100,
                        bottom: -200,
                        textAlign: 'center'
                      }}
                    >
                        Today's Sales 23,782 Rs.
                    </Text>
                </PieChart>
              </View>
            </View>
            </View>
            {/* </KeyboardAvoidingView> */}
          </Content>
          <Toast
            ref="toast"
            position="bottom"
            positionValue={150}
            fadeInDuration={750}
            fadeOutDuration={1000}
          />
        </Container>
      </StyleProvider>
    );
  }
}

AppRegistry.registerComponent("Dashboard", () => Dashboard);
