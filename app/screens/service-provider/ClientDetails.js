import React, { Component } from "react";
import { Container, Header, Content, Tab, Tabs, ListItem, Icon, Left, Body, Right, Switch, Button } from "native-base";
import getTheme from "../native-base-theme/components";
import Panel from '../components/panel';
import {AppRegistry,StyleSheet,Text,ScrollView} from 'react-native';

var styles = StyleSheet.create({
    container: {
      flex            : 1,
      backgroundColor : '#f4f7f9',
      paddingTop      : 30
    },
    
  });

export default class ClientDetails extends Component {
  static navigationOptions = {
    title: "Choose Available Services",
    headerStyle: {
      backgroundColor: getTheme().variables.brandPrimary
    },
    headerTintColor: '#fff',
    headerTitleStyle: {
        width:500
    }
  };
  render() {
    return (
        <ScrollView style={styles.container}>
        <Panel title="General Service">
        <ListItem icon>
            <Body>
              <Text>Road Side Assistance</Text>
            </Body>
            <Right>
              <Switch value={true}/>
            </Right>
          </ListItem>
        </Panel>
        <Panel title="Maintenace & Repair" expanded={false}>
          <Text>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</Text>
        </Panel>
        <Panel title="Dent Repairing & Painting" expanded={false}>
          <Text>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident.</Text>
        </Panel>
        <Panel title="Car Wash & detailing" expanded={false}>
          <Text>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident.</Text>
        </Panel>
      </ScrollView>
    );
  }
}
