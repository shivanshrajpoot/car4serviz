import React, { Component } from "react";
import {
    View,
    Text,
    Button
} from "native-base";
import { connect } from 'react-redux';
import { Theme, Toast } from '@components';
import { withNavigationFocus } from 'react-navigation';
import { stopRinging } from "@helpers";
import { acceptBooking } from '@actions';

class AcceptBooking extends Component {

    state = {
        address: "",
        latitude: "",
        longitude: "",
        remark: "",
        service_datetime: "",
        service_location: "",
        vehicle_name: "",
        booking_request_id: null
    };

    static navigationOptions = {
        header: null
    };

    componentDidMount(){
        const { navigation } = this.props;
        navigation.closeDrawer();
        const message = navigation.getParam('message', null);
        console.log('message', message);
        if (message) {
            const { id, address, remark, service_datetime, service_location, vehicle_name } = message;
            this.setState({ booking_request_id: id, address, remark, service_datetime, service_location, vehicle_name });
        }
    }

    _acceptBooking = async () => {
        const { acceptBooking, navigation } = this.props;
        const { booking_request_id } = this.state;
        const sound = navigation.getParam('sound', null);
        try{
            await acceptBooking(booking_request_id);
            Toast('Booking Accepted');
        }catch(e){
            console.log(e);
            Toast('Booking Timed Out');
        }finally{
            stopRinging(sound);
            navigation.goBack();
        }
    }

    _rejectBooking = async () => {
        const { navigation } = this.props;
        const sound = navigation.getParam('sound', null);
        stopRinging(sound);
        Toast('Booking Rejected');
        navigation.goBack();
    }

    render() {
        return (
            <Theme
                {...this.props}
            >
                <View style={{ height:'100%', justifyContent:'space-between', flexDirection: 'column', alignItems:'flex-start'}} >
                    <View style={{ flexDirection:'row', width: '100%', alignContent: 'center', justifyContent: 'center', marginVertical: 10, borderColor: 'grey', borderBottomWidth:2, padding:10 }}>
                        <Text style={{ fontSize: 40, color: 'grey' }}>
                            Accept Booking
                        </Text>
                    </View>
                    <View style={{ width:'80%', marginLeft: 5, margin:20, padding:15 }} >
                        <View style={{ flexDirection: 'row', justifyContent:'flex-start', marginVertical: 10 }}>
                            <Text style={{ fontSize: 20, color: 'grey'}}>Vehicle : </Text>
                            <Text style={{ fontSize: 20 }}>{this.state.vehicle_name}</Text>
                        </View>
                        <View style={{ flexDirection: 'row', justifyContent: 'flex-start', marginVertical: 10 }}>
                            <Text style={{ fontSize: 20, color: 'grey'}}>Service Time : </Text>
                            <Text style={{ fontSize: 20 }}>{this.state.service_datetime}</Text>
                        </View>
                        <View style={{ flexDirection: 'row', justifyContent: 'flex-start', marginVertical: 10 }}>
                            <Text style={{ fontSize: 20, color: 'grey'}}>Remark : </Text>
                            <Text style={{ fontSize: 20}}>{this.state.remark}</Text>
                        </View>
                        <View style={{ flexDirection: 'row', justifyContent: 'flex-start', marginVertical: 10 }}>
                            <Text style={{ fontSize: 20, color: 'grey'}}>Address : </Text>
                            <Text style={{ fontSize: 20 }}>{this.state.remark}</Text>
                        </View>
                        <View style={{ flexDirection: 'row', justifyContent: 'flex-start', marginVertical: 10 }}>
                            <Text style={{ fontSize: 20, color: 'grey'}}>Service Location : </Text>
                            <Text style={{ fontSize: 20 }}>{this.state.service_location}</Text>
                        </View>
                    </View>
                    <View style={{ alignItems:'center', flexDirection:'row', width: '100%', bottom:0}}>
                        <Button light block style={{ width: '50%', height: 50 }} onPress={this._rejectBooking}>
                            <Text>Decline</Text>
                        </Button>
                        <Button block success style={{ width: '50%', height: 50 }} onPress={this._acceptBooking}>
                            <Text>Accept</Text>
                        </Button>
                    </View>
                </View>
            </Theme>
        );
    }
}

const mapStateToProps = ({ provider }) => ({
    requests: provider.requests
});

const mapDispatchToProps = { acceptBooking };

export default connect(mapStateToProps, mapDispatchToProps)(withNavigationFocus(AcceptBooking));
