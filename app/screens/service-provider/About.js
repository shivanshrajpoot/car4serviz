import React, { PureComponent } from "react";
import { AppRegistry, FlatList } from "react-native";
import { Theme, PrimaryHeader } from '@components';
import { Card, CardItem, Text, Icon, Button } from 'native-base';

const Header = (props) => (<PrimaryHeader
    {...props}
    title={'About'}
    description={'You can update your profile from here'}
    type={'provider'}
    showDrawerButton
/>);

const ProfilePages = [
    { 'text': 'Personal Info', 'link': 'Details' },
    { 'text': 'Payment Info', 'link': 'Payment' },
    { 'text': 'Workshop Details', 'link': 'Workshop'},
    { 'text': 'Services Info', 'link': 'ChooseService' },
    { 'text': 'Car Pricing Info', 'link': 'CarTypePricing'},
    { 'text': 'Location Info', 'link': 'LocationDetails'},
];

export default class About extends PureComponent {
    static navigationOptions = {
        header: null
    };

    _renderListItem = ({ item, index }) => (
        <Card> 
            <CardItem style={{ justifyContent: 'space-between', alignItems: 'center'  }} >
                <Text>{ item.text }</Text>
                <Button iconLeft transparent onPress={() => this.props.navigation.navigate(item.link, { profilePage: true })}>
                    <Icon name='arrow-forward' />
                </Button>
            </CardItem>
        </Card>
    )


    render() {
        return (
            <Theme
                header={<Header {...this.props} />}
                {...this.props}
            >
                <FlatList
                    renderItem={this._renderListItem}
                    data={ProfilePages}
                    keyExtractor={ (item) => item.link }
                    extraData={ this.state }
                />    
            </Theme>
        );
    }
}

AppRegistry.registerComponent("About", () => About);
