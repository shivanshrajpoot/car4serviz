import React, { Component, Fragment } from "react";
import { AppRegistry, Dimensions } from "react-native";
import {
  Badge,
  View,
  Text,
  Button,
  Container,
  Content,
  StyleProvider,
  Icon,
  Picker,
  Form,
  Header,
  Left,
  Body
} from "native-base";
import GenerateForm from "react-native-form-builder";
import getTheme from "@base-theme-components";
import { StackActions, NavigationActions } from "react-navigation";
import Toast, { DURATION } from "react-native-easy-toast";
import axios from "axios";

// const resetAction = StackActions.reset({
//   index: 0,
//   actions: [
//     NavigationActions.navigate({ routeName: 'Decider'})
//   ]
// })
import { PieChart } from 'react-native-svg-charts';

const styles = {
  wrapper: {
    
  },
};

export default class QnA extends Component {
  state = {
    selectedSlice: {
        label: '',
        value: 0
      },
    labelWidth: 0,
    data:[
            {
                key: 1,
                amount: 50,
                svg: { fill: '#81D4FA' },
                text: 'General Service'
            },
            {
                key: 2,
                amount: 50,
                svg: { fill: '#4DB6AC' },
                text: 'Maintenance & Repair'
            },
            {
                key: 3,
                amount: 40,
                svg: { fill: '#EF6C00' },
                text: 'Dent Repairing & Paiting'
            },
            {
                key: 4,
                amount: 95,
                svg: { fill: '#FDD835' },
                text: 'Car Wash & Detailing'
            },
            {
                key: 5,
                amount: 35,
                svg: { fill: '#81C784' },
                text: 'Road Side Assistance'
            }
        ]
  };
  static navigationOptions = {
    header: null
  };

  onValueChange(value: string) {

    this.setState({
      selected: value
    });
  }
  render() {

    return (
      <StyleProvider style={ getTheme() }>
        <Container>
            <Header style={{ backgroundColor: '#FFA400', height:(Dimensions.get('window').height * 25 / 100) }} span>
              <Left>
                <Button
                  transparent
                  onPress={() => this.props.navigation.openDrawer()}>
                  <Icon name="menu" />
                </Button>
              </Left>
              <Body style={{ flex:1, flexDirection:'column', justifyContent: "space-between", marginTop:5 ,marginLeft:-(Dimensions.get('window').width * 30 / 100) }}>
                <Text style={{fontSize:25, fontWeight:'bold', color:'white'}}>Q & A</Text>
                <Text style={{color:'white'}}>You will find your orders here. It is to keep you updated of your upcoming orders...</Text>
              </Body>
            </Header>
          <Content contentContainerStyle={{ flexGrow: 1 }}>
          {/* <KeyboardAvoidingView behavior="height"> */}
            <View style={{ flexDirection:'column' }}>
              <Text>Coming Soon...</Text>
            </View>
            {/* </KeyboardAvoidingView> */}
          </Content>
          <Toast
            ref="toast"
            position="bottom"
            positionValue={150}
            fadeInDuration={750}
            fadeOutDuration={1000}
          />
        </Container>
      </StyleProvider>
    );
  }
}

AppRegistry.registerComponent("QnA", () => QnA);
