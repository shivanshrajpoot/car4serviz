import React, { Component, Fragment } from "react";
import { AppRegistry, Dimensions } from "react-native";
import {
  Badge,
  View,
  Text,
  Button,
  Container,
  Content,
  StyleProvider,
  Icon,
  Picker,
  Form,
  Tab,
  Tabs,
  Card,
  CardItem,
  List,
  ListItem,
  Body,
  Header,
  Left
} from "native-base";
import GenerateForm from "react-native-form-builder";
import getTheme from "@base-theme-components";
import { StackActions, NavigationActions } from "react-navigation";
import Toast, { DURATION } from "react-native-easy-toast";
import axios from "axios";
import CodeInput from 'react-native-confirmation-code-input';

// const resetAction = StackActions.reset({
//   index: 0,
//   actions: [
//     NavigationActions.navigate({ routeName: 'Decider'})
//   ]
// })
import { PieChart } from 'react-native-svg-charts';

const styles = {
  wrapper: {
    
  },
  greyText:{
    marginBottom: 10,
    color: 'grey'
  }
};

export default class Order extends Component {

  state = {
    selectedSlice: {
        label: '',
        value: 0
      },
    labelWidth: 0,
    data: [
      {time: '09:00', title: 'Event 1', description: 'Event 1 Description'},
      {time: '10:45', title: 'Event 2', description: 'Event 2 Description'},
      {time: '12:00', title: 'Event 3', description: 'Event 3 Description'},
      {time: '14:00', title: 'Event 4', description: 'Event 4 Description'},
      {time: '16:30', title: 'Event 5', description: 'Event 5 Description'}
    ]
  };
  static navigationOptions = {
    header: null
  };

  onValueChange(value) {

    this.setState({
      selected: value
    });
  }

  _onFinishCheckingCode1(){

  }

  render() {
    let data = this.state.data;
    const { navigate } = this.props.navigation;

    const ListItems = () => {
      return data.map((item, index) => {
        return(
          <ListItem key={index} >
            <Card style={{ width: (Dimensions.get('window').width * 90 / 100)}} >
              <CardItem header>
                <Text>New Service Request</Text>
              </CardItem>
              <CardItem>
                <Body>
                  <Text>
                    My car has a dent on the right back door and I want to get it fixed.
                  </Text>
                </Body>
              </CardItem>
              <CardItem footer>
                <Text>28-08-2018 Thursday 11:00 AM to 01:00 PM</Text>
              </CardItem>
           </Card>
          </ListItem>);
      });
    }

    return (
      <StyleProvider style={ getTheme() }>
        <Container>
           <Header style={{ backgroundColor: '#FFA400', height:(Dimensions.get('window').height * 25 / 100) }} span>
              <Left>
                 <Button
                  transparent
                  onPress={() => this.props.navigation.goBack()}>
                    <Icon
                    name="arrow-back"
                    style={{ color: "white" }}
                  />
                </Button>
              </Left>
              <Body style={{ flex:1, flexDirection:'column', justifyContent: "space-between", marginTop:5 ,marginLeft:-(Dimensions.get('window').width * 30 / 100) }}>
                <Text style={{fontSize:25, fontWeight:'bold', color:'white'}}>Details of Service Order</Text>
                <Text style={{color:'white'}}>Know the details of the order, your customer and schedule your calendar accordingly.</Text>
              </Body>
            </Header>
          <Content contentContainerStyle={{ flexGrow: 1 }}>
            <View style={{ flexDirection:'column', padding: 25, justifyContent:'space-between' }}>
              <Text style={ styles.greyText } >Vijay Kumar</Text>
              <Text style={ styles.greyText } >Requested for: General Service</Text>
              <Text style={ styles.greyText } >Additional Requests:</Text>
              <Text style={ styles.greyText } >Scheduled @ Customer's Location</Text>
              <Text style={ styles.greyText } >Date: 05/12/2018 (Thursday)</Text>
              <Text style={ styles.greyText } >Time: 11:00 AM to 03:00 PM</Text>
              <Text style={ styles.greyText } >Place: Uppal, Hyderabad</Text>
              <Button style={ styles.greyText } ><Text>Locate Customer's Address</Text></Button>
              <Button><Text>Call Customer</Text><Icon name="call" style={{ margin:10 }} /></Button>
            </View>
            <View style={{ justifyContent:'center', alignItems: 'center', margin: 20 }} >

              <Text style={ styles.greyText } > Car Make/Model: Maruti Suzuki Alto 800</Text>
              <Text style={ styles.greyText } > Enter OTP to start the servicing</Text>              
              <CodeInput
                ref="codeInputRef2"
                secureTextEntry
                compareWithCode='AsDW2'
                activeColor='rgba(49, 180, 4, 1)'
                inactiveColor='rgba(49, 180, 4, 1.3)'
                autoFocus={false}
                ignoreCase={true}
                inputPosition='center'
                size={50}
                onFulfill={(isValid) => this._onFinishCheckingCode1(isValid)}
                containerStyle={{ margin: 30 }}
                codeInputStyle={{ borderWidth: 1.5 }}
              />
               <Button
                  rounded
                  block
                  onPress={() => this.props.navigation.navigate('OrderWorkFlow')}
                  disabled={this.state.disableButton}
                >
                  <Text>SUBMIT</Text>
                </Button>
            </View>
          </Content>
          <Toast
            ref="toast"
            position="bottom"
            positionValue={150}
            fadeInDuration={750}
            fadeOutDuration={1000}
          />
        </Container>
      </StyleProvider>
    );
  }
}

AppRegistry.registerComponent("Order", () => Order);
