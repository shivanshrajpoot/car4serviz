import _ from 'lodash';
import React, { Component } from 'react';
import {
  View,
  Linking,
  Dimensions,
  Image
} from 'react-native';
import { connect } from 'react-redux';
// import { checkAppVersion } from '@actions';
import { Container, Header, Content, Spinner } from 'native-base';
import { StyleSheet } from 'react-native';
import { getServices } from '@actions';
// import SplashScreen from 'react-native-splash-screen'

const styles = StyleSheet.create({
    content: { flex:1 },
    contentView: {
        height: Dimensions.get('window').height,
        alignItems: 'stretch',
        flexDirection: 'column',
        justifyContent: 'center'
    }
});

const loadingGif = require("../../assets/images/drawer-cover-provider.png");

class AuthLoading extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isForceUpdate: false,
    };
    this.bootstrapAsync = this.bootstrapAsync.bind(this);
  }

  componentDidMount() {
    const { getServices } = this.props;
    // const { checkAppVersion } = this.props;
    // checkAppVersion()
    //   .then((response) => {
    //     const data = _.get(response, 'data.data', {});
    //     if (data) {
    //       if (data.minimum_version > data.current_version) {
    //         this.setState({
    //           isForceUpdate: true,
    //         }, () => {
    //           Alert.alert(
    //             'Update available',
    //             `Please update to latest version ${data.current_version}`,
    //             [
    //               { text: 'Update Now', onPress: () => Linking.openURL('http://www.google.com') },
    //             ],
    //             { cancelable: false },
    //           );
    //         });
    //       } else {
    //         this.bootstrapAsync();
    //       }
    //     }
    //   })
    //   .catch(() => {

    //   });
    this.bootstrapAsync();

  }

  // Fetch the token from storage then navigate to our appropriate place
  async bootstrapAsync() {
    const { user, navigation, onboardingStep } = this.props;
    const { isForceUpdate } = this.state;
    // This will switch to the App screen or Auth screen and this loading
    // screen will be unmounted and thrown away.
    
    await navigation.navigate(!isForceUpdate && user && user.access_token ? 'ProviderStack' : 'AuthStack');
    // SplashScreen.hide();
  }

  // Render any loading content that you like here
  render() {
    return (
       <Container>
        <Content style={styles.content}>
          <View style={styles.contentView}>
            <Spinner color='blue' />
          </View>
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = ({ auth }) => ({
    user: auth.user,
    onboardingStep: auth.onboardingStep
});

const mapDispatchToProps = { getServices };

export default connect(mapStateToProps, mapDispatchToProps)(AuthLoading);
