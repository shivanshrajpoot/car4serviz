import React, { Component, Fragment } from "react";
import getTheme from "@base-theme-components";
import {
  Text,
  Icon,
  View,
  Button
} from "native-base";
import { withNavigationFocus } from 'react-navigation';
import RNGooglePlaces from "react-native-google-places";
import { connect } from 'react-redux';
import { StyleSheet } from 'react-native';
import GenerateForm from "react-native-form-builder";

import { Theme, PrimaryHeader, PrimaryFab, Toast } from '@components';
import { saveProfileDataSteps, updateOnboardingState } from '@actions';
import { handleError } from '@helpers';
import { forEach } from 'lodash';


const styles = StyleSheet.create({
    mainContent: {
        height: 400,
        padding: 15,
        paddingTop: 20,
        flexDirection: "column",
        flex: 1,
        alignItems: "center",
        justifyContent: "space-evenly"
    },
    mapButton: {
        alignItems: 'center'
    }
});

const fields = [
    {
        type: "text",
        name: "address_line_1",
        required: true,
        icon: "home",
        label: "Address Line 1"
    },
    {
        type: "text",
        name: "address_line_2",
        required: true,
        icon: "home",
        label: "Address Line 2"
    },
    {
        type: "text",
        name: "city",
        required: true,
        label: "City"
    },
    {
        type: "text",
        name: "state",
        required: true,
        label: "State"
    },
    {
        type: "text",
        name: "postal_code",
        required: true,
        label: "Postal Code"
    },
    {
        type: "hidden",
        name: "lat",
        required: true,
        label: "Latitude"
    },
    {
        type: "hidden",
        name: "long",
        required: true,
        label: "Longitude"
    }
];

const Header = () => (
    <PrimaryHeader
        type="provider"
        title="Fill in your location details"
        description="Enter full address if you can't locate in map below. It is for your customer to reach you." 
    />
)

class LocationDetails extends Component {
    static navigationOptions = { header: null };
    state = {
        formValues: {
            address_line_1: "",
            address_line_2: "",
            city: "",
            state: "",
            postal_code: "",
            lat: "",
            long: ""
        },
        loading: false,
        errors: {}
    };

    openSearchModal = () => {
        RNGooglePlaces.openPlacePickerModal()
        .then(place => {
            let address = place.address;
            let postal_code = (address.match(/([0-9]{6}|[0-9]{3}\s[0-9]{3})/g) || [])[0] || "";
            let state = ( (address.match(/,[^,]*([0-9]{6}|[0-9]{3}\s[0-9]{3})/g) || [])[0] || "" ).split(" ")[1] || "";
            let city = ( (address.match(/,[^,]*,[^,]*([0-9]{6}|[0-9]{3}\s[0-9]{3})/g) || [])[0] || "" ).split(" ")[1] || "";
            let temp;
            if (city) {
                temp = address.substr(0, address.match(city).index).split(",");
            } else {
                temp = address.split(",");
            }

            let address_line_1 = temp.slice(0, Math.floor(temp.length / 2)).join(", ");
            let address_line_2 = temp.slice(Math.floor(temp.length / 2)).join(", ");
            let lat = place.latitude;
            let long = place.longitude;

            this.setState({
                formValues:{
                    address_line_1,
                    address_line_2,
                    city,
                    state,
                    postal_code,
                    lat,
                    long,
                }
            })
            this.locationDetails.setValues(this.state.formValues);
            console.log('formValues', this.locationDetails.getValues());
        })
        .catch(error => console.log(error.message)); // error is a Javascript Error object
    }

    submit = async () => {
        this.setState({ loading: true });
        const { saveProfileDataSteps, navigation } = this.props;
        this.locationDetails.setValues(this.state.formValues);

        try {
            await saveProfileDataSteps(this.locationDetails.getValues(), 1, 'Details')
            this.setState({ loading: false }, () => {
                navigation.navigate("Details");
            });
        } catch (errorRes) {
            const { message, validation_errors } = handleError(errorRes);
            let error = message;
            if (validation_errors) {
                forEach(validation_errors, err => {
                    error = err[0];
                });
            }
            this.setState({ loading: false }, () => {
                Toast(error)
            });   
        }
    }

    render() {
        const { navigation } = this.props;
        const profilePage = navigation.getParam('profilePage', false);
        return (
            <Theme 
                loading={this.state.loading}
                header={<Header navigation={navigation} />}
                fab={!profilePage && <PrimaryFab type="provider" onPress={() => this.submit} />}
            >
                <View>
                    <Button style={styles.mapButton} iconLeft full onPress={this.openSearchModal}>
                        <Icon name='pin'/>
                        <Text>Open Map</Text>
                    </Button>
                    <GenerateForm ref={c => this.locationDetails = c } fields={fields}/>
                </View>
            </Theme>
        );
    }
}

const mapStateToProps = ({ auth }) => ({
    navigateToScreen: auth.onboardingStep
});

const mapDispatchToProps = { saveProfileDataSteps, updateOnboardingState };

export default connect(mapStateToProps, mapDispatchToProps)(withNavigationFocus(LocationDetails));