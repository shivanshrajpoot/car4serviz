import React, { Component, Fragment } from "react";
import { 
    Form,
    Right, 
    View, 
    Item, 
    Input, 
    Label, 
    Switch 
} from "native-base";
import { connect } from 'react-redux';
import { withNavigationFocus } from 'react-navigation';

import { handleError } from '@helpers';
import { Theme, PrimaryHeader, PrimaryFab, PrimaryButton, Toast } from '@components';
import { saveProfileDataSteps, updateOnboardingState } from '@actions';
import _ from 'lodash';

const Header = (props) => (
    <PrimaryHeader
        {...props}
        type="provider"
        title="Fill-in Profile Details"
        description="Fill in profile details as per your GST registration"
    />
);

class Details extends Component {
    static navigationOptions = { header: null };

    state = {
        formValues: {
            gst_available: false,
            gst: null,
            email: "",
            advisor_name : '',
            garage_name : ''
        },
        loading: false,
    }

    onChangeEvent(fieldName, event, type = "text") {
        let value;

        switch (type) {
            case "text":
                event.persist();
                value = event.nativeEvent.text;
                break;
            case "switch":
                value = event;
                break;
        }

        let formValues = this.state.formValues;
        formValues[fieldName] = value;
        this.setState({ formValues });
    }

    submit = () => {
        this.setState({ loading: true });
        const { saveProfileDataSteps, updateOnboardingState } = this.props;

        saveProfileDataSteps(this.state.formValues, 2)
        .then(() => {
            this.setState({ loading: false }, () => {
                updateOnboardingState({ screenName: 'Workshop' });
                this.props.navigation.push("Workshop");
            });
        })
        .catch(errorRes => {
            const { message, validation_errors } = handleError(errorRes);
            let error = message;
            if (validation_errors) {
                _.forEach(validation_errors, (err, index) => {
                    error = err[0];
                });
            }
            this.setState({ loading: false },() => {
                Toast(error)
            });
        });
    }

    render() {
        const { navigation } = this.props;
        const profilePage = navigation.getParam('profilePage', false);
        console.log('profilePage', profilePage);
        return (
            <Theme
                {...this.props}
                loading={this.state.loading}
                fab={!profilePage && <PrimaryFab type="provider" onPress={() => this.submit} />}
                header={!profilePage ? <Header goBackTo={'LocationDetails'} {...this.props} /> : <Header {...this.props} />}
            >
                <View
                    style={
                        {
                            height: "100%",
                            padding: 15,
                            flexDirection: "column",
                            alignItems: "center",
                        }
                    }>
                    <Form>
                        <Item picker>
                            <Input
                                value={this.state.formValues.advisor_name}
                                placeholder="Advisor Name"
                                onChange={e => {
                                    this.onChangeEvent("advisor_name", e);
                                }}/>
                        </Item>

                        <Item picker>
                            <Input
                                value={this.state.formValues.garage_name}
                                placeholder="Garage Name"
                                onChange={e => {
                                    this.onChangeEvent("garage_name", e);
                                }}
                            />
                        </Item>

                        <Item picker>
                            <Input
                                value={this.state.formValues.email}
                                placeholder="E-mail"
                                onChange={e => {
                                    this.onChangeEvent("email", e);
                                }}/>
                        </Item>

                        <Item
                            style={{
                                width: "100%",
                                marginTop: 10,
                                paddingBottom: 15,
                                marginLeft: 10
                            }}>
                            <Label>Do you submit GST</Label>
                            <Right>
                                <Switch
                                    value={this.state.formValues["gst_available"]}
                                    onValueChange={e => {
                                        this.onChangeEvent("gst_available", e, "switch");
                                        if (!e) {
                                            this.setState({ gst: null });
                                        }
                                    }}
                                />
                            </Right>
                        </Item>
                        <Item
                            style={{
                                display: this.state.formValues.gst_available ? "flex" : "none"
                            }}>
                            <Input
                                value={this.state.formValues.gst}
                                placeholder="GSTN"
                                onChange={e => this.onChangeEvent("gst", e) }/>
                        </Item>
                    </Form>
                    <View style={{ justifyContent:'center' }}>
                        {profilePage && <PrimaryButton buttonText={'Submit'} onPress={this.submit} />}
                    </View>
                </View>
            </Theme>
        );
    }
}

const mapStateToProps = ({  }) => ({ });

const mapDispatchToProps = { saveProfileDataSteps, updateOnboardingState };

export default connect(mapStateToProps, mapDispatchToProps)(withNavigationFocus(Details));
