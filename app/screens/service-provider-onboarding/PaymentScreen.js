import React, { Component, Fragment } from "react";
import {
  View,
  Icon,
  Form,
  Item,
  Label,
  Input,
} from "native-base";

import { withNavigationFocus } from 'react-navigation';
import { connect } from 'react-redux';

import { Theme, PrimaryHeader, PrimaryFab, Toast } from '@components';
import { saveProfileDataSteps, updateOnboardingState, getBankDetails } from '@actions';
import { forEach, get as getLodash } from 'lodash';
import { handleError } from '@helpers';

const Header = (props) => (
    <PrimaryHeader
        {...props}
        type="provider"
        title=""
        description="Give Us your bank account info for payment purposes!!"
    />
);
class Payment extends Component {

    static navigationOptions = { header: null };

    constructor(props){
        super(props);
        this.state = {
            formValues: {},
            loading: false
        };
    }


    onChangeEvent(fieldName, event, type = "text") {
        let value;
        switch (type) {
            case "text":
                event.persist();
                value = event.nativeEvent.text;
                break;
            case "dropdown":
                value = event;
                break;
        }
        let formValues = this.state.formValues;
        formValues[fieldName] = value;
        this.setState({ formValues });
    }

    submit = () => {
        this.setState({ loading: true });
        const { saveProfileDataSteps, updateOnboardingState } = this.props;
        saveProfileDataSteps(this.state.formValues, 6)
        .then(() => {
            this.setState({ loading: false }, () => {
              this.props.navigation.push("ThankYou");
              updateOnboardingState({ screenName: 'ThankYou' });
            });
        })
        .catch(errorRes => {
            const { message, validation_errors } = handleError(errorRes);
                let error = message;
                if (validation_errors) {
                    forEach(validation_errors, (err, index) => {
                        error = err[0];
                    });
                }
            this.setState({ loading: false }, () => {
                Toast(error);
            });
        });
    }

    _searchIfsc = (ifsc_code) => {
        if(ifsc_code.length != 11){
            return Toast('The ifsc code must be 11 characters');
        }
        let formValues = this.state.formValues;

        this.setState({ loading: true });
        const { getBankDetails } = this.props;
        getBankDetails(ifsc_code)
        .then((response) => {
            let bankDetails = getLodash(response, 'data.data.bank_details');
            formValues['ifsc_code'] = ifsc_code;
            formValues['bank_name'] = bankDetails['BANK'];
            formValues['branch_name'] = bankDetails['BRANCH'];
            this.setState({ loading: false, formValues});
        })
        .catch((errorRes) => {
            const { message, validation_errors } = handleError(errorRes);
                let error = message;
                if (validation_errors) {
                    forEach(validation_errors, (err, index) => {
                        error = err[0];
                    });
                }
            this.setState({ loading: false }, () => Toast(error));
        })
    }

    render() {
        return (
            <Theme
                loading={this.state.loading}
                header={<Header goBackTo={'ChooseService'} {...this.props.navigation} />}
                fab={<PrimaryFab type="provider" onPress={() => this.submit} />}
            >
                <View style={{ height: 400, flex: 1 }}>
                    <Form style={{ paddingTop: 20 }}>
                      <Item fixedLabel last>
                        <Label>A/C Number</Label>
                        <Input
                          value={this.state.formValues.account_number}
                          onChange={e => {
                            this.onChangeEvent("account_number", e);
                          }}
                        />
                      </Item>
                        <Item fixedLabel last>
                            <Label>IFSC Code</Label>
                            <Icon name="ios-search" />
                            <Input 
                                style={{textTransform: 'uppercase'}} 
                                placeholder="IFSC"  
                                value={this.state.formValues.ifsc_code}
                                onChangeText={this._searchIfsc} />
                        </Item>
                      <Item fixedLabel last>
                        <Label>Bank Name</Label>
                        <Input
                            style={{textTransform: 'uppercase'}}
                            multiline
                            value={this.state.formValues.bank_name}
                        />
                      </Item>
                      <Item fixedLabel last>
                        <Label>Branch Name</Label>
                        <Input
                            multiline
                            value={this.state.formValues.branch_name}
                        />
                      </Item>
                    </Form>
                  </View>
            </Theme>
        );
    }
}

const mapStateToProps = () => ({ });

const mapDispatchToProps = { getBankDetails, saveProfileDataSteps, updateOnboardingState };

export default connect(mapStateToProps, mapDispatchToProps)(withNavigationFocus(Payment));