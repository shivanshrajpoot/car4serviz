import React, { Component } from "react";
import { 
    ListItem,
    Icon,
    Right,
    Switch,
    Button,
    Label,
    View,
    Input,
    Body,
    Item,
    List
} from "native-base";

import { withNavigationFocus } from "react-navigation";
import { Modal, StyleSheet, Text, ScrollView } from "react-native";
import { connect } from 'react-redux';

import { Theme, PrimaryHeader, PrimaryFab, Toast, PrimaryButton } from '@components';
import { getServices, saveProfileDataSteps, updateOnboardingState } from '@actions';
import { get as getLodash, forEach } from 'lodash';
import { handleError } from '@helpers';

var styles = StyleSheet.create({
  container: {
    // paddingTop: 30,
    flex: 1
  }
});

const Header = (props) => (
    <PrimaryHeader
        {...props}
        type="provider"
        title="Choose your Services"
        description="Choose what you offer within each of your service."
    />
)

class ChooseService extends Component {

    static navigationOptions = { header: null };

    constructor(props){
        super(props);
        this.state = {
            services: [],
            formValues: { services: [], service_categories: [] },
            user: {},
            selectedService: {
                categories: []
            },
            modalOpened: false,
            loading: false
        };
    }

    saveData = () => {
        let payload = {
            services: this.state.formValues.services.filter(e => e.value),
            service_subcategories: this.state.formValues.service_categories.map((sc) => { return { id: sc }})
        };
        this.setState({ loading: true });
        const { saveProfileDataSteps, updateOnboardingState } = this.props;
        saveProfileDataSteps(payload, 4)
        .then(() => {
            this.setState({ loading: false }, () => {
                updateOnboardingState({ screenName: 'CarTypePricing' });
                this.props.navigation.push("CarTypePricing");
            });
        })
        .catch(errorRes => {
            const { message, validation_errors } = handleError(errorRes);
            let error = message;
            if (validation_errors) {
                forEach(validation_errors, (err, index) => {
                    error = err[0];
                });
            }
            this.setState({ loading: false }, () => {
                Toast(error)
            });
        });
    }

    async componentDidMount() {
        this.setState({loading: true});
        const { getServices } = this.props;
        await getServices().then(services => {
            let formValues = this.state.formValues;
            formValues["services"] = services.map(type => {
                return {
                    id: type.id,
                    discount: 0,
                    value: false,
                    categories: type.categories
                };
            });
            this.setState({ services, formValues, loading: false });
        }).catch(() => {
            this.setState({loading: false });
        });
    }

    cancel() {
        let formValues = this.state.formValues;
        formValues.service_categories = this.state.selectedCategoriesCpy;
        this.setState({ formValues }, () => {
            this.setState({ modalOpened: false });
        });
    }

    save = () => {
        this.setState({ modalOpened: false });
    }

    _onValueChange = (value, i) => {
        let formValues = this.state.formValues;
        this.state.formValues.services[i].value = value;
        if (value) {
            if(!formValues.service_categories.length) {
                formValues.service_categories = formValues.service_categories.concat(
                    this.state.services[i].categories.map(c => c.id)
                );
            }

        } else {
            formValues.service_categories = formValues.service_categories.filter(e => {
                return (this.state.services[i].categories.indexOf(e.id) < 0);
            });
        }

        this.setState({
            formValues,
            selectedService: this.state.formValues.services[i],
            selectedCategoriesCpy: JSON.parse(JSON.stringify(this.state.formValues.service_categories)),
            modalOpened: value
        });
    }

    saveDiscount(serviceId, discount){
        let formValues = this.state.formValues;
        formValues.services[serviceId]['discount'] = discount;
        this.setState({formValues: formValues});
    }

    render() {
        const { navigation } = this.props;
        const profilePage = navigation.getParam('profilePage', false);
        console.log('profilePage', profilePage);
        return (
            <Theme 
                loading={this.state.loading}
                toastRef={ ref => this.toast = ref }
                header={!profilePage ? <Header goBackTo={'Workshop'} {...this.props} /> : <Header {...this.props} />}
                fab={!profilePage && <PrimaryFab type="provider" onPress={() => this.saveData} />}
            >   
                <View>
                    <List>
                    {this.state.services.map((service, i) => {
                    return (
                        <View key={i} style={{marginVertical: 15}}>
                            <ListItem itemDivider>
                                <Body><Label>{service.name}</Label></Body>
                                <Right>
                                    <Switch
                                        value={this.state.formValues.services[i].value}
                                        onValueChange={value => this._onValueChange(value, i)}
                                    />
                                </Right>
                            </ListItem>
                            {this.state.formValues.services[i].value &&
                                <Item style={{marginLeft: 18, marginRight: 18}}>
                                    <Label>Discount %</Label>
                                    <Input 
                                        value={this.state.formValues.services[i].discount && this.state.formValues.services[i].discount.toString()} 
                                        onChangeText={(disc) => this.saveDiscount(i, disc)} 
                                        keyboardType="numeric"
                                    />
                                </Item>
                            }
                        </View>
                    );
                    })}
                    </List>
                    <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                        {!this.state.loading && profilePage && <PrimaryButton buttonText={'Submit'} onPress={this.saveData} />}
                    </View>
                </View>
              <Modal
                animationType="slide"
                transparent={false}
                onRequestClose={() => this.setState({ modalOpened: false })}
                visible={this.state.modalOpened}
              >
                <View>
                  <View>
                    <View
                      style={{
                        backgroundColor: '#ffa400',
                        paddingTop: 30,
                        paddingBottom: 10,
                        paddingLeft: 20
                      }}
                    >
                      <Text style={{ color: "white", fontSize: 20 }}>
                        Select what you offer in this category
                      </Text>
                    </View>
                        <ScrollView style={{height:'68%'}}>
                          <View>
                            <View>
                              {this.state.selectedService.categories.map((c, i) => {
                                return (
                                  <ListItem key={i}>
                                    <Body>
                                      <Label>{c.name}</Label>
                                    </Body>
                                    <Right>
                                      <Switch
                                        value={this.state.formValues.service_categories.indexOf(c.id) >= 0}
                                        onValueChange={value => {
                                          let formValues = this.state.formValues;
                                          if (value) {
                                            formValues.service_categories.push(c.id);
                                          } else {
                                            formValues.service_categories = formValues.service_categories.filter(e => e != c.id);
                                          }
                                          this.setState({ formValues });
                                        }}
                                      />
                                    </Right>
                                  </ListItem>
                                );
                              })}
                            </View>
                          </View>
                        </ScrollView>
                        <View
                          style={{
                            flexDirection: "row",
                            justifyContent: "space-around",
                            marginTop: "3%"
                          }}
                        >
                          <Button
                            style={{ width: "45%" }}
                            onPress={this.save.bind(this)}
                          >
                            <Text
                              style={{
                                textAlign: "center",
                                width: "100%",
                                color: "white"
                              }}
                            >
                              <Icon name="done-all" />
                            </Text>
                          </Button>

                          <Button
                            style={{ width: "45%", textAlign: "center" }}
                            onPress={this.cancel.bind(this)}
                          >
                            <Text
                              style={{
                                textAlign: "center",
                                width: "100%",
                                color: "white"
                              }}
                            >
                              <Icon name="close" />
                            </Text>
                          </Button>
                        </View>
                  </View>
                </View>
              </Modal>
            </Theme>
        );
    }
}

const mapStateToProps = () => ({ });

const mapDispatchToProps = { getServices, saveProfileDataSteps, updateOnboardingState };

export default connect(mapStateToProps, mapDispatchToProps)(withNavigationFocus(ChooseService));