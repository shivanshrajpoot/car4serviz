import React, { Component } from "react";
import {
  Container,
  Text,
  View,
  StyleProvider,
  Icon
} from "native-base";
import { withNavigationFocus } from 'react-navigation';
import { connect } from 'react-redux';

import getTheme from "@base-theme-components";
import { updateOnboardingState } from '@actions';

class ThankYou extends Component {
    static navigationOptions = {
        header: null
    };

    constructor(props){
        super(props);
    }

    componentDidMount(){
        setTimeout(() => {
          updateOnboardingState({ screenName: 'Drawer' });
          this.props.navigation.navigate('Drawer');

        }, 2000);
    }
    
  render() {
    return (
      <StyleProvider style={getTheme()}>
        <Container>
          <View
            style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
          >
            <Icon
              name="done-all"
              style={{
                fontSize: 100,
                color: "green",
                backgroundColor: "#eff0f1",
                borderRadius: 200 / 2,
                width: 200,
                height: 200,
                alignItems: "center",
                justifyContent: "center",
                padding: 45
              }}
            />
            <Text style={{ textAlign: "center", padding: 10, fontSize: 25 }}>
              Thank you for your registration
            </Text>
            <Text>Our team will get back to you.</Text>
          </View>
        </Container>
      </StyleProvider>
    );
  }
}

const mapStateToProps = () => ({ });

const mapDispatchToProps = { updateOnboardingState };

export default connect(mapStateToProps, mapDispatchToProps)(withNavigationFocus(ThankYou));
