import React, { Component } from "react";
import { Text, Icon, View } from "native-base";
import { Image, StyleSheet } from "react-native";
import PhotoUpload from "react-native-photo-upload";
import { connect } from 'react-redux';

import { withNavigationFocus } from 'react-navigation';
import { handleError } from '@helpers';
import { Theme, PrimaryHeader, PrimaryFab, Toast } from '@components';
import { saveProfileDataSteps, updateOnboardingState } from '@actions';
import _ from 'lodash';

const Header = (props) => (
    <PrimaryHeader
        {...props}
        type="provider"
        title="Photo"
        description="Choose/Take profile photo of yours or your garage It increase your chance with customers"
    />
);

const styles = StyleSheet.create({
    content: {
        height: "100%",
        padding: 15,
        flexDirection: "column",
        flex: 1,
        alignItems: "center",
        marginTop: 50
    }
});

class Workshop extends Component {
    static navigationOptions = { header: null };
    
    state = {
        formValues: {
            profile_workshop_photo: ""
        },
        loading: false,
    };

    onPhotoSelect(avatar){
         if (avatar) {
            let formValues = this.state.formValues;
            formValues.profile_workshop_photo = avatar;
            this.setState({ formValues });
        }
    }

    submit = () => {
        this.setState({ loading: true });
        const { saveProfileDataSteps, updateOnboardingState } = this.props;

        saveProfileDataSteps(this.state.formValues, 3)
        .then(() => {
            this.setState({ loading: false }, () => {
                updateOnboardingState({ screenName: 'ChooseService' });
                this.props.navigation.push("ChooseService");
            });
        })
        .catch(errorRes => {
            this.setState({ loading: false }, () => {
                updateOnboardingState({ screenName: 'ChooseService' });
                this.props.navigation.push("ChooseService");
            });
            const { message, validation_errors } = handleError(errorRes);
            console.log('validation_errors', validation_errors);
            _.forEach(validation_errors, (err, index) => {
                console.log(err[0],index);
                this.toast.show(err[0])
            });
            this.setState({ loading: false },() => {
                Toast(message)
            });
        });
    }

    render() {
        return (
            <Theme
                loading={this.state.loading}
                {...this.props}
                toastRef={ ref => this.toast = ref }
                header={<Header/>}
                fab={<PrimaryFab type="provider" onPress={() => this.submit} />}
            >
                <View style={styles.content}>
                    <PhotoUpload
                        ref="photoUpload"
                        containerStyle={{ flexGrow: 0 }}
                        onPhotoSelect={avatar => this.onPhotoSelect(avatar)}>
                    <View style={{ position: "absolute", zIndex: 1, right: "10%" }}>
                        <Icon name="create" />
                    </View>
                    <Icon
                        name="person"
                        style={{
                            fontSize: 100,
                            color: "grey",
                            backgroundColor: "#eff0f1",
                            borderRadius: 200 / 2,
                            width: 200,
                            height: 200,
                            padding: 60,
                            alignItems: "center",
                            justifyContent: "center",
                            display: this.state.formValues.profile_workshop_photo
                            ? "none"
                            : "flex"
                        }}/>
                      <Image
                        style={{
                            borderRadius: 200 / 2,
                            width: 200,
                            height: 200,
                            padding: 61,
                            alignItems: "center",
                            justifyContent: "center",
                            display: this.state.formValues.profile_workshop_photo
                            ? "flex"
                            : "none"
                        }}
                        resizeMode="cover"
                        source={{
                            uri: this.state.formValues.profile_photo || "https://via.placeholder.com/200x200.png"
                        }}
                      />
                    </PhotoUpload>
                    <View>
                        <Text style={{color:'gray', textAlign:'center', fontSize:17, padding:'10%', paddingBottom:'0%'}}>Upload your picture with great smile!</Text>
                    </View>
                </View>
            </Theme>
        );
    }
}

const mapStateToProps = () => ({ });

const mapDispatchToProps = { saveProfileDataSteps, updateOnboardingState };

export default connect(mapStateToProps, mapDispatchToProps)(withNavigationFocus(Workshop));