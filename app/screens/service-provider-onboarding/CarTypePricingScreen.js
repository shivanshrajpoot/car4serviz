import React, { Component } from "react";
import {
  Fab,
  Icon,
  Button,
  Input,
  Item,
  View,
  Label,
  Switch,
  Right,
  Grid,
  Row,
  Text,
  Col
} from "native-base";
import { Modal, ScrollView } from "react-native";
import { connect } from 'react-redux';
import { withNavigationFocus } from 'react-navigation';

import { Theme, PrimaryHeader, PrimaryFab, Toast, PrimaryButton } from '@components';
import { getServices, saveProfileDataSteps, updateOnboardingState, getCarTypes } from '@actions';
import { get as getLodash, forEach } from 'lodash';
import { handleError } from '@helpers';

const Header = (props) => (
    <PrimaryHeader
        {...props}
        type="provider"
        title=""
        description="Let us know your general service cost & little more about facilities discounts and availablity to let
            your customers know you better."
    />
);

class CarTypePricing extends Component {
    static navigationOptions = { header: null };

    constructor(props){
        super(props);
        this.state = {
            formValues: {
                "24_facility_available": false,
                pickup_facility_available: false,
                service_at_customer_location: false,
                phone: ""
            },
            loading: false,
            errors: {},
            carTypeModal: false,
            discountModal: false,
            carTypes: []
        };
        
    }


    onChangeEvent(fieldName, event, type = "text") {
        let value;
        switch (type) {
            case "text":
                event.persist();
                value = event.nativeEvent.text;
                break;
            case "switch":
                value = event;
                break;
        }
        let formValues = this.state.formValues;
        formValues[fieldName] = value;
        this.setState({ formValues });
    }

    submit = () => {
        this.setState({ loading: true });
        console.log(this.state.formValues);
        const { saveProfileDataSteps, updateOnboardingState } = this.props;

        saveProfileDataSteps(this.state.formValues, 5)
        .then(() => {
            this.setState({ loading: false });
            updateOnboardingState({ screenName: 'Payment' });
            this.props.navigation.push("Payment");
        })
        .catch(errorRes => {
            const { message, validation_errors } = handleError(errorRes);
                let error = message;
                if (validation_errors) {
                    forEach(validation_errors, (err, index) => {
                        error = err[0];
                    });
                }
            this.setState({ loading: false }, () => {
              Toast(error);
            });
        });
    }

    async componentDidMount() {
        const { getCarTypes, getServices } = this.props;
        getCarTypes().then(response => {
            let carTypes = getLodash(response, 'data.data');
            let formValues = this.state.formValues;
            formValues["car_types"] = carTypes;
            this.setState({ carTypes, formValues });
        });

        // getServices().then(response => {
        //     let services = getLodash(response, 'data.data');
        //     let userServices = this.state.user.userable.services.map(s => s.id);
        //     console.log('userServices', userServices);
        //     services = services.filter(s => {
        //         return userServices.indexOf(s.id) >= 0;
        //     });
        //     let formValues = this.state.formValues;
        //     formValues["services"] = services.map(type => {
        //         return {
        //             id: type.id,
        //             discount: ""
        //             // value: false,
        //             // categories: type.categories
        //         };
        //     });
        //     this.setState({ services, formValues });
        // });
    }

    render() {
        const { navigation } = this.props;
        const profilePage = navigation.getParam('profilePage', false);
        console.log('profilePage', profilePage);
        return (
            <Theme
                loading={this.state.loading}
                toastRef={ ref => this.toast = ref } 
                header={!profilePage ? <Header goBackTo={'ChooseService'} {...this.props} /> : <Header {...this.props} />}
                fab={!profilePage && <PrimaryFab type="provider" onPress={() => this.submit} />}
            >
                <View
                    style={{
                      height: "100%",
                      padding: 15,
                      flexDirection: "column",
                      flex: 1,
                      alignItems: "center"
                    }}
                >
                    <Item
                      style={{
                        width: "100%",
                        marginTop: 10,
                        paddingBottom: 15,
                        marginLeft: 10
                      }}
                    >
                      {/* <Icon name="document" /> */}
                      <Label>General Service Cost</Label>
                      <Right>
                        <Button
                          transparent
                          onPress={() => this.setState({ carTypeModal: true })}
                        >
                          <Icon name="add" />
                        </Button>
                      </Right>
                    </Item>
                    <Item
                      style={{
                        width: "100%",
                        marginTop: 10,
                        paddingBottom: 15,
                        marginLeft: 10
                      }}
                    >
                      {/* <Icon name="document" /> */}
                      <Label>24 Hrs facility available</Label>
                      <Right>
                        <Switch
                          value={this.state.formValues["24_facility_available"]}
                          onValueChange={e => {
                            this.onChangeEvent("24_facility_available", e, "switch");
                          }}
                        />
                      </Right>
                    </Item>

                    <Item
                      style={{
                        width: "100%",
                        marginTop: 10,
                        paddingBottom: 15,
                        marginLeft: 10
                      }}
                    >
                      {/* <Icon name="document" /> */}
                      <Label>Pickup facility available</Label>
                      <Right>
                        <Switch
                          value={this.state.formValues["pickup_facility_available"]}
                          onValueChange={e => {
                            this.onChangeEvent(
                              "pickup_facility_available",
                              e,
                              "switch"
                            );
                          }}
                        />
                      </Right>
                    </Item>

                    <Item
                      style={{
                        width: "100%",
                        marginTop: 10,
                        paddingBottom: 15,
                        marginLeft: 10
                      }}
                    >
                      {/* <Icon name="document" /> */}
                      <Label>Service at Customer's location</Label>
                      <Right>
                        <Switch
                          value={this.state.formValues["service_at_customer_location"]}
                          onValueChange={e => {
                            this.onChangeEvent(
                              "service_at_customer_location",
                              e,
                              "switch"
                            );
                          }}
                        />
                      </Right>
                    </Item>
                    <View style={{ justifyContent: 'center' }}>
                        {profilePage && <PrimaryButton buttonText={'Submit'} onPress={this.submit} />}
                    </View>
                </View>
                <Modal
                  animationType="slide"
                  transparent={false}
                  onRequestClose={() => this.setState({ carTypeModal: false })}
                  visible={this.state.carTypeModal}
                >
                <View
                    style={{
                        backgroundColor: "#ffa400",
                        paddingTop: 30,
                        paddingBottom: 10,
                        paddingLeft: 20
                    }}
                >
                    <Text style={{ color: "white", fontSize: 20 }}>
                        Set pricing for car types.
                    </Text>
                </View>
                  <ScrollView style={{ height: "80%", padding: 10, margin:10 }}>
                    <Grid>
                    {this.state.carTypes.map((type, i) => {
                      return (
                        <Row key={i}>
                          <Col size={65}>
                            <Text style={{marginTop:12}}>{type.name + " (" + type.price_range + ") "}</Text>
                          </Col>

                          <Col size={35}>
                          <Input
                            placeholder="Enter Cost"
                            keyboardType="numeric"
                            value={this.state.formValues.car_types[i].price}
                            onChange={e => {
                              e.persist();
                              let formValues = this.state.formValues;
                              this.state.formValues.car_types[i].price =
                                e.nativeEvent.text;
                              this.setState({ formValues });
                            }}
                          />
                          </Col>
                          </Row>
                      );
                    })}
                    </Grid>
                  </ScrollView>
                  <Button full block onPress={() => this.setState({ carTypeModal: false })}>
                    <Text style={{ textAlign: "center", width:'100%' }}>
                      <Icon style={{ color:'white'}} name="done-all" />
                    </Text>
                  </Button>
                </Modal>
            </Theme>
        );
    }
}

const mapStateToProps = (auth) => ({ 
  user: auth
});

const mapDispatchToProps = { getServices, saveProfileDataSteps, updateOnboardingState, getCarTypes };

export default connect(mapStateToProps, mapDispatchToProps)(withNavigationFocus(CarTypePricing));
