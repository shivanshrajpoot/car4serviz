import React, { Component } from "react";
import { Image, Platform, Dimensions, StyleSheet, Alert, View } from "react-native";
import {
  Content,
  Text,
  List,
  ListItem,
  Container,
  Left,
  Right,
  Badge,
  Icon
} from "native-base";
import { connect } from 'react-redux';
import SimpleIcon from 'react-native-vector-icons/FontAwesome';
import { logoutUser } from '@actions';

import { colors } from '@constants';

const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;

const styles = StyleSheet.create({
    drawerCover: {
        alignSelf: "stretch",
        height: deviceHeight / 3.5,
        width: null,
        position: "relative",
        marginBottom: 10
    },
    drawerImage: {
        position: "absolute",
        left: Platform.OS === "android" ? deviceWidth / 10 : deviceWidth / 9,
        top: Platform.OS === "android" ? deviceHeight / 13 : deviceHeight / 12,
        width: 210,
        height: 75,
        resizeMode: "cover"
    },
    text: {
        fontWeight: Platform.OS === "ios" ? "500" : "400",
        fontSize: 16,
        marginLeft: 20
    },
    badgeText: {
        fontSize: Platform.OS === "ios" ? 13 : 11,
        fontWeight: "400",
        textAlign: "center",
        marginTop: Platform.OS === "android" ? -3 : undefined
    },
    userIcon: {
        position: "absolute",
        left: Platform.OS === "android" ? deviceWidth / 10 : deviceWidth / 9,
        top: Platform.OS === "android" ? deviceHeight / 13 : deviceHeight / 12,
        backgroundColor: "#eff0f1",
        borderRadius: 200 / 2,
        width: 100,
        height: 100,
        paddingTop: 30,
        paddingBottom: 30,
        paddingLeft: 32,
        alignItems: "center",
        justifyContent: "center",
        display:"flex"
    },
    userInfo: {
    	position: "absolute",
        left: Platform.OS === "android" ? deviceWidth / 2.6 : deviceWidth / 9,
        top: Platform.OS === "android" ? deviceHeight / 7.3 : deviceHeight / 12,
    }
});

const drawerCoverProvider = require("../../assets/images/drawer-cover-provider.png");
const drawerCoverClient = require("../../assets/images/drawer-cover-client.png");
const drawerImage = require("../../assets/images/user_placeholder.png");

class SideBar extends React.PureComponent {

  confirmLogout = () => {
    const { logoutUser } = this.props;
    Alert.alert(
      'Logout',
      'Are you sure ?',
      [
        {
          text: 'Logout', 
          onPress: () => logoutUser()
        },
        {
          text: 'Cancel',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        }
      ],
      {cancelable: false},
    );
  }

  render() {
    const { userType, logoutUser, user } = this.props;
    let drawerBackgroundImage = userImage = displayName = null;
    if (userType === 'provider') {
    	drawerBackgroundImage = drawerCoverProvider;
    	userImage = user.service_provider.workshop_photo_url;
    	displayName = user.service_provider.garage_name;
    }else if(userType === 'client'){
    	drawerBackgroundImage = drawerCoverClient;
		userImage = user.customer.profile_photo_url
		displayName = `${user.customer.first_name} ${user.customer.last_name}`
    }

    // let drawerBackgroundImage = userType === 'provider' ? drawerCoverProvider : drawerCoverClient;
    // let userImage = userType === 'provider' ?  user.service_provider.workshop_photo_url : user.customer.profile_photo_url; 
    // let displayName = userType === 'provider' ? user.service_provider.garage_name : `${user.customer.first_name} ${user.customer.last_name}`;
    return (
      <Container>
        <Content bounces={false} style={{ flex: 1, backgroundColor: "#fff", top: -1 }}>
        	<Image source={drawerBackgroundImage} style={styles.drawerCover} />
			{userImage && <Image source={{uri: userImage}} style={styles.userIcon} />}
			{!userImage && 
				<Icon
					name="person"
					style={ StyleSheet.flatten([styles.userIcon, { fontSize: 50, color: 'grey' }]) }
			/>}
			<View style={styles.userInfo} >
				<Text style={{ color: userType === 'provider' ? colors.clientColor : colors.providerColor }} >
					{ displayName }
				</Text>
			</View>
          	<List
	            dataArray={this.props.items}
	            renderRow={data =>
	              	<ListItem
		                button
		                noBorder
		                onPress={() => this.props.navigation.navigate(data.route)}
		            >
	                <Left>
						<SimpleIcon
							active
							name={data.icon}
							style={{ color: "#777", fontSize: 26, width: 30 }}
						/>
						<Text style={styles.text}> {data.name} </Text>
	                </Left>
              		</ListItem>}
          	/>
			<ListItem button noBorder onPress={this.confirmLogout}>
				<Left>
				<SimpleIcon
					active
					name={'sign-out'}
					style={{ color: "#777", fontSize: 26, width: 30 }}
				/>
					<Text style={styles.text}> {'Logout'} </Text>
				</Left>
			</ListItem>
        </Content>
      </Container>
    );
  }
}

const mapStateToProps = ({ auth }) => ({
    user: auth.user,
    userType: auth.userType
});

const mapDispatchToProps = { logoutUser };

export default connect(mapStateToProps, mapDispatchToProps)(SideBar);