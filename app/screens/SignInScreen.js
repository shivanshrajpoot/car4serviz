import React, { Component } from "react";
import { Icon, View, Text, Item, Input } from "native-base";
import { connect } from 'react-redux';
import { forEach, get } from 'lodash';
import { StyleSheet } from 'react-native';
import { authValidator } from '@validators';
import { handleError, navigationService } from '@helpers';
import { Theme, PrimaryButton, PrimaryHeader, Toast } from '@components';
import { loginUser, saveFcmToken, updateOnboardingState } from '@actions';


const styles = StyleSheet.create({
    wrapper: {
        flex: 1
    },
    box: {
        flexDirection: 'column',
        justifyContent: 'center',
        alignSelf: 'center'
    },
    forgotPassword: {
        color:'gray',
        textAlign:'center',
        fontSize:17,
        padding:'10%',
        paddingBottom:'0%'
    }

});

const providerStepMapping = [
    'LocationDetails',
    'Details',
    'Workshop',
    'ChooseService',
    'CarTypePricing',
    'Payment',
    'Dashboard',
]

const Header = (props) => (
    <PrimaryHeader
        title="Welcome Again"
        description="Enter Phone Number and Password"
        goBackTo="Auth"
        {...props}
    />
)

class SignIn extends Component {
    static navigationOptions = {
        header: null
    };
    
    state = {
        loading: false,
        isPasswordVisible: false,
        password: null,
        phone: null,
        passwordErr: false,
        phoneErr: false,
        disableButton: false,
    }

    submit = async () => {
        const { loginUser, updateOnboardingState, navigation, userType, saveFcmToken } = this.props;
        let { phone, password } = this.state;
        let formValues = { phone, password };
        console.log('formValues', formValues);
        try {
            await authValidator.userValidator.validate(formValues);
        } catch ({ path, message }) {
            if (path === 'password') {
                this.setState({ passwordErr: message })
            } else {
                this.setState({ passwordErr: false })
            }
            if (path === 'phone') {
                this.setState({ phoneErr: message })
            } else {
                this.setState({ phoneErr: false })
            }
            return;
        }

        this.setState({
            disableButton: true,
            loading: true
        })

        formValues.user_type = 1;
        let navigateTo = 'ProviderStack';

        if (userType !== 'provider') {
            formValues.user_type = 2;
            navigateTo = 'ClientStack';
        }
        console.log('formValues', formValues);

        try {
            let user = await loginUser(formValues);
            await saveFcmToken();

            if (userType === 'provider') {
                let serviceProviderStep = get(user, 'service_provider.step', 0);
                navigateTo = providerStepMapping[serviceProviderStep];
            }
            this.setState({
                disableButton: false,
                loading: false
            }, () => {
                updateOnboardingState({ screenName: navigateTo });
                navigationService.navigate(navigateTo);
            })
            
        } catch (errorRes) {
            const { error, validation_errors } = handleError(errorRes);
            let errorMsg = error;
            if (validation_errors) {
                forEach(validation_errors, err => {
                    errorMsg = err[0];
                });
            }
            this.setState({
                disableButton: false,
                loading: false
            }, () => Toast(errorMsg, userType))
        }
    }

    render() {
        const { navigation, userType } = this.props;

        return (
            <Theme 
                {...this.props}
                header={<Header type={userType} />}
            >
                <View style={styles.wrapper}>
                    <View style={{ margin: 10, padding: 10 }}>
                        <Item error={!(!this.state.phoneErr)} style={{ marginVertical: 10 }} >
                            <Icon active name='call' />
                            <Input keyboardType={'phone-pad'} dataDetectorTypes={'phoneNumber'} autoComplete={'tel'} placeholder='Phone' onChangeText={phone => this.setState({ phone })} value={this.state.phone} />
                            {this.state.phoneErr && <Icon name='close-circle' />}
                        </Item>
                        {this.state.phoneErr && <Text style={{ color: 'red' }}>{this.state.phoneErr}</Text>}
                        <Item error={!(!this.state.passwordErr)} style={{ marginVertical: 10 }}>
                            <Icon active name='lock' />
                            <Input contextMenuHidden={true} autoComplete={'password'} secureTextEntry={!this.state.isPasswordVisible} placeholder='Password' onChangeText={password => this.setState({ password })} value={this.state.password} />
                            {this.state.passwordErr && <Icon name='close-circle' />}
                            {!this.state.passwordErr && <Icon active name={this.state.isPasswordVisible ? 'eye-off' : 'eye'} onPress={() => this.setState((prevState) => ({ isPasswordVisible: !(prevState.isPasswordVisible) }))} />}
                        </Item>
                        {this.state.passwordErr && <Text style={{ color: 'red' }}>{this.state.passwordErr}</Text>}
                    </View>
                    <View style={styles.box}>
                        <PrimaryButton isLoading={ this.state.loading } type={ userType } onPress={ this.submit } disabled={ this.state.disableButton } buttonText="LOGIN" />
                        <Text style={styles.forgotPassword} onPress={ () => navigation.navigate('ForgotPassword') } >Forgot your Password</Text>
                    </View>
                </View>
            </Theme>
        );
    }
}

const mapStateToProps = ({ auth }) => ({
    user: auth.user,
    userType: auth.userType
});

const mapDispatchToProps = { loginUser, updateOnboardingState, saveFcmToken };

export default connect(mapStateToProps, mapDispatchToProps)(SignIn);
