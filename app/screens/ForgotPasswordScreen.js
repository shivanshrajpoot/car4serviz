import React, { Component } from "react";
import { connect } from 'react-redux';
import { forEach } from 'lodash';
import { StyleSheet } from 'react-native';
import GenerateForm from "react-native-form-builder";
import { Icon, View, Text, Spinner } from "native-base";

import { Theme, PrimaryButton, PrimaryHeader, Toast } from '@components';
import { forgotPassword, updateOnboardingState } from '@actions';
import getTheme from '@base-theme-components';  
import { handleError } from '@helpers';

const styles = StyleSheet.create({
    wrapper: {
        flex: 1
    },
    box: {
        flexDirection: 'column',
        justifyContent: 'center',
        alignSelf: 'center'
    },
    forgotPassword: {
        color:'gray',
        textAlign:'center',
        fontSize:17,
        padding:'10%',
        paddingBottom:'0%'
    }

});

const fields = [
    {
        type: "number",
        name: "phone",
        required: true,
        icon: "call",
        label: "Enter Phone"
    }
];


class ForgotPassword extends Component {
    static navigationOptions = {
        header: null
    };
    
    constructor(props){
        super(props);
        this.state = {
            disableButton: false,
            loading: false,
            resetPassword: {}
        };
        this.submit = this.submit.bind(this)
    }

    submit(){
        const { forgotPassword, updateOnboardingState, navigation, userType } = this.props;
        if (this.resetPassword.state.fields.phone.error) return Toast('Please enter valid input.');

        this.setState({
            disableButton: true,
            loading: true
        })

        const formValues = this.resetPassword.getValues();

        forgotPassword(formValues)
        .then((response) => {
            this.setState({
                disableButton: false,
                loading: false
            }, () => {
                navigation.navigate('ResetPassword', formValues);
            })
        })
        .catch((errorRes) => {
            const { message, validation_errors } = handleError(errorRes);
            let error = message;
            if (validation_errors) {
                forEach(validation_errors, (err, index) => {
                    error = err[0];
                });
            }
            this.setState({
                disableButton: false,
                loading: false
            }, () => Toast(error, userType) )
        });
    }

    render() {
        const { navigation, userType } = this.props;

        return (
            <Theme 
                {...this.props}
                toastRef={ ref => this.toast = ref }
            >
                <PrimaryHeader
                    {...this.props}
                    title="Reset Your Password"
                    description="Enter Phone Number To Reset Your Password"
                    goBackTo="Auth"
                    type={userType}
                />
                <View style={styles.wrapper}>
                    <View>
                        <GenerateForm ref={c => this.resetPassword = c } fields={fields}/>
                    </View>
                    <View style={styles.box}>
                        <PrimaryButton 
                            isLoading={ this.state.loading } 
                            type={ userType } 
                            onPress={ this.submit } 
                            disabled={ this.state.disableButton } 
                            buttonText="SUBMIT" 
                        />
                    </View>
                </View>
            </Theme>
        );
    }
}

const mapStateToProps = ({ auth }) => ({
    user: auth.user,
    userType: auth.userType
});

const mapDispatchToProps = { forgotPassword, updateOnboardingState };

export default connect(mapStateToProps, mapDispatchToProps)(ForgotPassword);
