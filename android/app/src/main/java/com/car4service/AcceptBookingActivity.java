package com.car4service;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONObject;

import java.util.Map;

/**
 * Activity to start from React Native JavaScript, triggered via
 * {@link ActivityStarterModule#navigateToExample()}.
 */
public class AcceptBookingActivity extends Activity implements OnMapReadyCallback {

    private JSONObject jsonObject;
    private TextView vehicleNameView;
    private TextView serviceLocationView;
    private TextView serviceDatetimeView;
    private TextView remarkView;
    private TextView addressView;
    private MapView bookingMapView;
    private GoogleMap map;

    @Override
    @CallSuper
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.accept_booking);
        vehicleNameView=findViewById(R.id.vehicle_name);
        serviceLocationView=findViewById(R.id.service_location);
        serviceDatetimeView=findViewById(R.id.service_datetime);
        remarkView=findViewById(R.id.remark);
        addressView=findViewById(R.id.address);
        bookingMapView=findViewById((R.id.mapView));
        getIntentData();
        bookingMapView.onCreate(savedInstanceState);
        bookingMapView.getMapAsync(this);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON|
        WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD|
        WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED|
        WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);

        findViewById(R.id.decline_booking_button).setEnabled(true);

        findViewById(R.id.accept_booking_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ActivityStarterModule.triggerBooking("ACCEPTED");
                onBackPressed();
                finish();
            }
        });

        findViewById(R.id.decline_booking_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ActivityStarterModule.triggerBooking("DECLINED");
                onBackPressed();
                finish();
            }
        });
    }

    private void getIntentData(){
        try{
            if (getIntent().getExtras()!=null)
                jsonObject=new JSONObject(getIntent().getExtras().getString("json"));
            setData();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void setData(){
        if (jsonObject==null)
            return;
        String vehicle_name=jsonObject.optString("vehicle_name");
        String serviceDatetime=jsonObject.optString("service_datetime");
        String remark=jsonObject.optString("remark");
        String address=jsonObject.optString("address");
        String service_location=jsonObject.optString("service_location");
        
        vehicleNameView.setText(vehicle_name);
        serviceDatetimeView.setText(serviceDatetime);
        remarkView.setText(remark);
        addressView.setText(address);
        serviceLocationView.setText(service_location);
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        if (jsonObject==null)
            return;
        Double latitude = Double.parseDouble(jsonObject.optString("latitude"));
        Double longitude = Double.parseDouble(jsonObject.optString("longitude"));
        this.map=googleMap;
        map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        map.addMarker(new MarkerOptions().position(new LatLng(latitude, longitude) ) );
    }
}
