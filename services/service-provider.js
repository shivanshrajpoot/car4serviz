import axios from 'axios';
import { getAuthHeaders, parseSuccessData } from './service';

export const getCarTypes = async () => {
    let authHeaders = await getAuthHeaders();
    return axios.get("/car-types", {headers:authHeaders})
    .then(parseSuccessData)
}

export const saveProfileData = async (data) => {
    let authHeaders = await getAuthHeaders();
    return axios.put("/service-providers", data, {headers:authHeaders})
    .then(parseSuccessData)
}

export const saveProfileDataSteps = async (data, step) => {
    let authHeaders = await getAuthHeaders();
    return axios.post(`/service-providers/step/${step}`, data, {headers:authHeaders})
    .then(parseSuccessData)
}

export const getServices = async () => {
    let authHeaders = await getAuthHeaders();
    return axios.get("/services", {headers:authHeaders})
    .then(parseSuccessData)
}