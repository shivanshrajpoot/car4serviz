import { isSignedIn } from '../auth';

export const getAuthHeaders = () => {
    return new Promise(async (res, rej) => {
        let token = await isSignedIn();
        if(token) {
            return res({'Authorization' : 'Bearer ' + token});
        } else {
            reject();
        }
    })
}

export const parseSuccessData = (res) => {
    console.log(res);
    return res.data.data
}
